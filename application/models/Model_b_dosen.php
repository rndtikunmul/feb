
<?php
class Model_b_dosen extends Model_Master
{
    protected $table = 'f_dosen';


    public function __construct()
    {
        parent::__construct();
    }       
    function all($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('f_karyawan','dosenKaryawanId = karyawanId','LEFT');
        $this->db->join('f_prodi','karyawanProdiId = prodiId','LEFT');
        if(!empty($id)){
        $this->db->where($id);
        }
      
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

     function all_dosen_jurusan($id)
    {
        $this->db->select('*');
        $this->db->from('f_karyawan');
        $this->db->join('f_dosen','karyawanId = dosenKaryawanId','LEFT');
        $this->db->join('f_prodi','karyawanProdiId = prodiId','LEFT');
       if(!empty($id)){
        $this->db->where($id);
        }
      
        $this->db->where('KaryawanJenis','dosen');
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('f_karyawan','dosenKaryawanId = karyawanId','LEFT');
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }



}
