
<?php
class Model_b_files extends Model_Master
{
    protected $table = 'f_files';

    public function __construct()
    {
        parent::__construct();
    }

    function all($user)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('s_user', 'susrNama = fileUserId', 'LEFT');
        $this->db->where('susrNama', $user);
        $this->db->order_by('fileDateTime DESC');
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

     function all_admin($user)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('s_user', 'susrNama = fileUserId', 'LEFT');
        $this->db->join('s_user_group', 'susrSgroupNama = sgroupNama', 'LEFT');
        $this->db->where('sgroupProdiId', $user);
        $this->db->order_by('fileDateTime DESC');
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }
}
