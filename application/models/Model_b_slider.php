
<?php
class Model_b_slider extends Model_Master
{
    protected $table = 'f_slider';

    public function __construct()
    {
        parent::__construct();
    }       
    
 //    function get_slider($number, $start = 0)
	// {
	// 	$this->db->select('*');
	// 	$this->db->from('f_slider');
	// 	$this->db->order_by('sliderId', 'asc');
 //        $this->db->limit($number, $start);
	// 	$qr=$this->db->get();

	// 	if($qr->num_rows()>0)
	// 		return $qr->result();
	// 	else
	// 		return FALSE;
	// }

	function get_slider($key)
	{
		$this->db->select('*');
		$this->db->from('f_slider');
		$this->db->join('f_prodi','prodiId=sliderUnit', 'LEFT');
		if($key==2){
			$this->db->where('prodiJurusanId','5');
		}elseif($key==4){
			$this->db->where('prodiJurusanId','6');
		}elseif($key==8){
			$this->db->where('prodiJurusanId','8');
		}
		else{
			$this->db->where('sliderUnit',$key);
		}
		$qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
	}

}
            