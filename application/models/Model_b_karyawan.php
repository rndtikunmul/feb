
<?php
class Model_b_karyawan extends Model_Master
{
    protected $table = 'f_karyawan';


    public function __construct()
    {
        parent::__construct();
    }       
    function all($Unit,$karyawanJenis)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('f_prodi','karyawanProdiId = prodiId','LEFT');
        if($Unit != '%')
        $this->db->where('karyawanProdiId',$Unit);
        if($karyawanJenis != '%')
        $this->db->where('karyawanJenis',$karyawanJenis);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function sdm_dosen_detail($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('f_prodi','karyawanProdiId = prodiId','LEFT');
        $this->db->where('karyawanNIP',$id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }

   

   public function getAll($id)
   {
    $this->db->select('*');
    $this->db->from('f_karyawan');
    $this->db->join('f_prodi','karyawanProdiId = prodiId','LEFT');
    $this->db->join('f_dosen','karyawanId = dosenKaryawanId','LEFT');
    $this->db->where($id);
    $this->db->where('karyawanNIP Is Not Null');
    return $this->db->get();
}

function prodi_karyawan($id)
{
    $this->db->select('*');
    $this->db->from($this->table);
    $this->db->join('f_prodi','karyawanProdiId = prodiId','LEFT');
    $this->db->where($id);
    $qr=$this->db->get();
    if($qr->num_rows()>0)
        return $qr->result();
    else
        return false;
}

function prodi_karyawan_sdm($limit, $offset,$id,$jenis)
{
    $this->db->select('*');
    $this->db->from($this->table);
    $this->db->join('f_prodi','karyawanProdiId = prodiId','LEFT');
    $this->db->where($id);
    $this->db->where('karyawanJenis',$jenis);
    $this->db->where('karyawanNIP Is Not Null');
    $this->db->limit($limit, $offset);
    $qr=$this->db->get();
    if($qr->num_rows()>0)
        return $qr->result();
    else
        return false;
}

 public function getDataPagination($limit, $offset,$id)
    {
       $this->db->select('*');
       $this->db->from('f_karyawan');
       $this->db->join('f_prodi','karyawanProdiId = prodiId','LEFT');
       $this->db->join('f_dosen','karyawanId = dosenKaryawanId','LEFT');
       $this->db->where($id);
       $this->db->where('karyawanNIP Is Not Null');
       $this->db->limit($limit, $offset);
       return $this->db->get();
   }

function by_id($id)
{
    $this->db->select('*');
    $this->db->from($this->table);
    $this->db->join('f_prodi','karyawanProdiId = prodiId','LEFT');
    $this->db->where($id);
    $qr=$this->db->get();
    if($qr->num_rows()==1)
        return $qr->row();
    else
        return false;
}

function get_karyawan($id)
{
    $this->db->select('*');
    $this->db->from($this->table);
    $this->db->join('f_prodi','karyawanProdiId = prodiId','LEFT');
    $this->db->where($id);
    $qr=$this->db->get();
    if($qr->num_rows()>0)
        return $qr->result();
    else
        return false;
}

function search($key)
{
    $this->db->select('*');
    $this->db->from($this->table);
    $this->db->join('f_prodi','karyawanProdiId = prodiId','LEFT');
    $this->db->where('karyawanNIP Is Not Null');
    $this->db->where("(karyawanJenis LIKE '%$key%' or karyawanNama LIKE '%$key%'  or karyawanNIP LIKE '%$key%')");
    $qr=$this->db->get();
    if($qr->num_rows()>0) 
        return $qr->result();
    else
        return false;
}

function search_prodi($key,$id)
{
    $this->db->select('*');
    $this->db->from($this->table);
    $this->db->join('f_prodi','karyawanProdiId = prodiId','LEFT');
    $this->db->where($id);
    $this->db->where('karyawanNIP Is Not Null');
    $this->db->where("(karyawanJenis LIKE '%$key%' or karyawanNama LIKE '%$key%'  or karyawanNIP LIKE '%$key%')");
   
    $qr=$this->db->get();
    if($qr->num_rows()>0)
        return $qr->result();
    else
        return false;
}

function prodi_karyawan_foto($id,$prodi)
{
    $this->db->select('*');
    $this->db->from('f_karyawan');
    $this->db->join('f_prodi','karyawanProdiId = prodiId','LEFT');
    $this->db->where($id);
    $this->db->where('karyawanProdiId',$prodi);
    $qr=$this->db->get();
    if($qr->num_rows()==1)
        return $qr->row();
    else
        return false;
}



}
