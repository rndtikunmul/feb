<?php
class Model_prodi extends CI_Model
{	
	function get_id($key)
	{
		$this->db->select('*');
		$this->db->from('f_prodi');
		$this->db->where('prodiId',$key);
		$qr=$this->db->get();

		if($qr->num_rows()==1)
			return $qr->row();
		else
			return FALSE;
	}

	function get_prodi_menu($key)
	{
		$this->db->select('*');
		$this->db->from('f_page_prodi');
		$this->db->where('pageProdi',$key);
		$this->db->where("(pageJudul LIKE '%Visi%')");
		$qr=$this->db->get();

		if($qr->num_rows()==1)
			return $qr->row();
		else
			return FALSE;
	}

	function menu($key)
    {
        $this->db->select('*');
        $this->db->from('f_page_prodi');
        $this->db->join('f_prodi','prodiId=pageProdi', 'LEFT');
        $this->db->where('pageProdi',$key);  
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

	function get_foto($key)
	{
		$this->db->select('headgalfNama, galfNama, galfFiles');
		$this->db->from('f_prodi');
		$this->db->join('f_galeri_foto','prodiUser=galfUserId', 'LEFT');
		$this->db->join('f_head_galeri_foto','galfHeadgalfId=headgalfId', 'LEFT');
		$this->db->where('prodiId',$key);
		$qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
	}

	function get_foto_prodi($key)
	{
		$this->db->select('*');
		$this->db->from('f_galeri_foto');
		// $this->db->join('f_galeri_foto','prodiUser=galfUserId', 'LEFT');
		$this->db->join('f_head_galeri_foto','galfHeadgalfId=headgalfId', 'LEFT');
		$this->db->where('galfUserId',$key);
		$qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
	}

	
	function get_lab($key)
    {
        $this->db->select('*');
        $this->db->from('f_lab');
		$this->db->join('f_prodi','prodiId=labprodiId', 'LEFT');
		$this->db->where('labprodiId',$key);
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

	function get_lab_id($key)
	{
		$this->db->select('*');
		$this->db->from('f_lab');
		$this->db->join('f_prodi','prodiId=labprodiId','left');
		$this->db->where('labId',$key);
		$qr=$this->db->get();

		if($qr->num_rows()==1)
			return $qr->row();
		else
			return FALSE;
	}

	function cari_berita($keyword)
	{
		$matchb = $this->input->post('cari');
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->join('s_user','susrNama=beritaAuthor','left');
		$this->db->like('beritaNama',$keyword);
		// $this->db->or_like('beritaContent',$keycari);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function get_slider($key)
	{
		$this->db->select('*');
		$this->db->from('f_slider');
		$this->db->join('f_prodi','prodiId=sliderUnit', 'LEFT');
		if($key==2){
			$this->db->where('prodiJurusanId','5');
		}elseif($key==4){
			$this->db->where('prodiJurusanId','6');
		}elseif($key==8){
			$this->db->where('prodiJurusanId','8');
		}
		else{
			$this->db->where('sliderUnit',$key);
		}
		$this->db->where('sliderIsDisplay','1');
		$qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
	}

}
?>