<?php
class Model_f_master extends Model_Master
{
    public function __construct()
    {
        parent::__construct();
    }

    function get_prodi()
	{
		$this->db->select('*');
        $this->db->from('f_jurusan');
        $this->db->join('f_prodi','jurusanId = prodijurusanId','LEFT');
        $this->db->where("jurusanId != 9");
		$this->db->order_by('jurusanId,prodiId');
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	 function get_informasi()
	{
		$this->db->select('*');
        $this->db->from('f_jenjang');
        $this->db->join('f_infodaftar','jenjangId = infodaftarJenjangId','LEFT');
		$this->db->order_by('jenjangId');
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	 function get_informasi_id($id)
	{
		$this->db->select('*');
        $this->db->from('f_jenjang');
        $this->db->join('f_infodaftar','jenjangId = infodaftarJenjangId','LEFT');
		$this->db->where('infodaftarId',$id);
		$qr=$this->db->get();
		 if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
	}

	function get_lab()
	{
		$this->db->select('*');
        $this->db->from('f_lab');
		$this->db->order_by('labId');
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function get_jurnam()
	{
		$this->db->select('*');
        $this->db->from('f_jurnal_nama');
		$this->db->order_by('jurnamId');
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function get_link()
	{
		$this->db->select('*');
        $this->db->from('f_link');
		$this->db->order_by('linkId');
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	 function get_slider()
	{
		$this->db->select('*');
        $this->db->from('f_slider');
		$this->db->where('sliderIsDisplay',1);
		$this->db->where('sliderUnit',1);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function get_fotofb($number, $start = 0)
	{
		$this->db->select('*');
        $this->db->from('f_logo');
		$this->db->where("logoSection = 'Foto FB'",);
		$this->db->order_by('logoId','desc');
		$this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function get_keanggotaan($number, $start = 0)
	{
		$this->db->select('*');
        $this->db->from('f_logo');
		$this->db->where("logoSection = 'Keanggotaan'",);
		$this->db->order_by('logoId','desc');
		$this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function get_langganan($number, $start = 0)
	{
		$this->db->select('*');
        $this->db->from('f_logo');
		$this->db->where("logoSection = 'Jurnal Langganan'",);
		$this->db->order_by('logoId','desc');
		$this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function get_jurusan($number, $start = 0)
	{
		$this->db->select('*');
        $this->db->from('f_logo');
		$this->db->where("logoSection = 'Jurusan'",);
		$this->db->order_by('logoId','desc');
		$this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function get_youtube($number, $start = 0)
	{
		$this->db->select('*');
        $this->db->from('f_video');
		$this->db->order_by('videoId','desc');
		$this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}
}