            <?php
            class Model_f_kerjasama extends Model_Master
            {
                protected $table = 'f_kerjasama';

                public function __construct()
                {
                    parent::__construct();
                }       
                
                public function all($id)
                {
                    $this->db->select('*');
                    $this->db->from('f_kerjasama');
                    $this->db->where('kerjasamaJenis',$id);
                    $this->db->order_by('kerjasamaTahun DESC');
                    return $this->db->get();
                }
            }
            