<?php
class Model_b_berita extends Model_Master
{
    protected $table = 'f_berita';

    public function __construct()
    {
        parent::__construct();
    }       
    
    function all()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function by_user($user,$tanggalawal,$tanggalakhir)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('beritaUnit', $user);
        if (!empty($tanggalawal))
        $this->db->where("beritaDatetime >=", $tanggalawal);
        if (!empty($tanggalakhir))
        $this->db->where("beritaDatetime <=", $tanggalakhir);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function by_idd($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }

    public function by_id($key)
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('beritaNama',$key);
		$qr=$this->db->get();

		if($qr->num_rows()==1)
			return $qr->row();
		else
			return FALSE;
	}

    function get_berita($section,$number, $start = 0)
	{
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->where('beritaSection', $section);
        $this->db->where('beritaIsDisplay','1'); 
		$this->db->order_by('beritaDatetime', 'desc');
        $this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

      


}
            