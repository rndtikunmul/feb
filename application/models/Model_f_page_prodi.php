    
<?php
class Model_f_page_prodi extends Model_Master
{
    protected $table = 'f_page_prodi';


    public function __construct()
    {
        parent::__construct();
    }       
    function all($key)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        // $this->db->join('s_user_modul_group_ref','pageHead = susrmdgroupNama','LEFT');
        $this->db->join('f_prodi','prodiId=pageProdi', 'LEFT');
        $this->db->where('pageProdi',$key);
        
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('s_user_modul_group_ref','pageHead = susrmdgroupNama','LEFT');
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }



}
