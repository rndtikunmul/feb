
<?php
class Model_b_hgfoto extends Model_Master
{


	public function __construct()
	{
		parent::__construct();
	}       


	 function all_id($id)
    {
        $this->db->select('*');
        $this->db->from('f_head_galeri_foto');
        $this->db->join('f_prodi','prodiId = headgalfUserId','LEFT');
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

}
