<?php
class Model_berita extends CI_Model
{

	function get_berita($section,$number, $start = 0)
	{
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->where('beritaIsDisplay','1'); 
		$this->db->where('beritaSection', $section);
		$this->db->where('beritaUnit','1');
		$this->db->OR_where("('beritaSection = ".$section."'  AND 'beritaprodiaprov = 1')");
		$this->db->order_by('beritaDatetime', 'desc');
		$this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}


	public function getDataPagination($limit, $offset,$section)
	{
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->where("(beritaIsDisplay = '1' AND beritaSection = '".$section."'  AND beritaUnit = '1')");
		$this->db->OR_where("('beritaSection = ".$section."'  AND 'beritaprodiaprov = 1')");
		$this->db->order_by('beritaDatetime', 'desc');
		$this->db->limit($limit, $offset);
		return $this->db->get();
	}

	public function getAll($section)
	{
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->where("(beritaIsDisplay = '1' AND beritaSection = '".$section."'  AND beritaUnit = '1')");
		$this->db->OR_where("('beritaSection = ".$section."'  AND 'beritaprodiaprov = 1')");
		return $this->db->get();
	}

	function get($number =500, $start = 0)
	{
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->where('beritaIsDisplay','1'); 
		$this->db->where('beritaUnit','1');
		$this->db->OR_where("(('beritaSection = pengumuman' or 'beritaSection = berita')  AND 'beritaprodiaprov = 1')");
		$this->db->order_by('beritaDatetime', 'desc');
		$this->db->limit($number, $start);		
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	

	
	function get_beritaaa($number, $start = 0)
	{
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->where('beritaIsDisplay','1'); 
		$this->db->where('beritaSection', 'berita');
		$this->db->where('beritaUnit','1');
		$this->db->OR_where("('beritaSection = berita'  AND 'beritaprodiaprov = 1')");
		$this->db->order_by('beritaDatetime', 'desc');
		$this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}


	public function getDataPagination_mahasiswa($limit, $offset,$section)
	{
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->where("(beritaIsDisplay = '1' AND beritaSection = '".$section."'  AND beritaUnit = '1')");
		$this->db->OR_where("(beritaIsDisplay = '1' AND beritaSection = '".$section."'  AND beritaprodiaprov = '1')");
		$this->db->OR_where("(beritaIsDisplay = '1' AND beritaSection = '".$section."'  AND beritaUnit = '15')");
		$this->db->order_by('beritaDatetime', 'desc');
		$this->db->limit($limit, $offset);
		return $this->db->get();
	}

	public function getAll_mahasiswa($section)
	{
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->where("(beritaIsDisplay = '1' AND beritaSection = '".$section."'  AND beritaUnit = '1')");
		$this->db->OR_where("(beritaIsDisplay = '1' AND beritaSection = '".$section."'  AND beritaprodiaprov = '1')");
		$this->db->OR_where("(beritaIsDisplay = '1' AND beritaSection = '".$section."'  AND beritaUnit = '15')");
		return $this->db->get();
	}


	function get_berita_mahasiswa($section,$number, $start = 0)
	{
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->where("(beritaIsDisplay = '1' AND beritaSection = '".$section."'  AND beritaUnit = '1')");
		$this->db->OR_where("(beritaIsDisplay = '1' AND beritaSection = '".$section."'  AND beritaprodiaprov = '1')");
		$this->db->OR_where("(beritaIsDisplay = '1' AND beritaSection = '".$section."'  AND beritaUnit = '15')");
		$this->db->order_by('beritaDatetime', 'desc');
		$this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}
	
	function get_berita_pin($number =500, $start = 0)
	{
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->where('beritaIsDisplay','1'); 
		$this->db->where('beritaIsPin','1');  
		$this->db->order_by('beritaDatetime', 'desc');
		$this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function get_nama($key)
	{
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->join('s_user','susrNama=beritaAuthor','left');
		$this->db->where('beritaNama',$key);
		$qr=$this->db->get();

		if($qr->num_rows()==1)
			return $qr->row();
		else
			return FALSE;
	}

	function cari_berita($keyword)
	{
		$matchb = $this->input->post('cari');
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->join('s_user','susrNama=beritaAuthor','left');
		$this->db->like('beritaNama',$keyword);
		// $this->db->or_like('beritaContent',$keycari);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function beritaprodi($sgroupProdiId,$number, $start = 0)
	{
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->where("beritaSection IN ('agenda','berita','pengumuman','kegiatan')"); 
		$this->db->where('beritaUnit', $sgroupProdiId); 
		$this->db->where('beritaIsDisplay','1'); 
		$this->db->order_by('beritaDatetime', 'desc');
		if ($number == true){
			$this->db->limit($number, $start);
		}
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function search($key)
	{


		$this->db->select("beritaContent, beritaJudul, beritaBanner, beritaNama, beritaDatetime ,beritaAuthor, beritaTag,beritaSection");
		$this->db->from('f_berita');
		$this->db->like('beritaJudul', $key);
		// $this->db->OR_like('beritaContent', $key);
		
		$query1 = $this->db->get_compiled_select();

		$this->db->select(" jurnalAbst As beritaContent, jurnalJudul As beritaJudul, jurnalJudul As beritaBanner, jurnalPenulis As beritaNama, jurnalTgl As beritaDatetime , jurnalLink As beritaAuthor, jurnalLink As beritaTag, jurnalnamaId As beritaSection");
		$this->db->from('f_jurnal');
		$this->db->like('jurnalJudul', $key);
		// $this->db->OR_like('jurnalAbst', $key);
		$this->db->OR_like('jurnalPenulis', $key);
		
		$query2 = $this->db->get_compiled_select();

		// $qr = $this->db->get();

		$qr = $this->db->query($query1 . " UNION " . $query2);

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

}
?>