<?php
class Model_prestasimhs extends CI_Model
{
	function cari_prestasi($key)
	{
		$this->db->select('*');
		$this->db->from('f_prestasimhs');    
		$this->db->join('f_prodi','prestasiProdiId = prodiId','LEFT');
		$this->db->like('prestasiTingkat', $key);
        $this->db->OR_like('prestasiNamaMhs', $key);
        $this->db->OR_like('prestasiJuara', $key);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	public function getAll($id)
	{
		$this->db->select('*');
		$this->db->from('f_prestasimhs');
		$this->db->join('f_prodi','prestasiProdiId = prodiId','LEFT');
		$this->db->where('prestasiTingkat',$id);
		return $this->db->get();
	}

	public function internasional($limit, $offset,$id)
	{
		$this->db->select('*');
		$this->db->from('f_prestasimhs');
		$this->db->join('f_prodi','prestasiProdiId = prodiId','LEFT');
		$this->db->where('prestasiTingkat',$id);
		$this->db->limit($limit, $offset);
		return $this->db->get();
	}

	

	function nasional()
	{
		$this->db->select('*');
		$this->db->from('f_prestasimhs');
		$this->db->join('f_prodi','prestasiProdiId = prodiId','LEFT');
		$this->db->where('prestasiTingkat','Nasional');
		$qr=$this->db->get();
		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}

	function provinsi()
	{
		$this->db->select('*');
		$this->db->from('f_prestasimhs');
		$this->db->join('f_prodi','prestasiProdiId = prodiId','LEFT');
		$this->db->where('prestasiTingkat','Provinsi');
		$qr=$this->db->get();
		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}

	function kotakab()
	{
		$this->db->select('*');
		$this->db->from('f_prestasimhs');
		$this->db->join('f_prodi','prestasiProdiId = prodiId','LEFT');
		$this->db->where('prestasiTingkat','Kota');
		$qr=$this->db->get();
		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}
	
	
}
?>