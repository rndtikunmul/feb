<?php
class Model_kerjasama extends CI_Model
{
	function cari_kerjasama($key)
	{
		$this->db->select('*');
		$this->db->from('f_kerjasama');    
		$this->db->like('kerjasamaNama', $key);
        $this->db->OR_like('kerjasamaJenis', $key);
        $this->db->OR_like('kerjasamaLembaga', $key);
		$this->db->order_by('kerjasamaTahun DESC');
    	$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	public function getAll($id)
	{
		$this->db->select('*');
		$this->db->from('f_kerjasama');
		$this->db->where('kerjasamaJenis',$id);
		$this->db->order_by('kerjasamaTahun DESC');
		return $this->db->get();
	}

	public function jenis($limit, $offset,$id)
	{
		$this->db->select('*');
		$this->db->from('f_kerjasama');
		$this->db->where('kerjasamaJenis',$id);
		$this->db->order_by('kerjasamaTahun DESC');
		$this->db->limit($limit, $offset);
		return $this->db->get();
	}

}
?>