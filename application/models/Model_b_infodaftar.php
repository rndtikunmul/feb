<?php
class Model_b_infodaftar extends Model_Master
{
    protected $table = 'f_infodaftar';


    public function __construct()
    {
        parent::__construct();
    }
    function all()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('f_jenjang', 'infodaftarJenjangId = jenjangId', 'LEFT');
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function all_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('f_jenjang', 'infodaftarJenjangId = jenjangId', 'LEFT');
          $this->db->where($id);
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('f_jenjang', 'infodaftarJenjangId = jenjangId', 'LEFT');
        $this->db->where($id);
        $qr = $this->db->get();
        if ($qr->num_rows() == 1)
            return $qr->row();
        else
            return false;
    }
}
