<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Prodi extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->_controller_name = 'prodi';
        $this->_model = 'model_f_master';
        $this->load->model('model_berita','',TRUE);
        $this->load->model('model_b_berita','',TRUE);
        $this->load->model('model_prodi','',TRUE);
        $this->load->model('model_b_galfoto','',TRUE);
        $this->load->model('Model_jurnal', '', TRUE);
        $this->load->model('Model_b_karyawan', '', TRUE);
        $this->load->model($this->_model, '', TRUE);
        $this->load->library('Aksesapi');
    }

    public function index()//single post page
    {
        redirect('/home', 'refresh');
        $data['currentPage'] = false;
        $data['search_url'] = site_url('f_home/searchpost').'/';
    }

    public function post($prodiId)//single post page
    {
        $prodiId = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $this->session->_unit     = $prodiId;
        $data['prodi_active'] = $this->encryptions->encode($prodiId,$this->config->item('encryption_key')); ;
        // print_r($data['prodi_active'] );
        // exit;
        $pageId =$this->uri->segment(4);
        if ($pageId == false) {
            $data['contenprodi'] =$this->model_prodi->get_prodi_menu($prodiId);
            $data['pageId'] = false;

        }elseif ($pageId == 'sdm') {
                $data['pageId'] =$pageId;
                $data['contenprodi'] =false;

        }elseif ($pageId == 'berita') {
            $data['pageId'] =$pageId;
            $data['contenprodi'] =false;

        }else{
            $data['contenprodi'] = $this->{$this->_model}->get_by_id('f_page_prodi',['pageId'=>$pageId]);
            $data['pageId'] = false;
        }
        $data['is_active'] = '';
        $data['pages'] = 'page/prodi/post3';
        $data['menuprodi'] = $this->model_prodi->menu($prodiId);

        $data['datas'] = $this->model_prodi->get_id($prodiId);
        $data['informasi'] = $this->{$this->_model}->get_informasi();
        $data['prodinama'] = $data['datas']->prodiNama;
        $this->session->_unitnama     = $data['datas']->prodiNama;

        $data['jurnal'] = $this->Model_jurnal->get( ['jurnamprodiId'=>$prodiId]);
        $data['all'] = $this->model_berita->get(5);
        $data['pin'] = $this->model_berita->get_berita_pin(5);
        $data['prodiId_select'] = $prodiId;
        if ($prodiId == in_array($prodiId, array("2","4","8"))) {
            $prodijurusanId = $this->{$this->_model}->get_by_id('f_prodi',['prodiId'=>$prodiId]);
            $data['staff'] = $this->Model_b_karyawan->prodi_karyawan(['prodijurusanId'=>$prodijurusanId->prodijurusanId]);  
            $data['prorjur_select'] =  $prodiId;
        }else{
            $data['staff'] = $this->Model_b_karyawan->prodi_karyawan(['karyawanProdiId'=>$prodiId]);
            $data['prorjur_select'] =  $prodiId;    
        }
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
        $data['header'] = $header;
        $prodi = $this->{$this->_model}->get_prodi('f_jurusan');
        $data['prodi'] = $prodi;
        $data['menu'] = menu();
        $data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
        $data['langganan'] = $this->{$this->_model}->get_langganan(5);
        $stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
        $data['stat'] = $stat;
        $link = $this->{$this->_model}->get_link();
        $data['link'] = $link;
        $lab = $this->{$this->_model}->get_lab('f_lab');
        $data['lab'] = $lab;
        $jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
        $data['jurnam'] = $jurnam;
        $data['foto'] = $this->model_prodi->get_foto_prodi($prodiId);
        //  echo $this->db->last_query();
        $data['slider'] = $this->model_prodi->get_slider($prodiId);
        // print_r( $data['slider']);
        // exit;
        $data['pengumuman'] = $this->model_berita->beritaprodi($prodiId,5);
        $data['search_url'] = site_url('f_home/searchpost').'/';
        $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $this->load->view('page/templateprodi', $data);
    }

    public function ragam_informasi($prodiId)//single post page
    {
        $prodiId = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $this->session->_unit     = $prodiId;
        $data['prodi_active'] = $this->encryptions->encode($prodiId,$this->config->item('encryption_key')); ;
        // print_r($data['prodi_active'] );
        // exit;
        $pageId =$this->uri->segment(4);

        if ($pageId == false) {
            $data['contenprodi'] =$this->model_prodi->get_prodi_menu($prodiId);
            $data['pageId'] = false;

        }elseif ($pageId == 'sdm') {
                $data['pageId'] =$pageId;
                $data['contenprodi'] =false;

        }elseif ($pageId == 'berita') {
            $data['pageId'] =$pageId;
            $data['contenprodi'] =false;

        }else{
            $data['contenprodi'] = $this->{$this->_model}->get_by_id('f_page_prodi',['pageId'=>$pageId]);
            $data['pageId'] = false;
        }
        $data['is_active'] = '';
        $data['pages'] = 'page/prodi/post3';
        $perpage =10;
        $offset = $this->uri->segment(5);
      
        $data['menuprodi'] = $this->model_prodi->menu($prodiId);
        if ($offset == false){
             $data['datas'] = $this->model_prodi->get_id($prodiId);
            $data['informasi'] = $this->{$this->_model}->get_informasi();
            $data['prodinama'] = $data['datas']->prodiNama;
            $this->session->_unitnama     = $data['datas']->prodiNama; 
                   if ($prodiId == in_array($prodiId, array("2","4","8"))) {
                        $prodijurusanId = $this->{$this->_model}->get_by_id('f_prodi',['prodiId'=>$prodiId]);
                        $data['staff'] = $this->Model_b_karyawan->prodi_karyawan(['prodijurusanId'=>$prodijurusanId->prodijurusanId]);  
                        $data['prorjur_select'] =  $prodiId;
                    }else{
                        $data['staff'] = $this->Model_b_karyawan->prodi_karyawan(['karyawanProdiId'=>$prodiId]);
                        $data['prorjur_select'] =  $prodiId;    
                    }
             $data['pengumuman'] = $this->model_berita->beritaprodi($prodiId,$perpage,$offset);
             $jumlah = $this->model_berita->beritaprodi($prodiId,'','');
             $jum = $jumlah!=false?count($jumlah ):'';
              $config['base_url'] = site_url('prodi/ragam_informasi/'.$prodiId.'/'.$pageId .'/');  
       
        }else{
              $data['datas'] = $this->model_prodi->get_id($this->uri->segment(3));
        $data['informasi'] = $this->{$this->_model}->get_informasi();
        $data['prodinama'] = $data['datas']->prodiNama;
        $this->session->_unitnama     = $data['datas']->prodiNama;
            $data['pengumuman'] = $this->model_berita->beritaprodi($this->uri->segment(3),$perpage,$offset);
            $jumlah = $this->model_berita->beritaprodi($this->uri->segment(3),'','');
            $jum = $jumlah!=false?count($jumlah ):'';
             $config['base_url'] = site_url('prodi/ragam_informasi/'.$this->uri->segment(3).'/'.$pageId .'/');  
       
        }
   
        $data['jurnal'] = $this->Model_jurnal->get( ['jurnamprodiId'=>$prodiId]);
        $data['all'] = $this->model_berita->get(5);
        $data['pin'] = $this->model_berita->get_berita_pin(5);
        $data['prodiId_select'] = $prodiId;
        $config['total_rows'] =  $jum; 
        $config['per_page'] = $perpage;
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li class="page-item page-link">';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="page-item page-link">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li class="page-item page-link">';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li class="page-item page-link">';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li class="page-item page-link">';
        $config['num_tag_close']    = '</li>';
        $this->pagination->initialize($config);
        $data['offset'] =$offset;

        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
        $data['header'] = $header;
        $prodi = $this->{$this->_model}->get_prodi('f_jurusan');
        $data['prodi'] = $prodi;
        $data['menu'] = menu();
        $data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
        $data['langganan'] = $this->{$this->_model}->get_langganan(5);
        $stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
        $data['stat'] = $stat;
        $link = $this->{$this->_model}->get_link();
        $data['link'] = $link;
        $lab = $this->{$this->_model}->get_lab('f_lab');
        $data['lab'] = $lab;
        $jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
        $data['jurnam'] = $jurnam;
        $data['foto'] = $this->model_prodi->get_foto_prodi($prodiId);
        //  echo $this->db->last_query();
        $data['slider'] = $this->model_prodi->get_slider($prodiId);
        // print_r( $data['slider']);
        // exit;
        $data['search_url'] = site_url('f_home/searchpost').'/';
        $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $this->load->view('page/templateprodi', $data);
    }


    public function post_sdm($prodiId)//single post page
    {
        $api = new Aksesapi();
        
        $labId =$this->uri->segment(4);
        $ex = $this->uri->segment(3);
        $exprodiId= explode('_',$ex);
        $prodiId =$exprodiId[0];
        // print_r( $jenis_kryawan);
        // print_r( $exprodiId[1]);
        // exit();
        $this->session->_unit     = $exprodiId[0];
        $this->session->_labId    = $exprodiId[1];

        if ($exprodiId[1] == 11){
            $jenis = 'dosen';
            $is_active = 'Dosen PNS';
        }elseif ($exprodiId[1] == 12){
            $jenis = 'dosenonpns';
            $is_active = 'Dosen Non PNS';
        }elseif ($exprodiId[1] == 22){
            $jenis = 'tendikpns';
            $is_active = 'Pegawai Tendik PNS';
        }else{
            $jenis = 'tendiknonpns';
            $is_active = 'Pegawai Tendik NON PNS';
        }
// print_r($prodiId);
// exit();

        $perpage = 10;
          $offset = $this->uri->segment(4);
        if ($prodiId == in_array($prodiId, array("2","4","8"))) {
            $prodijurusanId = $this->{$this->_model}->get_by_id('f_prodi',['prodiId'=>$prodiId]);

            $getnip = $this->Model_b_karyawan->getDataPagination($perpage, $offset,['prodijurusanId'=>$prodijurusanId->prodijurusanId,'karyawanJenis'=>$jenis])->result();
            $datanew = false;
            if($getnip!==false) 
            {
                foreach ($getnip as $row) 
                {
                    $api = new Aksesapi();
                    $pegawai = $api->Getkaryawan( $row->karyawanNIP);
                    $foto =  $this->{$this->_model}->get_by_id('f_karyawan',['karyawanNIP'=>$row->karyawanNIP]); 
                        if($foto !==false) {
                      $pegawai->datas->karyawanFoto = $foto->karyawanFoto;
                    }else{ $pegawai->datas->karyawanFoto = false; } 
                                                    
                         $datanew[] = $pegawai;
                }
            }
            $data['staff'] = $datanew ;
            $jumlah = $this->Model_b_karyawan->getAll(['prodijurusanId'=>$prodijurusanId->prodijurusanId,'karyawanJenis'=>$jenis])->num_rows();
            $prod = $prodijurusanId->prodijurusanId;
            $data['prodinama'] = $prodijurusanId->prodiNama;
            // print_r($prodiId);
            // exit();
            $config['base_url'] = site_url('prodi/post_sdm/'.$prodiId.'_'.$exprodiId[1].'/');  

        }else{
            $prodijurusanId = $this->{$this->_model}->get_by_id('f_prodi',['prodiId'=>$prodiId]);

            $getnip = $this->Model_b_karyawan->prodi_karyawan_sdm($perpage, $offset,['karyawanProdiId'=>$prodiId],$jenis);
            //   print_r($getnip);
            // exit();
            $datanew = false;
            if($getnip!==false) 
            {
                foreach ($getnip as $row) 
                {
                    $api = new Aksesapi();
                    $pegawai = $api->Getkaryawan($row->karyawanNIP);
                    $foto =  $this->{$this->_model}->get_by_id('f_karyawan',['karyawanNIP'=>$row->karyawanNIP]); 
                     if($foto !==false) {
                      $pegawai->datas->karyawanFoto = $foto->karyawanFoto;
                    }else{ $pegawai->datas->karyawanFoto = false; } 
                       
                         $datanew[] = $pegawai;
                }
            }
            $data['staff'] = $datanew ;
             
            $prod = $prodiId;
            $jumlah = $this->Model_b_karyawan->getAll(['karyawanProdiId'=>$prodiId,'karyawanJenis'=>$jenis])->num_rows();
            $data['prodinama'] = $prodijurusanId->prodiNama;
            $config['base_url'] = site_url('prodi/post_sdm/'.$prod.'_'.$exprodiId[1].'/');  
        }   
    
        $data['datasprod'] = $this->model_prodi->get_id($prodiId);
        
        $config['total_rows'] = $jumlah; 
        $config['per_page'] = $perpage;
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li class="page-item page-link">';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="page-item page-link">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li class="page-item page-link">';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li class="page-item page-link">';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li class="page-item page-link">';
        $config['num_tag_close']    = '</li>';
        $this->pagination->initialize($config);
        $data['offset'] =$offset;
        $data['is_active'] = $is_active; 
        $data['pages'] = 'page/prodi/sdm';
        $data['jurnal'] = $this->Model_jurnal->get( ['jurnamprodiId'=>$prodiId]);
        $data['all'] = $this->model_berita->get(5);
        $data['pin'] = $this->model_berita->get_berita_pin(5);
        $data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
        $data['langganan'] = $this->{$this->_model}->get_langganan(5);
        $stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
        $link = $this->{$this->_model}->get_link();
        $data['link'] = $link;
        $data['informasi'] = $this->{$this->_model}->get_informasi();
        $data['prodiId_select'] = $prodiId;
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
        $data['header'] = $header;
        $prodi = $this->{$this->_model}->get_prodi('f_jurusan');
        $data['prodi'] = $prodi;
        $data['menu'] = menu();
        $lab = $this->{$this->_model}->get_lab('f_lab');
        $data['lab'] = $lab;
        $jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
        $data['jurnam'] = $jurnam;
        $data['foto'] = $this->model_prodi->get_foto($prodiId);
        $data['slider'] = $this->model_prodi->get_slider($prodiId);
        $data['pengumuman'] = $this->model_berita->beritaprodi($prodiId,5);
        $data['search_url'] = site_url('f_home/searchpost').'/';
        $data['searchkaryawan_url'] = site_url('prodi/searchpost').'/';
        $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $data['datas'] = false;
        $data['detail_url'] = site_url('prodi/detail').'/';
        $this->load->view('page/templateprodi', $data);
    }


    public function detail($labId)//single post page
    {

        $api = new Aksesapi();
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));

        $data['is_active'] = '';
        $data['pages'] = 'page/prodi/detail';
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
        $datanip = $this->Model_b_karyawan->sdm_dosen_detail($keyS);
        $data['datassdm'] = $api->Getkaryawan($keyS);
        $data['datas'] = false;
        $data['karyawan'] = $datanip;
        $data['penelitian'] = $api->penelitian_by_id($keyS);
        $data['pengabdian'] = $api->pengabdian_by_id($keyS);
        $NIP = $this->{$this->_model}->get_by_id('f_karyawan',['karyawanNIP'=>$keyS]);
        $prodiNama = $this->model_prodi->get_id($NIP->karyawanProdiId);
        $data['prodinama'] = $prodiNama->prodiNama;
        $data['prodinamajurusan'] = $prodiNama->prodiNamaJurusan;
        $data['informasi'] = $this->{$this->_model}->get_informasi();
        $data['header'] = $header;
        $prodi = $this->{$this->_model}->get_prodi('f_jurusan');
        $data['prodi'] = $prodi;
        $lab = $this->{$this->_model}->get_lab('f_lab');
        $data['lab'] = $lab;
        $jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
        $data['jurnam'] = $jurnam;
        $data['all'] = $this->model_berita->get(5);
        $data['pin'] = $this->model_berita->get_berita_pin(5);
        $data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
        $data['langganan'] = $this->{$this->_model}->get_langganan(5);
        $stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
        $link = $this->{$this->_model}->get_link();
        $data['link'] = $link;
        $data['search_url'] = site_url('f_home/searchpost').'/';
        $data['searchkaryawan_url'] = site_url('sdm/searchpost').'/';
        $data['menu'] = menu(); 
         $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
            $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $this->load->view('page/templateprodi', $data);
    }




    public function biologi()//single post page
    {
        $data['is_active'] = 'prodi';
        $pageNama = preg_replace('/[^A-Za-z0-9\.]/', '', strip_tags($this->uri->segment(3)));
        $data['menu'] = menu();
        $data['pages'] = 'page/prodi/biologi';
        $data['datas'] = $this->model_menu->get_nama($pageNama);
        $data['is_active'] = $data['datas']!=false?$data['datas']->pageHead:'';
        $this->load->view('page/template', $data);
    }

    public function searchpost()
    {
        $prodiId =$this->session->_unit ;
        $labId = $this->session->_labId;

        if ($labId == 11){
            $jenis = 'dosen';
            $is_active = 'Dosen PNS';
        }elseif ($labId == 12){
            $jenis = 'dosenonpns';
            $is_active = 'Dosen Non PNS';
        }elseif ($labId == 22){
            $jenis = 'tendikpns';
            $is_active = 'Pegawai Tendik PNS';
        }else{
            $jenis = 'tendiknonpns';
            $is_active = 'Pegawai Tendik NON PNS';
        }
        $keyword = $this->input->get('searchkaryawan', true);
        $data['prodinama'] =  $this->session->_unitnama  ;
        $prodijurusanId = $this->{$this->_model}->get_by_id('f_prodi',['prodiId'=>$prodiId]);
        // echo "<pre>";
        // print_r ($keyword);
        // echo "</pre>";exit();
        $this->session->keyword = $keyword;
        if ($keyword==NULL) {
            redirect('prodi/sdm');
        }
        $data['offset'] =0;
        $data['keyword'] = $keyword;
        $data['is_active'] = $is_active;    
        $data['pages'] = 'page/prodi/sdm';
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
        $data['header'] = $header;
        $prodi = $this->{$this->_model}->get_prodi('f_jurusan');
        $data['prodi'] = $prodi;
        $lab = $this->{$this->_model}->get_lab('f_lab');
        $data['lab'] = $lab;
        $jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
        $data['jurnam'] = $jurnam;
        $data['data'] = $this->model_berita->get(5);
        $data['all'] = $this->model_berita->get(5);
        $data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
        $data['langganan'] = $this->{$this->_model}->get_langganan(5);
        $stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
        $link = $this->{$this->_model}->get_link();
        $data['link'] = $link;
        $data['search_url'] = site_url($this->_controller_name.'/searchpost').'/';
        $data['searchkaryawan_url'] = site_url('sdm/searchpost').'/';
        $data['menu'] = menu();
        $data['data'] = $this->model_berita->get(5);
        $getnip = $this->Model_b_karyawan->search_prodi($keyword,['karyawanProdiId'=>$prodiId,'karyawanJenis'=>$jenis]);
        $data['informasi'] = $this->{$this->_model}->get_informasi();
        $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $data['detail_url'] = site_url('prodi/detail').'/';
        $data['datas'] = false;
        $api = new Aksesapi();
        $datanew = false;
        if($getnip!==false) 
        {
            foreach ($getnip as $row) 
            {
                    $api = new Aksesapi();
                    $pegawai = $api->Getkaryawan($row->karyawanNIP);
                    $foto =  $this->{$this->_model}->get_by_id('f_karyawan',['karyawanNIP'=>$row->karyawanNIP]); 
                     if($foto !==false) {
                      $pegawai->datas->karyawanFoto = $foto->karyawanFoto;
                    }else{ $pegawai->datas->karyawanFoto = false; } 
                       
                         $datanew[] = $pegawai;
                }
        }
        $data['staff'] = $datanew;
        $this->load->view('page/template',$data);
    }


    public function loadimage()
    {
        $file = $this->uri->segment(3);
        ob_clean();
        $path = FCPATH . '../upload_file/galeri/'. $file;
        $size = getimagesize($path);
        header('Content-Type:' . $size['mime']);
        switch ($size['mime']) {
            case 'image/png':
            $img = imagecreatefrompng($path);

            imagepng($img);
            break;

            default:
            $img = imagecreatefromjpeg($path);
            imagejpeg($img);
            break;
        }
        imagedestroy($img);
    }

    public function loadslider()
    {
        $file = $this->uri->segment(3);
        ob_clean();
        $path = FCPATH . '../upload_file/files/'. $file;
        $files = pathinfo($path, PATHINFO_EXTENSION);
        if ($files == 'mp4') {
            header('Content-type: video/mp4');
            header('Content-Disposition: inline; filename="' . $path . '"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($path);

        }   elseif ($files == 'x-flv') {
            header('Content-type: video/x-flv');
            header('Content-Disposition: inline; filename="' . $path . '"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($path);

        }   else {

            $size = getimagesize($path);
            header('Content-Type:' . $size['mime']);
            switch ($size['mime']) {
                case 'image/png':
                $img = imagecreatefrompng($path);

                imagepng($img);
                break;

                default:
                $img = imagecreatefromjpeg($path);
                imagejpeg($img);
                break;
            }
            imagedestroy($img);
        }
    }

}