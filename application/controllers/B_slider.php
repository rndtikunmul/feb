
<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_slider extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_slider/';
        $this->_path_js = 'b_slider/';
        $this->_judul = 'B_slider';
        $this->_controller_name = 'b_slider';
        $this->_model_name = 'model_b_slider';
        $this->_path_upload = '../upload_file/files/';
        $this->_page_index = 'index';
        $this->_logged_in = $this->session->userdata('logged_in');

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [$this->_path_js . 'b_slider'];
        $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
        if(($this->_logged_in['susrSgroupNama'] == "ADMIN_FEB") or ($this->_logged_in['susrSgroupNama'] == "ADMIN") ){
         $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');
     }else{
        $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi','',['prodijurusanId'=>$jurusan->sgroupProdiId]); 
    }
    $data['user'] = $this->_logged_in['susrSgroupNama'] ;
    $data['show_url'] = site_url($this->_controller_name . '/response') . '/';
    $this->load->view($this->_template, $data);
}

public function response()
{
    $this->form_validation->set_rules('beritaUnit', 'beritaUnit', 'trim|required|xss_clean');

    if ($this->form_validation->run()) {
        if (IS_AJAX) {
            $data['scripts'] = [$this->_path_js . 'b_slider'];
            $data = $this->get_master($this->_path_page.$this->_page_index);
            $beritaUnit = $this->input->post('beritaUnit');
            $this->session->_unit     = $beritaUnit;
            $data['datas'] = $this->{$this->_model_name}->get_slider($beritaUnit);
                // echo $this->db->last_query();
                // exit();
            $data['create_url'] = site_url($this->_controller_name.'/create').'/';
            $data['update_url'] = site_url($this->_controller_name.'/update').'/';
            $data['delete_url'] = site_url($this->_controller_name.'/delete').'/';
            $data['Ya'] = site_url($this->_controller_name .'/publisYa') . '/';
            $data['Tidak'] = site_url($this->_controller_name .'/publisTak') . '/';
            $pages = $this->_path_page . 'response';
            $this->load->view($pages, $data);
        }
    } else {
        message('Ooops!! Something Wrong!!', 'error');
    }
}


public function create()
{

    $data = $this->get_master($this->_path_page . 'form');
    $data['scripts'] = [$this->_path_js . 'b_slider'];
    $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
    $data['status_page'] = 'Create';
    $data['datas'] = false;
    $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
    if(($this->_logged_in['susrSgroupNama'] == "ADMIN_FEB") or ($this->_logged_in['susrSgroupNama'] == "ADMIN") ){
       $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');
   }else{
    $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi','',['prodijurusanId'=>$jurusan->sgroupProdiId]); 
}
$data['user'] = $this->_logged_in['susrSgroupNama'] ;

$this->load->view($this->_template, $data);
}

public function update()
{
    $data = $this->get_master($this->_path_page . 'form');
    $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
    $data['scripts'] = [$this->_path_js . 'b_slider'];
    $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
    $data['status_page'] = 'Update';
    $key = ['sliderId' => $keyS];
    $data['datas'] = $this->{$this->_model_name}->get_by_id('f_slider', $key);
    $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
    if(($this->_logged_in['susrSgroupNama'] == "ADMIN_FEB") or ($this->_logged_in['susrSgroupNama'] == "ADMIN") ){
     $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');
 }else{
    $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi','',['prodijurusanId'=>$jurusan->sgroupProdiId]); 
}
$data['user'] = $this->_logged_in['susrSgroupNama'] ;
$this->load->view($this->_template, $data);
}

public function save()
{
    $sliderIdOld = $this->input->post('sliderIdOld');
    $this->form_validation->set_rules('sliderNama', 'sliderNama', 'trim|xss_clean');
       // $this->form_validation->set_rules('sliderNoUrut', 'sliderNoUrut', 'trim|xss_clean');
    if (empty($_FILES['sliderFiles']['name']) and empty($sliderIdOld))
        $this->form_validation->set_rules('sliderFiles', 'sliderFiles', 'trim|xss_clean');
    if ($this->form_validation->run()) {
        if (IS_AJAX) {
            if (empty($sliderIdOld))
                $string_replace = '/[^a-zA-Z0-9 ]/';
            $sliderNama = str_replace(' ', '-', preg_replace($string_replace, '', (strip_tags($this->input->post('sliderNama')))));
            $sliderFiles = $this->input->post('sliderFiles');
            $sliderUnit = $this->input->post('sliderUnit');
            $konfig = array(
                'url'      => '../upload_file/files/',
                'type'     => 'jpg|jpeg|png|mp4',
                'size'     => 1024 * 25,
                'namafile' => 'slider' . '_' . $sliderNama . '_' . mt_rand(),
                'name' => 'sliderFiles'
            );

            if (!empty($_FILES['sliderFiles']['name'])) {
                $prosesUpload = uploadfile($konfig, $this->_model_name);
            }

            $param = array(
                'sliderFiles' =>  $prosesUpload['file_name'],
                'sliderUnit' =>  $sliderUnit,
                'sliderNama' => $sliderNama,
            );


            if (empty($sliderIdOld)) {
                $proses = $this->{$this->_model_name}->insert('f_slider', $param);
            } else {
                $key = array('sliderId' => $sliderIdOld);
                $proses = $this->{$this->_model_name}->update('f_slider', $param, $key);
            }

            if ($proses)
                message($this->_judul . ' Berhasil Disimpan', 'success');
            else {
                $error = $this->db->error();
                message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
            }
        }
    } else {
        message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
    }
}

public function delete()
{
    $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
    $key = ['sliderId' => $keyS];
    $data['scripts'] = [$this->_path_js . 'b_slider'];
    $proses = $this->{$this->_model_name}->delete('f_slider', $key);
    if ($proses)
        message($this->_judul . ' Berhasil Dihapus', 'success');
    else {
        $error = $this->db->error();
        message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
    }
}


public function publisYa()
{
    if (IS_AJAX) {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [$this->_path_js . 'b_slider'];
        $key = ['sliderId' => $keyS];
        $param = array(
            'sliderIsDisplay' => 1
        );

        $proses = $this->{$this->_model_name}->update('f_slider', $param, $key);
        if ($proses) {
            message('Data Berhasil Di Publis', 'success');
        } else {
            message('Data Tidak Dapat di Simpan, Silahkan cek data', 'error');
        }
    }
}

public function publisTak()
{
    if (IS_AJAX) {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['sliderId' => $keyS];
         $data['scripts'] = [$this->_path_js . 'b_slider'];
        $param = array(
            'sliderIsDisplay' => 2
        );
        $proses = $this->{$this->_model_name}->update('f_slider', $param, $key);
        if ($proses) {
            message('Data Berhasil Di Publish', 'success');
        } else {
            message('Data Tidak Dapat di Simpan, Silahkan cek dataa', 'error');
        } 
    }
}



public function loadimage()
{
    $file = $this->uri->segment(3);
    ob_clean();
    $path = FCPATH . '../upload_file/files/'. $file;
    $size = getimagesize($path);
    header('Content-Type:' . $size['mime']);
    switch ($size['mime']) {
        case 'image/png':
        $img = imagecreatefrompng($path);

        imagepng($img);
        break;

        default:
        $img = imagecreatefromjpeg($path);
        imagejpeg($img);
        break;
    }
    imagedestroy($img);
}
}
