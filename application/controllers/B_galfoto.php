<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_galfoto extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_galfoto/';
        $this->_path_js = 'b_galfoto/';
        $this->_judul = 'Galeri Foto Files';
        $this->_controller_name = 'b_galfoto';
        $this->_model_name = 'model_b_galfoto';
        $this->_path_upload = '../upload_file/galeri/';
        $this->_page_index = 'index';
        $this->_logged_in = $this->session->userdata('logged_in');
        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $session_data = $this->session->userdata('logged_in');
        $user = $this->_logged_in['susrNama'];
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [$this->_path_js . 'b_galfoto'];
        $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
        $data['datas'] = $this->{$this->_model_name}->all_id(['prodijurusanId'=>$jurusan->sgroupProdiId]);
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
         if(($this->_logged_in['susrSgroupNama'] == "ADMIN_FEB") or ($this->_logged_in['susrSgroupNama'] == "ADMIN") ){
             $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');
        }else{
            $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi','',['prodijurusanId'=>$jurusan->sgroupProdiId]); 
        }
        $data['scripts'] = [$this->_path_js . 'b_galfoto'];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['f_head_galeri_foto'] = $this->{$this->_model_name}->get_ref_table('f_head_galeri_foto');
        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
         $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
         if(($this->_logged_in['susrSgroupNama'] == "ADMIN_FEB") or ($this->_logged_in['susrSgroupNama'] == "ADMIN") ){
             $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');
        }else{
            $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi','',['prodijurusanId'=>$jurusan->sgroupProdiId]); 
        }
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['galfId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['f_head_galeri_foto'] = $this->{$this->_model_name}->get_ref_table('f_head_galeri_foto');
        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $session_data = $this->session->userdata('logged_in');
        $galfIdOld = $this->input->post('galfIdOld');
        $this->form_validation->set_rules('galfHeadgalfId', 'galfHeadgalfId', 'trim|xss_clean');
        if (empty($_FILES['galfFiles']['name']) and empty($galfIdOld))
            $this->form_validation->set_rules('galfFiles', 'galfFiles', 'trim|xss_clean');
        $this->form_validation->set_rules('galfNama', 'galfNama', 'trim|xss_clean');
        $this->form_validation->set_rules('galfDatetime', 'galfDatetime', 'trim|xss_clean');
        $this->form_validation->set_rules('galfUserId', 'galfUserId', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                if (empty($galfIdOld))
                    $string_replace = '/[^a-zA-Z0-9 ]/';
                $galfNama = str_replace(' ', '-', preg_replace($string_replace, '', (strip_tags($this->input->post('galfNama')))));
                $galfFiles = $this->input->post('galfFiles');
                $galfHeadgalfId = $this->input->post('galfHeadgalfId');
                $galfDatetime = date('Y-m-d H:i:s');
                $galfUserId = $this->input->post('galfHeadgalfId');;

                $konfig = array(
                    'url'      => '../upload_file/galeri/',
                    'type'     => 'jpg|jpeg|png',
                    'size'     => 1024 * 8,
                    'namafile' => $galfNama . '_' . mt_rand(),
                    'name' => 'galfFiles'
                );

                if (!empty($_FILES['galfFiles']['name'])) {
                    $prosesUpload = uploadfile($konfig, $this->_model_name);
                }

                $param = array(
                    'galfHeadgalfId' => $galfHeadgalfId,
                    'galfFiles' =>  $prosesUpload['file_name'],
                    'galfNama' => $galfNama,
                    'galfDatetime' => $galfDatetime,
                    'galfUserId' => $galfUserId,
                );

                if (empty($galfIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('f_galeri_foto', $param);
                } else {
                    $key = array('galfId' => $galfIdOld);
                    $proses = $this->{$this->_model_name}->update('f_galeri_foto', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['galfId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('f_galeri_foto', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/galeri/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}
}
