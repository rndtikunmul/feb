<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class f_home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->_controller_name = 'f_home';
		$this->_model = 'model_f_master';
		$this->_path_js = 'home';
		$this->load->model('model_berita','',TRUE);
		$this->load->model($this->_model, '', TRUE);
	}

	public function index()
	{
		$this->load->helper('datetoindo');
		$data['is_active'] = 'home';
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$footer = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
		$data['header'] = $header;
		$data['footer'] = $footer;
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$data['all'] = $this->model_berita->get(5);
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['pinhome'] = $this->model_berita->get_berita_pin(4);
		$data['data'] = $this->model_berita->get_beritaaa(4);
		$data['slider'] = $this->{$this->_model}->get_slider();
		$data['fotofb'] = $this->{$this->_model}->get_fotofb(3);	
		$data['youtube'] = $this->{$this->_model}->get_youtube(1);
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$data['jurusan'] = $this->{$this->_model}->get_jurusan(3);
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
		$data['pengumuman'] = $this->model_berita->get_berita('Pengumuman',5);
		$data['berita'] = $this->model_berita->get_berita('Berita',5);
		$data['mahasiswa'] = $this->model_berita->get_berita('Mahasiswa',5);
		$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
		$data['menu'] = menu();
		$prodi = $this->{$this->_model}->get_prodi();
		$data['informasi'] = $this->{$this->_model}->get_informasi();
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['search_url'] = site_url($this->_controller_name.'/searchpost').'/';
		$this->load->view('front/home/template', $data);
	}

	public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

	public function load_content()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/content/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}



	public function loadthumb()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/thumb/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

	public function loadslider()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/files/'. $file;
		$files = pathinfo($path, PATHINFO_EXTENSION);
		if ($files == 'mp4') {
            header('Content-type: video/mp4');
            header('Content-Disposition: inline; filename="' . $path . '"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($path);

        }   elseif ($files == 'x-flv') {
            header('Content-type: video/x-flv');
            header('Content-Disposition: inline; filename="' . $path . '"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($path);

        }   else {

		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
		}
	}

	public function searchpost()
	{

		$keyword = $this->input->get('search', true);
		// echo "<pre>";
		// print_r ($keyword);
		// echo "</pre>";exit();
		$this->session->keyword = $keyword;
		if ($keyword==NULL) {
			redirect('f_home');
		}
		$data['keyword'] = $keyword;
		$data['is_active'] = 'ragam';	
		$data['pages'] = 'page/search/view';
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['data'] = $this->model_berita->get(5);
		$data['all'] = $this->model_berita->get(5);
		$data['search_url'] = site_url($this->_controller_name.'/searchpost').'/';
		// $data['datas'] = $this->model_berita->get_berita();
        $data['menu'] = menu();
		$data['data'] = $this->model_berita->get(5);
		$data['datassearch']= $this->model_berita->search($keyword);
		$data['informasi'] = $this->{$this->_model}->get_informasi();
		$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
		$data['datas'] = false;
		$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
		$this->load->view('page/template',$data);
	}
}
