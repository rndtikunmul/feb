<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Lab extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_controller_name = 'lab';
		$this->_model = 'model_f_master';
		$this->load->model('model_berita','',TRUE);
        $this->load->model('model_b_berita','',TRUE);
        $this->load->model('model_prodi','',TRUE);
		$this->load->model($this->_model, '', TRUE);
	}

	public function index()//single post page
    {
    	redirect('/home', 'refresh');
        $data['currentPage'] = false;
    }

	public function post($labId)//single post page
    {
        $data['is_active'] = 'lab';
        $data['pages'] = 'page/lab/post';
        $data['datas'] = $this->model_prodi->get_lab_id($labId);
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
        $data['prodi'] = $prodi;
        $lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
        $data['menu'] = menu();        
        $data['search_url'] = site_url('f_home/searchpost').'/';
        // $data['kegiatan'] = $this->model_berita->get_berita('KEGIATAN',5);
		// $data['pengumuman'] = $this->model_berita->get_berita('PENGUMUMAN',5);
        $this->load->view('page/template', $data);
    }

    public function biologi()//single post page
    {
        $data['is_active'] = 'prodi';
    	$pageNama = preg_replace('/[^A-Za-z0-9\.]/', '', strip_tags($this->uri->segment(3)));
        $data['menu'] = menu();
        $data['pages'] = 'page/prodi/biologi';
        // $data['kegiatan'] = $this->model_berita->get_berita('KEGIATAN',5);
		// $data['pengumuman'] = $this->model_berita->get_berita('PENGUMUMAN',5);
        $data['datas'] = $this->model_menu->get_nama($pageNama);
        $data['is_active'] = $data['datas']!=false?$data['datas']->pageHead:'';
        $this->load->view('page/template', $data);
    }

}