<?php
defined('BASEPATH') or exit('No direct script access allowed');
class upload extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->_page_index = 'index';
        $this->_logged_in = $this->session->userdata('logged_in');
    }

    public function index()
    {
        $session_data = $this->session->userdata('logged_in');
        if (!empty($_FILES['upload']['name'])) {
            $config['upload_path'] = realpath(APPPATH . '../upload_file/content/'); //path folder
            $config['allowed_types'] = 'jpg|jpeg|png'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] = 1024 * 5; //maksimum besar file 5M
            $config['file_name'] = 'content_berita' . mt_rand(); //nama yang terupload nantinya
            $this->load->library('upload', $config);
                if (!$this->upload->do_upload('upload'))
                        echo message(strip_tags($this->upload->display_errors()), 'error');
                else{
                    
                $file = $this->upload->data();
                $config['source_image']= realpath(APPPATH . '../upload_file/content/'.$file['file_name']) ;
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['width']= 600;
                $config['height']= 400;
                // $config['upload_path']=realpath(APPPATH . '../upload_file/content/resize/'.$file['file_name']);
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                        $image_path = $file['full_path'];
                        if (file_exists($image_path)) 
                        {
                            $upload = true;
                            $f_name =$file['file_name'] ;
                            $CKEditorFuncNum = $_GET['CKEditorFuncNum']; 
                            $url = base_url('f_home/load_content/').$f_name;
                            $message = ''; 
                            echo "<script>window.parent.CKEDITOR.tools.callFunction('$CKEditorFuncNum','$url','$message')</script>";

                        } else {
                            $upload = false;
                            echo message('error');
                        }
                    }
                }
            }
}
