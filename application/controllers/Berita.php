<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class berita extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_controller_name = 'berita';
		$this->_model = 'model_f_master';
		$this->load->model('model_berita','',TRUE);
		$this->load->model('model_b_berita','',TRUE);
		$this->load->model($this->_model, '', TRUE);
	}

	public function index()
	{	
		$this->load->helper('datetoindo');
		$data['datas'] = false;
    	$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
		$data['is_active'] = 'Berita';	
		$data['pages'] = 'page/berita/view';
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['informasi'] = $this->{$this->_model}->get_informasi();
		$data['jurnam'] = $jurnam;
		$data['data'] = $this->model_berita->get(5);
		$data['all'] = $this->model_berita->get(5);
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
		$perpage = 5;
	    $offset = $this->uri->segment(3);
	    $data['berita'] =$this->model_berita->getDataPagination($perpage, $offset,'berita')->result();
	    $config['base_url'] = site_url('berita/index/');
	    $config['total_rows'] = $this->model_berita->getAll('berita')->num_rows();
		$config['per_page'] = $perpage;
		$config['full_tag_open']    = '<ul class="pagination">';
		$config['full_tag_close']   = '</ul>';
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['first_tag_open']   = '<li class="page-item page-link">';
		$config['first_tag_close']  = '</li>';
		$config['prev_link']        = '&laquo';
		$config['prev_tag_open']    = '<li class="page-item page-link">';
		$config['prev_tag_close']   = '</li>';
		$config['next_link']        = '&raquo';
		$config['next_tag_open']    = '<li class="page-item page-link">';
		$config['next_tag_close']   = '</li>';
		$config['last_tag_open']    = '<li class="page-item page-link">';
		$config['last_tag_close']   = '</li>';
		$config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
		$config['cur_tag_close']    = '</a></li>';
		$config['num_tag_open']     = '<li class="page-item page-link">';
		$config['num_tag_close']    = '</li>';
     	$this->pagination->initialize($config);
		$data['search_url'] = site_url('f_home/searchpost').'/';
		$data['keyword'] = FALSE;
		$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['menu'] = menu();
		$this->load->view('page/template', $data);
	}

	public function post($beritaNama)//single post page
    {
    	$this->load->helper('datetoindo');
    	$data['datas'] = false;
    	$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
		$data['is_active'] = 'Berita';
        $data['pages'] = 'page/berita/post';
        $data['data'] = $this->model_berita->get(5);
		$data['datasberita'] = $this->model_berita->get_nama($beritaNama);
		$data['all'] = $this->model_berita->get(5);
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['informasi'] = $this->{$this->_model}->get_informasi();
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['keyword'] = FALSE;
		$data['search_url'] = site_url($this->_controller_name.'/searchpost').'/';
		$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['menu'] = menu();
        $this->load->view('page/template', $data);
	}

	public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}
	
	public function loadthumb()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/thumb/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

	
}