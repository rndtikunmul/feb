<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Informasi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_controller_name = 'prodi';
		$this->_model = 'model_f_master';
		$this->load->model('model_berita','',TRUE);
        $this->load->model('model_b_berita','',TRUE);
        $this->load->model('model_prodi','',TRUE);
        $this->load->model('model_b_galfoto','',TRUE);
        $this->load->model('Model_jurnal', '', TRUE);
		$this->load->model('Model_b_karyawan', '', TRUE);
		$this->load->model($this->_model, '', TRUE);
	}

	public function index()//single post page
    {
    	redirect('/home', 'refresh');
        $data['currentPage'] = false;
        $data['search_url'] = site_url('f_home/searchpost').'/';
    }

	public function post($prodiId)//single post page
    {
    	$prodiId = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
    	$data['datas'] = false;
    	$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
    	$infoNama = $this->{$this->_model}->get_informasi_id($prodiId);
        $data['is_active'] = $infoNama->infodaftarNama;
        $data['informasi'] = $this->{$this->_model}->get_informasi();
        $data['pages'] = 'page/informasi/post';
        $data['datasinfo'] = $infoNama;
      	$data['jurnal'] = $this->Model_jurnal->get( ['jurnamprodiId'=>$prodiId]);
		$data['all'] = $this->model_berita->get(5);
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['prodiId_select'] = $prodiId;
		if ($prodiId == in_array($prodiId, array("2","4","8"))) {
		 $prodijurusanId = $this->{$this->_model}->get_by_id('f_prodi',['prodiId'=>$prodiId]);
		 $data['staff'] = $this->Model_b_karyawan->prodi_karyawan(['prodijurusanId'=>$prodijurusanId->prodijurusanId]);	
		 $data['prorjur_select'] =  $prodijurusanId->prodijurusanId;
		}else{
		$data['staff'] = $this->Model_b_karyawan->prodi_karyawan(['karyawanProdiId'=>$prodiId]);
		 $data['prorjur_select'] =  $prodiId;	
		}
       	$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
        $data['menu'] = menu();
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
        $lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
        $data['foto'] = $this->model_prodi->get_foto_prodi($prodiId);
        $data['slider'] = $this->model_prodi->get_slider($prodiId);
		$data['pengumuman'] = $this->model_berita->beritaprodi($prodiId,5);
		$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
		$data['search_url'] = site_url('f_home/searchpost').'/';
        $this->load->view('page/template', $data);
    }

   
    public function searchpost()
	{
		$prodiId =$this->session->_unit ;
    	$labId = $this->session->_labId;
		   
    	if ($labId == 11){
        	$jenis = 'dosen';
        	$is_active = 'Dosen PNS';
        }elseif ($labId == 12){
        	$jenis = 'dosenonpns';
        	$is_active = 'Dosen Non PNS';
        }elseif ($labId == 22){
        	$jenis = 'tendikpns';
        	$is_active = 'Pegawai Tendik PNS';
        }else{
        	$jenis = 'tendiknonpns';
        	$is_active = 'Pegawai Tendik NON PNS';
        }
		$keyword = $this->input->get('searchkaryawan', true);
		$prodijurusanId = $this->{$this->_model}->get_by_id('f_prodi',['prodiId'=>$prodiId]);
		$this->session->keyword = $keyword;
		if ($keyword==NULL) {
			redirect('prodi/sdm');
		}
		$data['offset'] =0;
		$data['keyword'] = $keyword;
		$data['is_active'] = $is_active;	
		$data['pages'] = 'page/prodi/sdm';
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
		$data['data'] = $this->model_berita->get(5);
		$data['all'] = $this->model_berita->get(5);
		$data['search_url'] = site_url($this->_controller_name.'/searchpost').'/';
		$data['searchkaryawan_url'] = site_url('sdm/searchpost').'/';
		$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['menu'] = menu();
		$data['data'] = $this->model_berita->get(5);
		$this->load->view('page/template',$data);
	}


    public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/galeri/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

	public function loadslider()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/files/'. $file;
		$files = pathinfo($path, PATHINFO_EXTENSION);
		if ($files == 'mp4') {
            header('Content-type: video/mp4');
            header('Content-Disposition: inline; filename="' . $path . '"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($path);

        }   elseif ($files == 'x-flv') {
            header('Content-type: video/x-flv');
            header('Content-Disposition: inline; filename="' . $path . '"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($path);

        }   else {

		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
		}
	}
	
}