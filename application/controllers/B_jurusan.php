
<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_jurusan extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_jurusan/';
        $this->_path_js = null;
        $this->_judul = 'Jurusan';
        $this->_controller_name = 'b_jurusan';
        $this->_model_name = 'model_b_jurusan';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('f_jurusan');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['jurusanId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('f_jurusan', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $jurusanIdOld = $this->input->post('jurusanIdOld');
        $this->form_validation->set_rules('jurusanNama', 'jurusanNama', 'trim|xss_clean');
        $this->form_validation->set_rules('jurusanDesk', 'jurusanDesk', 'trim|xss_clean');
        $this->form_validation->set_rules('jurusanVisi', 'jurusanVisi', 'trim|xss_clean');
        $this->form_validation->set_rules('jurusanMisi', 'jurusanMisi', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $jurusanNama = $this->input->post('jurusanNama');
                $jurusanDesk = $this->input->post('jurusanDesk');
                $jurusanVisi = $this->input->post('jurusanVisi');
                $jurusanMisi = $this->input->post('jurusanMisi');


                $param = array(
                    'jurusanNama' => $jurusanNama,
                    'jurusanDesk' => $jurusanDesk,
                    'jurusanVisi' => $jurusanVisi,
                    'jurusanMisi' => $jurusanMisi,

                );

                if (empty($jurusanIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('f_jurusan', $param);
                } else {
                    $key = array('jurusanId' => $jurusanIdOld);
                    $proses = $this->{$this->_model_name}->update('f_jurusan', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['jurusanId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('f_jurusan', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
