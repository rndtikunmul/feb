<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Publikasi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_controller_name = 'Publikasi_buku';
		$this->_model = 'model_f_master';
		$this->load->model('model_kerjasama','',TRUE);
		$this->load->model('model_berita','',TRUE);
		$this->load->model('model_b_berita','',TRUE);
		$this->load->model($this->_model, '', TRUE);
		$this->load->library('Aksesapi');
	}

	    private function semId()
    {
        return strval(date('Y') - ((date('n') <= 6) ? 1 : 0)) . strval(((date('n') <= 6) ? '2' : '1'));
    }


	public function publikasi($jenis)//single post page
    {
    	$this->load->helper('datetoindo');
		$data['is_active'] = '';
		$jenis = $this->uri->segment(3);
		if ($jenis == 'penelitian') {
			$data['jenis_publikasi'] = 'Penelitian';
			$jenbkd = 'penelitian';
			$group = '' ;
		}elseif ($jenis == 'PM') {
			$data['jenis_publikasi'] = 'Pengabdian Masyarakat';
			$jenbkd = 'PM';
			$group = '' ;
		}elseif ($jenis == 'jurnal') {
			$data['jenis_publikasi'] = 'Jurnal';
			$jenbkd = 'jurnal';
			$group ='form_2';
		}elseif ($jenis == 'buku') {
			$data['jenis_publikasi'] = 'Buku';
			$jenbkd = 'buku';
			$group ='form_1';
		}elseif ($jenis == 'haki') {
			$data['jenis_publikasi'] = 'Hak Kekayaan Intelektual';
			$jenbkd = 'hki';
			$group ='form_6';
		}elseif ($jenis == 'seminar') {
			$data['jenis_publikasi'] = 'Seminar';
			$jenbkd = 'seminar';
			$group ='form_3';
		}
		$api = new Aksesapi();
		$data['jenis_pub'] =$api->JenisPublikasi($jenbkd);
		// print_r($data['jenis_pub']);exit();
		$tahun = $this->input->post('tahun');
		$jenisp = $this->input->post('jenisp');
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(4);
		$data['langganan'] = $this->{$this->_model}->get_langganan(1);
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
		$perpage = 10;
		$linktahun = $this->uri->segment(4);
		$linkexplode = $this->uri->segment(4);
	    $offset = $this->uri->segment(5);
		$exjenisp = explode('-', $this->input->post('jenisp'));
		$exjenispexlpode = explode('-',$linkexplode);
		
		if ($exjenisp[0] == '%'){
		$cekpublikasi = $api->publikasi($tahun,'',$group,$perpage, $offset);	
		}else{
		$cekpublikasi = $api->publikasi($tahun,$exjenisp[0],'',$perpage, $offset);
		}
		$cekpm = $api->pengabdian($tahun,$perpage, $offset);
		$cekpenelitian = $api->penelitian($tahun,$perpage, $offset);

		if ($jenis == 'penelitian') {
			 	if ($tahun == false  or   $cekpenelitian->status == false and $linktahun != false)
			 	{
			 		
					 	  if ($linktahun == false){
					 	  	$data['dataspublikasi'] =$api->penelitian(date('Y'),$perpage, $offset);
					 		$jumlah =$api->penelitian(date('Y'),'','');
					 		$data['tahun'] = date('Y');
					 		$data['jenisp'] = 'Penelitian';
					 	    $jum = $jumlah->status!=false?count($jumlah->data ):'';
					 	  
				 
					 	}else{
					 		$data['dataspublikasi'] = $api->penelitian($linktahun,$perpage, $offset);
					 		$jumlah =$api->penelitian($linktahun,'','');
					 		$data['tahun'] = $linktahun;
					 		$data['jenisp'] = 'Penelitian';
					 		$jum = $jumlah->status!=false?count($jumlah->data ):'';
					 	}
			 	
			 	}else{
				 		$data['dataspublikasi'] = $cekpenelitian ;
				 		$jumlah =$api->penelitian($tahun,'','');
				 		$data['tahun'] = $tahun;
				 		$data['jenisp'] = 'Penelitian';
				 		$jum = $jumlah->status!=false?count($jumlah->data ):'';
			 	}
			 	
			 }elseif ( $jenis == 'PM') {
			 
			 	if ($tahun == false or   $cekpm->status == false and $linktahun != false)
			 	{

			 		  if ($linktahun == false){
			 		  		$data['dataspublikasi'] = $api->pengabdian(date('Y'),$perpage, $offset);
					 		$data['tahun'] = date('Y');
					 		$data['tahunpm'] =date('Y');
					 		$data['jenisp'] ='Pengabdian Masyarakat';
					 		$jumlah =$api->pengabdian(date('Y'),'','');
					 		$jum = $jumlah->status!=false?count($jumlah->data ):'';

			 		  }else{
			 		  		$data['dataspublikasi'] = $api->pengabdian($linktahun,$perpage, $offset);
					 		$jumlah =$api->pengabdian($linktahun,'','');
					 		$data['tahun'] = $linktahun;
					 		$data['tahunpm'] =$linktahun;
					 		$data['jenisp'] = 'Pengabdian Masyarakat';
					 		$jum = $jumlah->status!=false?count($jumlah->data ):'';

			 		  }
			 	
			 	}else{
			 		$data['dataspublikasi'] = $cekpm ;
			 		$data['tahun'] = $tahun;
			 		$data['tahunpm'] =$tahun;
			 		$data['jenisp'] ='Pengabdian Masyarakat';
			 		$jumlah =$api->pengabdian($tahun,'','');
			 		$jum = $jumlah->status!=false?count($jumlah->data ):'';

			 	}
			 }else { 
				 	if ($tahun == false or $jenisp == false  or   $cekpublikasi->status == false and $linktahun != false)
				 	{

				 		 if ($linktahun == false){
					 	  	$data['dataspublikasi'] =  $api->publikasi(date('Y'),'',$group,$perpage, $offset);
					 		$jumlah =$api->publikasi(date('Y'),'',$group,'','');
					 		$data['tahun'] = date('Y');
					 		$data['jenisp'] = $data['jenis_publikasi'];
					 	    $jum = $jumlah->status!=false?count($jumlah->data ):'';
				 
					 	}else{
					 		if ($exjenispexlpode[1] == false){
							$cekpublikasi = $api->publikasi($exjenispexlpode[0],'',$group,$perpage, $offset);	
							$jumlah =$api->publikasi($exjenispexlpode[0],'',$group,'','');
							}else{
							$cekpublikasi = $api->publikasi($exjenispexlpode[0],$exjenispexlpode[1],'',$perpage, $offset);
							$jumlah =$api->publikasi($exjenispexlpode[0],$exjenispexlpode[1],'','','');
							}
					 		$data['dataspublikasi'] = $cekpublikasi;
					 		$data['tahun'] = $exjenispexlpode[0];
					 		$data['jenisp'] =$data['jenis_publikasi'];
					 		$jum = $jumlah->status!=false?count($jumlah->data ):'';
					 	}
				 	}else{

				 		$data['dataspublikasi'] = $cekpublikasi;
				 		$data['tahun'] = $tahun;
				 		$data['jenisp'] = $data['jenis_publikasi'];
				 		   if ($exjenisp[0] =='%'){
                            $jumlah =$api->publikasi($tahun,'',$group,'','');    
                            }else{
                             $jumlah =$api->publikasi($tahun,$exjenisp[0],'','','');
                            }

				 		$jum = $jumlah->status!=false?count($jumlah->data ):'';

				 	}

			 }

			 if (($jenis == 'penelitian') or ($jenis == 'PM') ){
				 if ($linktahun == false){
				 	$config['base_url'] = site_url('publikasi/publikasi/'.$jenbkd.'/'.$data['tahun'].'/');
				 }else{
				 	$config['base_url'] = site_url('publikasi/publikasi/'.$jenbkd.'/'.$linktahun.'/');
				 }
			}else{
				if ($exjenisp[0] =='%'){
					$exjenisp[0] = '';
				}else{
					$exjenisp[0] = $exjenisp[0];
				}
				 if ($linktahun == false){
				 	
				 	$config['base_url'] = site_url('publikasi/publikasi/'.$jenbkd.'/'.$data['tahun'].'-'.$exjenisp[0].'/');
				 }else{
				 	$config['base_url'] = site_url('publikasi/publikasi/'.$jenbkd.'/'.$linktahun.'-'.$exjenisp[0].'/');
				 }

			}

	    $config['total_rows'] =  $jum;
		$config['per_page'] = $perpage;
		$config['full_tag_open']    = '<ul class="pagination">';
		$config['full_tag_close']   = '</ul>';
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['first_tag_open']   = '<li class="page-item page-link">';
		$config['first_tag_close']  = '</li>';
		$config['prev_link']        = '&laquo';
		$config['prev_tag_open']    = '<li class="page-item page-link">';
		$config['prev_tag_close']   = '</li>';
		$config['next_link']        = '&raquo';
		$config['next_tag_open']    = '<li class="page-item page-link">';
		$config['next_tag_close']   = '</li>';
		$config['last_tag_open']    = '<li class="page-item page-link">';
		$config['last_tag_close']   = '</li>';
		$config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
		$config['cur_tag_close']    = '</a></li>';
		$config['num_tag_open']     = '<li class="page-item page-link">';
		$config['num_tag_close']    = '</li>';
     	$this->pagination->initialize($config);
        $data['offset'] =$offset;

	    $data['menu'] = menu();
        $data['pages'] = 'page/publikasi/view';
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['all'] = $this->model_berita->get(5);	
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(4);
		$data['langganan'] = $this->{$this->_model}->get_langganan(1);
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;	
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(4);
		$data['langganan'] = $this->{$this->_model}->get_langganan(1);
		$link = $this->{$this->_model}->get_link();
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$data['link'] = $link;
        // $data['nasional'] = $this->model_prestasimhs->nasional();
        $data['informasi'] = $this->{$this->_model}->get_informasi();
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['search_url'] = site_url('f_home/searchpost').'/';
		$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['datas'] = false;
        $data['menu'] = menu();
        $data['searchpublikasi_url'] = site_url('Publikasi/publikasi/').$jenis.'/';
        $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $this->load->view('page/template', $data);
    }

	public function post($labId)//single post page
    {
    	$this->load->helper('datetoindo');
    	$data['datas'] = false;
		$data['pages'] = 'page/publikasi/response';

			$data['is_active'] = 'Publikasi';
		$jenis = $this->uri->segment(3);
		if ($jenis == 'penelitian') {
			$data['jenis_publikasi'] = 'Penelitian';
		}elseif ($jenis == 'PM') {
			$data['jenis_publikasi'] = 'Pengabdian Masyarakat';
		}elseif ($jenis == 'jurnal') {
			$data['jenis_publikasi'] = 'Jurnal';
		}elseif ($jenis == 'buku') {
			$data['jenis_publikasi'] = 'Buku';
		}elseif ($jenis == 'haki') {
			$data['jenis_publikasi'] = 'Hak Kekayaan Intelektual';
		}elseif ($jenis == 'conference') {
			$data['jenis_publikasi'] = 'conference';
		}

	    $perpage = 10;
	    $offset = $this->uri->segment(4);
	    if ($labId == 1){
	    	$jenisk = 'Dalam Negeri';
	    }else{
	    	$jenisk = 'Luar Negeri';
	    }
	    $data['jeniskerjasama'] = $this->model_kerjasama->jenis($perpage,$offset,$jenisk)->result();
	    //  echo $this->db->last_query();
	    // // print_r(   $data['jeniskerjasama'] );
	    // exit();
	    $config['base_url'] = site_url('kerjasama/post/'.$jenisk.'/');
	    $config['total_rows'] = $this->model_kerjasama->getAll($jenisk)->num_rows();
	   
		$config['per_page'] = $perpage;
		$config['full_tag_open']    = '<ul class="pagination">';
		$config['full_tag_close']   = '</ul>';
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['first_tag_open']   = '<li class="page-item page-link">';
		$config['first_tag_close']  = '</li>';
		$config['prev_link']        = '&laquo';
		$config['prev_tag_open']    = '<li class="page-item page-link">';
		$config['prev_tag_close']   = '</li>';
		$config['next_link']        = '&raquo';
		$config['next_tag_open']    = '<li class="page-item page-link">';
		$config['next_tag_close']   = '</li>';
		$config['last_tag_open']    = '<li class="page-item page-link">';
		$config['last_tag_close']   = '</li>';
		$config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
		$config['cur_tag_close']    = '</a></li>';
		$config['num_tag_open']     = '<li class="page-item page-link">';
		$config['num_tag_close']    = '</li>';
     	$this->pagination->initialize($config);
        $data['is_active'] = $jenisk;	
        $data['offset'] =$offset;
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$data['informasi'] = $this->{$this->_model}->get_informasi();
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['all'] = $this->model_berita->get(5);
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(4);
		$data['langganan'] = $this->{$this->_model}->get_langganan(1);
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
		$data['search_url'] = site_url('f_home/searchpost').'/';
		$data['searchkaryawan_url'] = site_url('kerjasama/searchpost').'/';
		$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['menu'] = menu(); 
        $this->load->view('page/template', $data);
    }


	public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}
	
	public function loadthumb()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/thumb/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

	 public function loadattach()
    {
        ob_clean();
        $this->load->helper('file');
        $file = $this->uri->segment(3);
        $path = FCPATH . '../upload_file/berita/' . $file;
        $files = get_mime_by_extension($path);
        // echo $files;
        // exit();
        if ($files == 'application/pdf') {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $path . '"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($path);

        }   elseif 
            ($files == 'application/msword') {
            header('Content-type: application/msword');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    

        }   elseif 
            ($files == 'application/vnd.ms-excel') {
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path); 
            
        }   elseif 
            ($files == 'application/vnd.ms-powerpoint') {
            header('Content-type: application/vnd.ms-powerpoint');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    

        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
            header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);
            
        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    
    
        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
            header('Content-type: application/vnd.openxmlformats-officedocument.presentationml.presentation');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);
            
        }   else {
            $size = getimagesize($path);
            header('Content-Type:' . $size['mime']);
            switch ($size['mime']) {
                case 'image/png':
                    $img = imagecreatefrompng($path);

                    imagepng($img);
                    break;

                default:
                    $img = imagecreatefromjpeg($path);
                    imagejpeg($img);
                    break;
            }
            imagedestroy($img);
        }
    }

	public function searchpost()
	{

		$keyword = $this->input->get('search', true);
		// echo "<pre>";
		// print_r ($keyword);
		// echo "</pre>";exit();
		$this->session->keyword = $keyword;
		if ($keyword==NULL) {
			redirect('publikasi');
		}
		$data['offset'] =0;
		$data['keyword'] = $keyword;
		$data['is_active'] = 'publikasi';	
		$data['pages'] = 'page/publikasi/response';
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(4);
		$data['langganan'] = $this->{$this->_model}->get_langganan(1);
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
		$data['data'] = $this->model_berita->get(5);
		$data['all'] = $this->model_berita->get(5);
		$data['search_url'] = site_url($this->_controller_name.'/searchpost').'/';
		$data['searchkaryawan_url'] = site_url('kerjasama/searchpost').'/';
        $data['menu'] = menu();
		$data['data'] = $this->model_berita->get(5);
        $data['jeniskerjasama'] = $this->model_kerjasama->cari_kerjasama($keyword);
        $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $data['datas'] = false;
		$this->load->view('page/template',$data);
	}
}