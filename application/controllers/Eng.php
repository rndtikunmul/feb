<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eng extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->_controller_name = 'f_home';
		$this->_model = 'model_f_master';
		$this->_path_js = 'home';
		$this->load->model('model_berita','',TRUE);
		$this->load->model($this->_model, '', TRUE);
		$this->load->model('model_kerjasama','',TRUE);
		$this->load->model('model_prodi','',TRUE);
		$this->load->model('Model_b_karyawan', '', TRUE);
		$this->load->library('Aksesapi');
	}
   private function semId()
    {
        return strval(date('Y') - ((date('n') <= 6) ? 1 : 0)) . strval(((date('n') <= 6) ? '2' : '1'));
    }
	public function index()
	{
		$this->load->helper('datetoindo');
		$data['jenisbhs'] = $this->uri->segment(1);
		$data['is_active'] = 'home';
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$footer = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['header'] = $header;
		$data['footer'] = $footer;
		$data['stat'] = $stat;
		$data['all'] = $this->model_berita->get(5);
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['pinhome'] = $this->model_berita->get_berita_pin(4);
		$data['data'] = $this->model_berita->get_beritaaa(4);
		$data['slider'] = $this->{$this->_model}->get_slider();
		$data['fotofb'] = $this->{$this->_model}->get_fotofb(3);	
		$data['youtube'] = $this->{$this->_model}->get_youtube(1);
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$data['jurusan'] = $this->{$this->_model}->get_jurusan(3);
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
		$data['pengumuman'] = $this->model_berita->get_berita('Pengumuman',5);
		$data['berita'] = $this->model_berita->get_berita('Berita',5);
		$data['mahasiswa'] = $this->model_berita->get_berita('Mahasiswa',5);
		$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
		$data['menu'] = menueng();
		$prodi = $this->{$this->_model}->get_prodi();
		$data['informasi'] = $this->{$this->_model}->get_informasi();
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['search_url'] = site_url($this->_controller_name.'/searchpost').'/';
		$this->load->view('front/home/templateng', $data);
	}

	public function post()//single post page
    {
        $data['is_active'] = 'pages';
    	$pageNama = preg_replace('/[^A-Za-z0-9\.]/', '', strip_tags($this->uri->segment(3)));
        $data['menu'] = menueng();
        $data['pages'] = 'page/menu/postEng';
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['all'] = $this->model_berita->get(5);
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
        $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['search_url'] = site_url('f_home/searchpost').'/';
        $data['informasi'] = $this->{$this->_model}->get_informasi();
        $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        // $data['kegiatan'] = $this->model_berita->get_berita('KEGIATAN',5);
		// $data['pengumuman'] = $this->model_berita->get_berita('PENGUMUMAN',5);
        $data['datas'] = $this->model_menu->get_nama($pageNama);
        // print_r($datas);
		// exit();	
        $data['is_active'] = $data['datas']!=false?$data['datas']->pageHead:'';
		$this->load->view('page/templateng', $data);
    }

    public function publikasi($jenis)//single post page
    {
    	$this->load->helper('datetoindo');
		$data['is_active'] = 'Publication';
	    $jenis = $this->uri->segment(3);
        if ($jenis == 'penelitian') {
            $data['jenis_publikasi'] = 'Research';
            $jenbkd = 'Research';
            $jenispilihan = 'penelitian';
            $group = '' ;
        }elseif ($jenis == 'PM') {
            $data['jenis_publikasi'] = 'Community Dedication';
            $jenbkd = 'Community Dedication';
             $jenispilihan = 'PM';
             $group = '' ;
        }elseif ($jenis == 'jurnal') {
            $data['jenis_publikasi'] = 'Journal';
            $jenbkd = 'jurnal';
            $jenispilihan = 'jurnal';
                $group ='form_2';
        }elseif ($jenis == 'buku') {
            $data['jenis_publikasi'] = 'BOOK';
            $jenbkd = 'book';
            $jenispilihan = 'buku';
            $group ='form_1';
        }elseif ($jenis == 'haki') {
            $data['jenis_publikasi'] = 'HAKI';
            $jenbkd = 'haki';
            $jenispilihan = 'hki';
            $group ='form_6';
        }elseif ($jenis == 'conference') {
            $data['jenis_publikasi'] = 'Conference';
            $jenbkd = 'conference';
            $jenispilihan = 'seminar';
            $group ='form_3';

        }
        $api = new Aksesapi();
        $data['jenis_pub'] =$api->JenisPublikasi($jenispilihan);
        $tahun = $this->input->post('tahun');
        $jenisp = $this->input->post('jenisp');
        $perpage = 10;
        $linktahun = $this->uri->segment(4);
        $linkexplode = $this->uri->segment(4);
        $offset = $this->uri->segment(5);
        $exjenisp = explode('-', $this->input->post('jenisp'));
        $exjenispexlpode = explode('-',$linkexplode);
        if ($exjenisp[0] == '%'){
        $cekpublikasi = $api->publikasi($tahun,'',$group,$perpage, $offset);    
        }else{
        $cekpublikasi = $api->publikasi($tahun,$exjenisp[0],'',$perpage, $offset);
        }

        $cekpm = $api->pengabdian($tahun,$perpage, $offset);
        $cekpenelitian = $api->penelitian($tahun,$perpage, $offset);

       if ($jenis == 'penelitian') {
                if ($tahun == false  or   $cekpenelitian->status == false and $linktahun != false)
                {
                    
                          if ($linktahun == false){
                            $data['dataspublikasi'] =$api->penelitian(date('Y'),$perpage, $offset);
                            $jumlah =$api->penelitian(date('Y'),'','');
                            $data['tahun'] = date('Y');
                            $data['jenisp'] = $data['jenis_publikasi'];
                            $jum = $jumlah->status!=false?count($jumlah->data ):'';
                          
                 
                        }else{
                            $data['dataspublikasi'] = $api->penelitian($linktahun,$perpage, $offset);
                            $jumlah =$api->penelitian($linktahun,'','');
                            $data['tahun'] = $linktahun;
                            $data['jenisp'] =$data['jenis_publikasi'];
                            $jum = $jumlah->status!=false?count($jumlah->data ):'';
                        }
                
                }else{
                        $data['dataspublikasi'] = $cekpenelitian ;
                        $jumlah =$api->penelitian($tahun,'','');
                        $data['tahun'] = $tahun;
                        $data['jenisp'] =$data['jenis_publikasi'];
                        $jum = $jumlah->status!=false?count($jumlah->data ):'';
                }
                
             }elseif ( $jenis == 'PM') {
             
                if ($tahun == false or   $cekpm->status == false and $linktahun != false)
                {

                      if ($linktahun == false){
                            $data['dataspublikasi'] = $api->pengabdian(date('Y'),$perpage, $offset);
                            $data['tahun'] = date('Y');
                            $data['tahunpm'] =date('Y');
                            $data['jenisp'] ='Pengabdian Masyarakat';
                            $jumlah =$api->pengabdian(date('Y'),'','');
                            $jum = $jumlah->status!=false?count($jumlah->data ):'';

                      }else{
                            $data['dataspublikasi'] = $api->pengabdian($linktahun,$perpage, $offset);
                            $jumlah =$api->pengabdian($linktahun,'','');
                            $data['tahun'] = $linktahun;
                            $data['tahunpm'] =$linktahun;
                            $data['jenisp'] = 'Pengabdian Masyarakat';
                            $jum = $jumlah->status!=false?count($jumlah->data ):'';

                      }
                
                }else{
                    $data['dataspublikasi'] = $cekpm ;
                    $data['tahun'] = $tahun;
                    $data['tahunpm'] =$tahun;
                    $data['jenisp'] ='Pengabdian Masyarakat';
                    $jumlah =$api->pengabdian($tahun,'','');
                    $jum = $jumlah->status!=false?count($jumlah->data ):'';

                }
             }else { 
                    if ($tahun == false or $jenisp == false  or   $cekpublikasi->status == false and $linktahun != false)
                    {

                         if ($linktahun == false){
                            $data['dataspublikasi'] =  $api->publikasi(date('Y'),'',$group,$perpage, $offset);
                            $jumlah =$api->publikasi(date('Y'),'',$group,'','');
                            $data['tahun'] = date('Y');
                            $data['jenisp'] = $data['jenis_publikasi'];
                            $jum = $jumlah->status!=false?count($jumlah->data ):'';
                          
                 
                        }else{
                            if ($exjenispexlpode[1] == false){
                            $cekpublikasi = $api->publikasi($exjenispexlpode[0],'',$group,$perpage, $offset);   
                            $jumlah =$api->publikasi($exjenispexlpode[0],'',$group,'','');
                            }else{
                            $cekpublikasi = $api->publikasi($exjenispexlpode[0],$exjenispexlpode[1],'',$perpage, $offset);
                            $jumlah =$api->publikasi($exjenispexlpode[0],$exjenispexlpode[1],'','','');
                            }
                            $data['dataspublikasi'] = $cekpublikasi;
                            $data['tahun'] = $exjenispexlpode[0];
                            $data['jenisp'] =$data['jenis_publikasi'];
                            $jum = $jumlah->status!=false?count($jumlah->data ):'';
                        }
                    }else{

                        $data['dataspublikasi'] = $cekpublikasi;
                        $data['tahun'] = $tahun;
                        $data['jenisp'] = $data['jenis_publikasi'];
                            if ($exjenisp[0] =='%'){
                            $jumlah =$api->publikasi($tahun,'',$group,'','');    
                            }else{
                             $jumlah =$api->publikasi($tahun,$exjenisp[0],'','','');
                            }
        
                       
                        $jum = $jumlah->status!=false?count($jumlah->data ):'';

                    }

             }

             if (($jenis == 'penelitian') or ($jenis == 'PM') ){
                 if ($linktahun == false){
                    $config['base_url'] = site_url('eng/publikasi/'.$jenis.'/'.$data['tahun'].'/');
                 }else{
                    $config['base_url'] = site_url('eng/publikasi/'.$jenis.'/'.$linktahun.'/');
                 }
            }else{
                if ($exjenisp[0] =='%'){
                    $exjenisp[0] = '';
                }else{
                    $exjenisp[0] = $exjenisp[0];
                }
                 if ($linktahun == false){
                    $config['base_url'] = site_url('eng/publikasi/'.$jenis.'/'.$data['tahun'].'-'.$exjenisp[0].'/');
                 }else{
                    $config['base_url'] = site_url('eng/publikasi/'.$jenis.'/'.$linktahun.'-'.$exjenisp[0].'/');
                 }

            }

        $config['total_rows'] =  $jum;
        $config['per_page'] = $perpage;
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li class="page-item page-link">';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="page-item page-link">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li class="page-item page-link">';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li class="page-item page-link">';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li class="page-item page-link">';
        $config['num_tag_close']    = '</li>';
        $this->pagination->initialize($config);
        $data['offset'] =$offset;

	    $data['menu'] = menueng();
        $data['pages'] = 'page/publikasi/viewEng';
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['all'] = $this->model_berita->get(5);		
        $data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
        $data['langganan'] = $this->{$this->_model}->get_langganan(5);
        $stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
        $data['stat'] = $stat;
        $data['informasi'] = $this->{$this->_model}->get_informasi();
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['search_url'] = site_url('f_home/searchpost').'/';
		$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['menu'] = menueng();
        $data['datas'] = false;
        $data['searchpublikasi_url'] = site_url('Eng/publikasi/').$jenis.'/';
        $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
		$this->load->view('page/templateng', $data);
    }


    public function prodi ($prodiId)//single post page
	{
		$prodiId = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
		$this->session->_unit     = $prodiId;
		$data['prodi_active'] = $this->encryptions->encode($prodiId,$this->config->item('encryption_key')); ;
    	// print_r($data['prodi_active'] );
    	// exit;
		$pageId =$this->uri->segment(4);
		if ($pageId == false) {
			$data['contenprodi'] =$this->model_prodi->get_prodi_menu($prodiId);
			$data['pageId'] = false;

		}elseif ($pageId == 'sdm') {
				$data['pageId'] =$pageId;
				$data['contenprodi'] =false;

		}else{
			$data['contenprodi'] = $this->{$this->_model}->get_by_id('f_page_prodi',['pageId'=>$pageId]);
			$data['pageId'] = false;
		}
		$data['is_active'] = 'prodi';
		$data['pages'] = 'page/prodi/postEng';
		$data['menuprodi'] = $this->model_prodi->menu($prodiId);
		$data['datas'] = $this->model_prodi->get_id($prodiId);
		$data['informasi'] = $this->{$this->_model}->get_informasi();
		$data['prodinama'] = $data['datas']->prodiNama;
		$this->session->_unitnama     = $data['datas']->prodiNama;
		$data['all'] = $this->model_berita->get(5);
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['prodiId_select'] = $prodiId;
		if ($prodiId == in_array($prodiId, array("2","4","8"))) {
			$prodijurusanId = $this->{$this->_model}->get_by_id('f_prodi',['prodiId'=>$prodiId]);
			$data['staff'] = $this->Model_b_karyawan->prodi_karyawan(['prodijurusanId'=>$prodijurusanId->prodijurusanId]);	
			$data['prorjur_select'] =  $prodiId;
		}else{
			$data['staff'] = $this->Model_b_karyawan->prodi_karyawan(['karyawanProdiId'=>$prodiId]);
			$data['prorjur_select'] =  $prodiId;	
		}
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$data['menu'] = menueng();
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['foto'] = $this->model_prodi->get_foto_prodi($prodiId);
        // 	echo $this->db->last_query();
		$data['slider'] = $this->model_prodi->get_slider($prodiId);
        // print_r( $data['slider']);
        // exit;
		$data['pengumuman'] = $this->model_berita->beritaprodi($prodiId,5);
		$data['search_url'] = site_url('f_home/searchpost').'/';
		$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
		$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
		$this->load->view('page/templatengProdi', $data);
	}

    public function post_sdm($prodiId,$labId)//single post page
    {
    	$api = new Aksesapi();
    	$prodiId =$this->uri->segment(3);
    	$labId =$this->uri->segment(4);
    	$offset = $this->uri->segment(5);
    	$this->session->_unit     = $prodiId;
    	$this->session->_labId    = $labId;

    	if ($labId == 11){
    		$jenis = 'dosen';
    		$is_active = 'Lecturer PNS';
    	}elseif ($labId == 12){
    		$jenis = 'dosenonpns';
    		$is_active = 'Lecturer Non PNS';
    	}elseif ($labId == 22){
    		$jenis = 'tendikpns';
    		$is_active = 'Employee Tendik PNS';
    	}else{
    		$jenis = 'tendiknonpns';
    		$is_active = 'Employee Tendik NON PNS';
    	}

    	$perpage = 10;
    	if ($prodiId == in_array($prodiId, array("2","4","8"))) {
    		$prodijurusanId = $this->{$this->_model}->get_by_id('f_prodi',['prodiId'=>$prodiId]);

    		$getnip = $this->Model_b_karyawan->getDataPagination($perpage, $offset,['prodijurusanId'=>$prodijurusanId->prodijurusanId,'karyawanJenis'=>$jenis])->result();
    		$datanew = false;
    		if($getnip!==false) 
    		{
    			foreach ($getnip as $row) 
    			{
    				$api = new Aksesapi();
    				$pegawai = $api->Getkaryawan( $row->karyawanNIP);
                    $foto =  $this->{$this->_model}->get_by_id('f_karyawan',['karyawanNIP'=>$row->karyawanNIP]); 
                        if($foto !==false) {
                      $pegawai->datas->karyawanFoto = $foto->karyawanFoto;
                    }else{ $pegawai->datas->karyawanFoto = false; } 
                                                    
                         $datanew[] = $pegawai;
    			}
    		}
    		$data['staff'] = $datanew ;
    		$config['total_rows'] = $this->Model_b_karyawan->getAll(['prodijurusanId'=>$prodijurusanId->prodijurusanId,'karyawanJenis'=>$jenis])->num_rows();
    		$prod = $prodijurusanId->prodijurusanId;
    		$data['prodinama'] = $prodijurusanId->prodiNama;

    	}else{
    		$prodijurusanId = $this->{$this->_model}->get_by_id('f_prodi',['prodiId'=>$prodiId]);
    		$getnip = $this->Model_b_karyawan->prodi_karyawan_sdm($perpage, $offset,['karyawanProdiId'=>$prodiId],$jenis);
    		$datanew = false;
    		if($getnip!==false) 
    		{
    			foreach ($getnip as $row) 
    			{
    				$api = new Aksesapi();
    				$pegawai = $api->Getkaryawan( $row->karyawanNIP);
                    $foto =  $this->{$this->_model}->get_by_id('f_karyawan',['karyawanNIP'=>$row->karyawanNIP]); 
                        if($foto !==false) {
                      $pegawai->datas->karyawanFoto = $foto->karyawanFoto;
                    }else{ $pegawai->datas->karyawanFoto = false; } 
                                                    
                         $datanew[] = $pegawai;
    			}
    		}
    		$data['staff'] = $datanew ;
    		$prod = $prodiId;
    		$config['total_rows'] = $this->Model_b_karyawan->getAll(['karyawanProdiId'=>$prodiId,'karyawanJenis'=>$jenis])->num_rows();
    		$data['prodinama'] = $prodijurusanId->prodiNama;
    	}	
		// print_r($data['staff']);
		// echo $this->db->last_query();
		// exit();

    	$config['base_url'] = site_url('prodi/post_sdm/'.$prod.'/'.$labId.'/');   
    	$config['per_page'] = $perpage;
    	$config['full_tag_open']    = '<ul class="pagination">';
    	$config['full_tag_close']   = '</ul>';
    	$config['first_link']       = 'First';
    	$config['last_link']        = 'Last';
    	$config['first_tag_open']   = '<li class="page-item page-link">';
    	$config['first_tag_close']  = '</li>';
    	$config['prev_link']        = '&laquo';
    	$config['prev_tag_open']    = '<li class="page-item page-link">';
    	$config['prev_tag_close']   = '</li>';
    	$config['next_link']        = '&raquo';
    	$config['next_tag_open']    = '<li class="page-item page-link">';
    	$config['next_tag_close']   = '</li>';
    	$config['last_tag_open']    = '<li class="page-item page-link">';
    	$config['last_tag_close']   = '</li>';
    	$config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
    	$config['cur_tag_close']    = '</a></li>';
    	$config['num_tag_open']     = '<li class="page-item page-link">';
    	$config['num_tag_close']    = '</li>';
    	$this->pagination->initialize($config);
    	$data['offset'] =$offset;
    	$data['is_active'] = $is_active; 
    	$data['pages'] = 'page/prodi/sdmEng';
    	$data['datasprod'] = $this->model_prodi->get_id($prodiId);
    	$data['all'] = $this->model_berita->get(5);
    	$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
    	$data['informasi'] = $this->{$this->_model}->get_informasi();
    	$data['prodiId_select'] = $prodiId;
    	$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
    	$data['header'] = $header;
    	$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
    	$data['prodi'] = $prodi;
    	$data['menu'] = menueng();
    	$lab = $this->{$this->_model}->get_lab('f_lab');
    	$data['lab'] = $lab;
    	$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
    	$data['jurnam'] = $jurnam;
    	$data['foto'] = $this->model_prodi->get_foto($prodiId);
    	$data['slider'] = $this->model_prodi->get_slider($prodiId);
    	$data['pengumuman'] = $this->model_berita->beritaprodi($prodiId,5);
    	$data['search_url'] = site_url('f_home/searchpost').'/';
    	$data['searchkaryawan_url'] = site_url('Eng/searchsdm').'/';
    	$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
    	$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
    	$data['datas'] = false;
    	$data['detail_url'] = site_url('Eng/detail').'/';
    	$this->load->view('page/templatengProdi', $data);
    }

      public function searchsdm()
    {
    	$prodiId =$this->session->_unit ;
    	$labId = $this->session->_labId;

    	if ($labId == 11){
    		$jenis = 'dosen';
    		$is_active = 'Lecturer PNS';
    	}elseif ($labId == 12){
    		$jenis = 'dosenonpns';
    		$is_active = 'Lecturer Non PNS';
    	}elseif ($labId == 22){
    		$jenis = 'tendikpns';
    		$is_active = 'Employee Tendik PNS';
    	}else{
    		$jenis = 'tendiknonpns';
    		$is_active = 'Employee Tendik NON PNS';
    	}
    	$keyword = $this->input->get('searchkaryawan', true);
    	$data['prodinama'] =  $this->session->_unitnama  ;
    	$prodijurusanId = $this->{$this->_model}->get_by_id('f_prodi',['prodiId'=>$prodiId]);
		// echo "<pre>";
		// print_r ($keyword);
		// echo "</pre>";exit();
    	$this->session->keyword = $keyword;
    	if ($keyword==NULL) {
    		redirect('prodi/sdm');
    	}
    	$data['offset'] =0;
    	$data['keyword'] = $keyword;
    	$data['is_active'] = $is_active;	
    	$data['pages'] = 'page/prodi/sdmEng';
    	$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
    	$data['header'] = $header;
    	$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
    	$data['prodi'] = $prodi;
    	$lab = $this->{$this->_model}->get_lab('f_lab');
    	$data['lab'] = $lab;
    	$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
    	$data['jurnam'] = $jurnam;
    	$data['data'] = $this->model_berita->get(5);
    	$data['all'] = $this->model_berita->get(5);
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
    	$data['search_url'] = site_url($this->_controller_name.'/searchpost').'/';
    	$data['searchkaryawan_url'] = site_url('sdm/searchpost').'/';
    	$data['menu'] = menueng();
    	$data['data'] = $this->model_berita->get(5);
    	$getnip = $this->Model_b_karyawan->search_prodi($keyword,['karyawanProdiId'=>$prodiId,'karyawanJenis'=>$jenis]);
    	$data['informasi'] = $this->{$this->_model}->get_informasi();
    	$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
    	$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
    	$data['detail_url'] = site_url('Eng/detail').'/';
    	$data['datas'] = false;
    	$api = new Aksesapi();
    	$datanew = false;
    	if($getnip!==false) 
    	{
    		foreach ($getnip as $row) 
    		{
    			$api = new Aksesapi();
    			$pegawai = $api->Getkaryawan( $row->karyawanNIP);

    			if($pegawai!==false)
    			{

    				$datanew[] = $pegawai;
    			}
    		}
    	}
    	$data['staff'] = $datanew;
    	$this->load->view('page/templatengProdi',$data);
    }

     public function detail($labId)//single post page
    {

        $api = new Aksesapi();
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['is_active'] = 'Jurusan';
        $data['pages'] = 'page/prodi/detailEng';
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
        $datanip = $this->Model_b_karyawan->sdm_dosen_detail($keyS);
        $data['datassdm'] = $api->Getkaryawan($keyS);
        $data['datas'] = false;
        $data['karyawan'] = $datanip;
        $data['penelitian'] = $api->penelitian_by_id($keyS);
        $data['pengabdian'] = $api->pengabdian_by_id($keyS);
        $data['informasi'] = $this->{$this->_model}->get_informasi();
        $data['header'] = $header;
        $prodi = $this->{$this->_model}->get_prodi('f_jurusan');
        $data['prodi'] = $prodi;
        $lab = $this->{$this->_model}->get_lab('f_lab');
        $data['lab'] = $lab;
        $jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
        $data['jurnam'] = $jurnam;
        $data['all'] = $this->model_berita->get(5);
        $data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
        $link = $this->{$this->_model}->get_link();
        $data['link'] = $link;
        $data['search_url'] = site_url('f_home/searchpost').'/';
        $data['searchkaryawan_url'] = site_url('sdm/searchpost').'/';
        $data['menu'] = menueng(); 
         $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
            $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $this->load->view('page/templatengProdi', $data);
    }


    public function kerjasama($labId)//single post page
    {
    	$this->load->helper('datetoindo');
    	$data['jenisbhs'] = $this->uri->segment(1);
    	$data['datas'] = false;
		$data['pages'] = 'page/kerjasama/response';
	    $perpage = 10;
	    $offset = $this->uri->segment(5);
	    $labId = $this->uri->segment(4) ;
	    if ($labId == 1){
	    	$jenisk = 'Dalam Negeri';
	    	$jeniskeng = 'Domestic';

	    }else{
	    	$jenisk = 'Luar Negeri';
	    	$jeniskeng = 'Internasional';
	    }
	    $data['jeniskerjasama'] = $this->model_kerjasama->jenis($perpage,$offset,$jenisk)->result();
	    //  echo $this->db->last_query();
	    // // print_r(   $data['jeniskerjasama'] );
	    // exit();
	    $config['base_url'] = site_url('Eng/kerjasama/post/'.$labId.'/');
	    $config['total_rows'] = $this->model_kerjasama->getAll($jenisk)->num_rows();
	   
		$config['per_page'] = $perpage;
		$config['full_tag_open']    = '<ul class="pagination">';
		$config['full_tag_close']   = '</ul>';
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['first_tag_open']   = '<li class="page-item page-link">';
		$config['first_tag_close']  = '</li>';
		$config['prev_link']        = '&laquo';
		$config['prev_tag_open']    = '<li class="page-item page-link">';
		$config['prev_tag_close']   = '</li>';
		$config['next_link']        = '&raquo';
		$config['next_tag_open']    = '<li class="page-item page-link">';
		$config['next_tag_close']   = '</li>';
		$config['last_tag_open']    = '<li class="page-item page-link">';
		$config['last_tag_close']   = '</li>';
		$config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
		$config['cur_tag_close']    = '</a></li>';
		$config['num_tag_open']     = '<li class="page-item page-link">';
		$config['num_tag_close']    = '</li>';
     	$this->pagination->initialize($config);
        $data['is_active'] = $jeniskeng;
        $data['is_active_judul'] = 'Cooperation';		
        $data['offset'] =$offset;
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$data['informasi'] = $this->{$this->_model}->get_informasi();
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['all'] = $this->model_berita->get(5);
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$data['search_url'] = site_url('f_home/searchpost').'/';
		$data['searchkaryawan_url'] = site_url('Eng/searchpostkerjasama').'/';
		$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['menu'] = menueng(); 
        $this->load->view('page/templateng', $data);
    }

	public function postinfoeng($prodiId)//single post page
    {
    	$prodiId = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
    	$data['datas'] = false;
    	$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
    	$infoNama = $this->{$this->_model}->get_informasi_id($prodiId);
        $data['is_active'] = $infoNama->infodaftarNama;
        $data['informasi'] = $this->{$this->_model}->get_informasi();
        $data['pages'] = 'page/informasi/posteng';
        $data['datasinfo'] = $infoNama;
		$data['all'] = $this->model_berita->get(5);
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['prodiId_select'] = $prodiId;
		if ($prodiId == in_array($prodiId, array("2","4","8"))) {
		 $prodijurusanId = $this->{$this->_model}->get_by_id('f_prodi',['prodiId'=>$prodiId]);
		 $data['staff'] = $this->Model_b_karyawan->prodi_karyawan(['prodijurusanId'=>$prodijurusanId->prodijurusanId]);	
		 $data['prorjur_select'] =  $prodijurusanId->prodijurusanId;
		}else{
		$data['staff'] = $this->Model_b_karyawan->prodi_karyawan(['karyawanProdiId'=>$prodiId]);
		 $data['prorjur_select'] =  $prodiId;	
		}
       	$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
        $data['menu'] = menueng();
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
        $lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
        $data['foto'] = $this->model_prodi->get_foto_prodi($prodiId);
        $data['slider'] = $this->model_prodi->get_slider($prodiId);
		$data['pengumuman'] = $this->model_berita->beritaprodi($prodiId,5);
		$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
		$data['search_url'] = site_url('f_home/searchpost').'/';
        $this->load->view('page/templateng', $data);
    }

    public function searchpostkerjasama()
    {

        $keyword = $this->input->get('search', true);
        // echo "<pre>";
        // print_r ($keyword);
        // echo "</pre>";exit();
        $this->session->keyword = $keyword;
        if ($keyword==NULL) {
            redirect('kerjasama');
        }
        $data['jenisbhs'] = $this->uri->segment(1);
        $data['is_active'] = 'Cooperation';
        $data['is_active_judul'] = 'Cooperation'; 
        $data['offset'] =0;
        $data['keyword'] = $keyword;
        $data['pages'] = 'page/kerjasama/response';
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
        $data['header'] = $header;
        $prodi = $this->{$this->_model}->get_prodi('f_jurusan');
        $data['prodi'] = $prodi;
        $data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
        $data['langganan'] = $this->{$this->_model}->get_langganan(5);
        $stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
        $data['stat'] = $stat;
        $lab = $this->{$this->_model}->get_lab('f_lab');
        $data['lab'] = $lab;
        $jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
        $data['jurnam'] = $jurnam;
        $data['data'] = $this->model_berita->get(5);
        $data['all'] = $this->model_berita->get(5);
        $data['search_url'] = site_url($this->_controller_name.'/searchpost').'/';
        $data['searchkaryawan_url'] =  site_url('Eng/searchpostkerjasama').'/';
        $data['menu'] = menueng();
        $data['data'] = $this->model_berita->get(5);
        $data['jeniskerjasama'] = $this->model_kerjasama->cari_kerjasama($keyword);
        $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $data['datas'] = false;
        $this->load->view('page/templateng',$data);
    }

    public function News($jenisnews)//single post page
    {

    	$jenisnews = $this->uri->segment(3);

		if ($jenisnews == 'Bulletin') {
			$data['jenis_berita'] = 'Pengumuman';
		}elseif ($jenisnews == 'Agenda') {
			$data['jenis_berita'] = 'Agenda';
		}elseif ($jenisnews == 'News') {
			$data['jenis_berita'] = 'Berita';
		}elseif ($jenisnews == 'Event') {
			$data['jenis_berita'] = 'kegiatan';
		}elseif ($jenisnews == 'Student') {
			$data['jenis_berita'] = 'Mahasiswa';
		}
    	$this->load->helper('datetoindo');
    	$data['datas'] = false;
    	$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
		$data['is_active'] = $jenisnews;
        $data['menu'] = menueng();
        $data['pages'] = 'page/berita/viewEng';
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
        $data['all'] = $this->model_berita->get(5);
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['informasi'] = $this->{$this->_model}->get_informasi();
		$perpage = 5;
	    $offset = $this->uri->segment(3);
	    $data['pengumuman'] =$this->model_berita->getDataPagination($perpage, $offset,$data['jenis_berita'])->result();
	    $config['base_url'] = site_url('Eng/News/'.$jenisnews.'/');
	    $config['total_rows'] = $this->model_berita->getAll($data['jenis_berita'])->num_rows();
		$config['per_page'] = $perpage;
		$config['full_tag_open']    = '<ul class="pagination">';
		$config['full_tag_close']   = '</ul>';
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['first_tag_open']   = '<li class="page-item page-link">';
		$config['first_tag_close']  = '</li>';
		$config['prev_link']        = '&laquo';
		$config['prev_tag_open']    = '<li class="page-item page-link">';
		$config['prev_tag_close']   = '</li>';
		$config['next_link']        = '&raquo';
		$config['next_tag_open']    = '<li class="page-item page-link">';
		$config['next_tag_close']   = '</li>';
		$config['last_tag_open']    = '<li class="page-item page-link">';
		$config['last_tag_close']   = '</li>';
		$config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
		$config['cur_tag_close']    = '</a></li>';
		$config['num_tag_open']     = '<li class="page-item page-link">';
		$config['num_tag_close']    = '</li>';
     	$this->pagination->initialize($config);
		$data['search_url'] = site_url('f_home/searchpost').'/';
        $data['menu'] = menueng();
		$data['slider'] = $this->{$this->_model}->get_ref_table('f_slider','',['sliderIsDisplay'=>1]);
		$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
		$this->load->view('page/templateng', $data);
    }

    public function postEng($beritaNama)//single post page
    {
    	$this->load->helper('datetoindo');
    	$data['datas'] = false;
    	$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
		$data['is_active'] = 'News';
        $data['pages'] = 'page/berita/postEng';
        $data['data'] = $this->model_berita->get(5);
		$data['datasberita'] = $this->model_berita->get_nama($beritaNama);
		$data['all'] = $this->model_berita->get(5);
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['informasi'] = $this->{$this->_model}->get_informasi();
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['keyword'] = FALSE;
		$data['search_url'] = site_url($this->_controller_name.'/searchpost').'/';
		$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['menu'] = menueng();
        $this->load->view('page/templateng', $data);
	}


	public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

	public function load_content()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/content/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}



	public function loadthumb()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/thumb/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

	public function loadslider()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/files/'. $file;
		$files = pathinfo($path, PATHINFO_EXTENSION);
		if ($files == 'mp4') {
            header('Content-type: video/mp4');
            header('Content-Disposition: inline; filename="' . $path . '"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($path);

        }   elseif ($files == 'x-flv') {
            header('Content-type: video/x-flv');
            header('Content-Disposition: inline; filename="' . $path . '"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($path);

        }   else {

		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
		}
	}

	public function searchpost()
	{

		$keyword = $this->input->get('search', true);
		// echo "<pre>";
		// print_r ($keyword);
		// echo "</pre>";exit();
		$this->session->keyword = $keyword;
		if ($keyword==NULL) {
			redirect('f_home');
		}
		$data['keyword'] = $keyword;
		$data['is_active'] = 'ragam';	
		$data['pages'] = 'page/search/view';
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['data'] = $this->model_berita->get(5);
		$data['all'] = $this->model_berita->get(5);
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$data['search_url'] = site_url($this->_controller_name.'/searchpost').'/';
		// $data['datas'] = $this->model_berita->get_berita();
        $data['menu'] = menu();
		$data['data'] = $this->model_berita->get(5);
		$data['datas']= $this->model_berita->search($keyword);
		$data['informasi'] = $this->{$this->_model}->get_informasi();
		$this->load->view('page/template',$data);
	}


    public function sdm()
    {
        // $data = $this->get_master('page/sdm/view');
        $data['is_active'] = 'Human Resources'; 
        $data['pages'] = 'page/sdm/vieweng';
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
        $data['header'] = $header;
        $prodi = $this->{$this->_model}->get_prodi('f_jurusan');
        $data['prodi'] = $prodi;
        $lab = $this->{$this->_model}->get_lab('f_lab');
        $data['lab'] = $lab;
        $jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
        $data['jurnam'] = $jurnam;
        $data['data'] = $this->model_berita->get(5);
        $data['all'] = $this->model_berita->get(5);
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
        $data['pin'] = $this->model_berita->get_berita_pin(5);
        $data['berita'] = $this->model_berita->get_berita('BERITA, KEGIATAN',5);
        $data['searchkaryawan_url'] = site_url('sdm/searchpost').'/';
        $data['search_url'] = site_url('f_home/searchpost').'/';
         $data['informasi'] = $this->{$this->_model}->get_informasi();
        // $data['datas'] = $this->model_berita->get_berita();
         $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
         $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $data['menu'] = menueng();
        $data['datas'] = false;
        $this->load->view('page/templateng', $data);
    }
    public function sdm_post_eng($labId)//single post page
    {
        $data['datas'] = false;
        $api = new Aksesapi();
        $data['is_active'] = 'List Human Resources';
        $data['pages'] = 'page/sdm/responseng';

        if ($labId == 1){
            $jenis = 'dosen';
            $is_active = 'Lecture PNS';
        }elseif ($labId == 12){
            $jenis = 'dosenonpns';
            $is_active = 'Lecture Non PNS';
        }elseif ($labId == 2){
            $jenis = 'tendikpns';
            $is_active = 'Pegawai Tendik PNS';
        }else{
            $jenis = 'tendiknonpns';
            $is_active = 'Pegawai Tendik NON PNS';
        }
        $data['detail_url'] = site_url('Eng/detaileng').'/';
         $perpage = 10;
         $offset = $this->uri->segment(4);
         $getnip = $this->Model_b_karyawan->getDataPagination($perpage, $offset,['karyawanJenis'=>$jenis])->result();
                 $datanew = false;
                
                 $jumlh = 0;
                 if($getnip!==false) 
                 {
                    foreach ($getnip as $row) 
                    {
                        // $datanew = array();
                        $api = new Aksesapi();
                        $pegawai = $api->Getkaryawan( $row->karyawanNIP);
                        $foto =  $this->{$this->_model}->get_by_id('f_karyawan',['karyawanNIP'=>$row->karyawanNIP]); 
                        $pegawai->datas->karyawanFoto=$foto->karyawanFoto;
                                                    
                         $datanew[] = $pegawai;
                    
                        $jumlh = count((array)$pegawai);
                         
                    }
                 }

        $data['datassdm'] = $datanew;
        $data['jenis'] = $jenis;
        $config['base_url'] = site_url('sdm/post/'.$labId.'/');
        $config['total_rows'] = $this->Model_b_karyawan->getAll(['karyawanJenis'=>$jenis])->num_rows();
        // $config['total_rows'] = $jumlh ; 
       
        $config['per_page'] = $perpage;
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li class="page-item page-link">';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="page-item page-link">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li class="page-item page-link">';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li class="page-item page-link">';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li class="page-item page-link">';
        $config['num_tag_close']    = '</li>';
        $this->pagination->initialize($config);
        $data['informasi'] = $this->{$this->_model}->get_informasi();
        $data['is_active'] = $is_active;    
        $data['offset'] =$offset;
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
        $data['header'] = $header;
        $prodi = $this->{$this->_model}->get_prodi('f_jurusan');
        $data['prodi'] = $prodi;
        $lab = $this->{$this->_model}->get_lab('f_lab');
        $data['lab'] = $lab;
        $jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
        $data['jurnam'] = $jurnam;
        $data['all'] = $this->model_berita->get(5);
        $data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
        $link = $this->{$this->_model}->get_link();
        $data['link'] = $link;
        $data['pin'] = $this->model_berita->get_berita_pin(5);
        $data['search_url'] = site_url('f_home/searchpost').'/';
        $data['searchkaryawan_url'] = site_url('sdm/searchpost').'/';
        $data['menu'] = menueng(); 
        $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
      
        $this->load->view('page/templateng', $data);
    }

       public function detaileng($labId)//single post page
    {

        $api = new Aksesapi();
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['is_active'] = 'Detail Human Resources';
        $data['pages'] = 'page/sdm/detaileng';
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
        $datanip = $this->Model_b_karyawan->sdm_dosen_detail($keyS);
         // echo $this->db->last_query();
        $data['datassdm'] = $api->Getkaryawan($keyS);
        $data['penelitian'] = $api->penelitian_by_id($keyS);
        $data['pengabdian'] = $api->pengabdian_by_id($keyS);
        $data['karyawan'] = $datanip;
        $data['datas'] = false;
     //             print_r(  $data['penelitian']);
                 // exit();
        $data['informasi'] = $this->{$this->_model}->get_informasi();
        $data['header'] = $header;
        $prodi = $this->{$this->_model}->get_prodi('f_jurusan');
        $data['prodi'] = $prodi;
        $lab = $this->{$this->_model}->get_lab('f_lab');
        $data['lab'] = $lab;
        $jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
        $data['jurnam'] = $jurnam;
        $data['all'] = $this->model_berita->get(5);
        $data['pin'] = $this->model_berita->get_berita_pin(5);
        $data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
        $link = $this->{$this->_model}->get_link();
        $data['link'] = $link;
        $data['search_url'] = site_url('f_home/searchpost').'/';
        $data['searchkaryawan_url'] = site_url('sdm/searchpost').'/';
        $data['menu'] = menueng(); 
         $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
            $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $this->load->view('page/templateng', $data);
    }


      public function ragam_informasi($prodiId)//single post page
    {
        $prodiId = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $this->session->_unit     = $prodiId;
        $data['prodi_active'] = $this->encryptions->encode($prodiId,$this->config->item('encryption_key')); ;
        // print_r($data['prodi_active'] );
        // exit;
        $pageId =$this->uri->segment(4);

        if ($pageId == false) {
            $data['contenprodi'] =$this->model_prodi->get_prodi_menu($prodiId);
            $data['pageId'] = false;

        }elseif ($pageId == 'sdm') {
                $data['pageId'] =$pageId;
                $data['contenprodi'] =false;

        }elseif ($pageId == 'berita') {
            $data['pageId'] =$pageId;
            $data['contenprodi'] =false;

        }else{
            $data['contenprodi'] = $this->{$this->_model}->get_by_id('f_page_prodi',['pageId'=>$pageId]);
            $data['pageId'] = false;
        }
        $data['is_active'] = '';
        $data['pages'] = 'page/prodi/postEng';
        $perpage =10;
        $offset = $this->uri->segment(5);
      
        $data['menuprodi'] = $this->model_prodi->menu($prodiId);
        if ($offset == false){
             $data['datas'] = $this->model_prodi->get_id($prodiId);
            $data['informasi'] = $this->{$this->_model}->get_informasi();
            $data['prodinama'] = $data['datas']->prodiNama;
            $this->session->_unitnama     = $data['datas']->prodiNama; 
                   if ($prodiId == in_array($prodiId, array("2","4","8"))) {
                        $prodijurusanId = $this->{$this->_model}->get_by_id('f_prodi',['prodiId'=>$prodiId]);
                        $data['staff'] = $this->Model_b_karyawan->prodi_karyawan(['prodijurusanId'=>$prodijurusanId->prodijurusanId]);  
                        $data['prorjur_select'] =  $prodiId;
                    }else{
                        $data['staff'] = $this->Model_b_karyawan->prodi_karyawan(['karyawanProdiId'=>$prodiId]);
                        $data['prorjur_select'] =  $prodiId;    
                    }
             $data['pengumuman'] = $this->model_berita->beritaprodi($prodiId,$perpage,$offset);
             $jumlah = $this->model_berita->beritaprodi($prodiId,'','');
             $jum = $jumlah!=false?count($jumlah ):'';
              $config['base_url'] = site_url('Eng/ragam_informasi/'.$prodiId.'/'.$pageId .'/');  
       
        }else{
              $data['datas'] = $this->model_prodi->get_id($this->uri->segment(3));
        $data['informasi'] = $this->{$this->_model}->get_informasi();
        $data['prodinama'] = $data['datas']->prodiNama;
        $this->session->_unitnama     = $data['datas']->prodiNama;
            $data['pengumuman'] = $this->model_berita->beritaprodi($this->uri->segment(3),$perpage,$offset);
            $jumlah = $this->model_berita->beritaprodi($this->uri->segment(3),'','');
            $jum = $jumlah!=false?count($jumlah ):'';
             $config['base_url'] = site_url('Eng/ragam_informasi/'.$this->uri->segment(3).'/'.$pageId .'/');  
       
        }
   
        $data['all'] = $this->model_berita->get(5);
        $data['pin'] = $this->model_berita->get_berita_pin(5);
        $data['prodiId_select'] = $prodiId;
        $config['total_rows'] =  $jum; 
        $config['per_page'] = $perpage;
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li class="page-item page-link">';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="page-item page-link">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li class="page-item page-link">';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li class="page-item page-link">';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li class="page-item page-link">';
        $config['num_tag_close']    = '</li>';
        $this->pagination->initialize($config);
        $data['offset'] =$offset;

        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
        $data['header'] = $header;
        $prodi = $this->{$this->_model}->get_prodi('f_jurusan');
        $data['prodi'] = $prodi;
        $data['menu'] = menueng();
        $data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
        $data['langganan'] = $this->{$this->_model}->get_langganan(5);
        $stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
        $data['stat'] = $stat;
        $link = $this->{$this->_model}->get_link();
        $data['link'] = $link;
        $lab = $this->{$this->_model}->get_lab('f_lab');
        $data['lab'] = $lab;
        $jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
        $data['jurnam'] = $jurnam;
        $data['foto'] = $this->model_prodi->get_foto_prodi($prodiId);
        //  echo $this->db->last_query();
        $data['slider'] = $this->model_prodi->get_slider($prodiId);
        // print_r( $data['slider']);
        // exit;
        $data['search_url'] = site_url('f_home/searchpost').'/';
        $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $this->load->view('page/templateng', $data);
    }

}
