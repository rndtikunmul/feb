<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Prestasimhs extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_controller_name = 'prestasimhs';
		$this->_model = 'model_f_master';
		$this->load->model('model_prestasimhs','',TRUE);
		$this->load->model('model_b_prestasimhs','',TRUE);
		$this->load->model('model_berita','',TRUE);
		$this->load->model('model_b_berita','',TRUE);
		$this->load->model($this->_model, '', TRUE);
	}

	public function index()//single post page
    {
    	$this->load->helper('datetoindo');
		$data['is_active'] = 'Prestasi';
		$data['datas'] = false;
        $data['menu'] = menu();
        $data['pages'] = 'page/prestasimhs/view';
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(4);
		$data['langganan'] = $this->{$this->_model}->get_langganan(1);
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['all'] = $this->model_berita->get(5);	
		$data['agenda'] = $this->model_berita->get_berita('Agenda',5);	
        // $data['nasional'] = $this->model_prestasimhs->nasional();
        $data['informasi'] = $this->{$this->_model}->get_informasi();
        $data['provinsi'] = $this->model_prestasimhs->provinsi();
        $data['kotakab'] = $this->model_prestasimhs->kotakab();
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['search_url'] = site_url('f_home/searchpost').'/';
		$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['menu'] = menu();
		$this->load->view('page/template', $data);
    }

	public function post($labId)//single post page
    {
    	$this->load->helper('datetoindo');
    	$data['datas'] = false;
    	$data['pages'] = 'page/prestasimhs/response';
	    $perpage = 10;
	    $offset = $this->uri->segment(4);
	    $data['internasional'] = $this->model_prestasimhs->internasional($perpage,$offset,$labId)->result();
	    $config['base_url'] = site_url('prestasimhs/post/'.$labId.'/');
	    $config['total_rows'] = $this->model_prestasimhs->getAll($labId)->num_rows();
		$config['per_page'] = $perpage;
		$config['full_tag_open']    = '<ul class="pagination">';
		$config['full_tag_close']   = '</ul>';
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['first_tag_open']   = '<li class="page-item page-link">';
		$config['first_tag_close']  = '</li>';
		$config['prev_link']        = '&laquo';
		$config['prev_tag_open']    = '<li class="page-item page-link">';
		$config['prev_tag_close']   = '</li>';
		$config['next_link']        = '&raquo';
		$config['next_tag_open']    = '<li class="page-item page-link">';
		$config['next_tag_close']   = '</li>';
		$config['last_tag_open']    = '<li class="page-item page-link">';
		$config['last_tag_close']   = '</li>';
		$config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
		$config['cur_tag_close']    = '</a></li>';
		$config['num_tag_open']     = '<li class="page-item page-link">';
		$config['num_tag_close']    = '</li>';
     	$this->pagination->initialize($config);
        $data['is_active'] = $labId;	
        $data['offset'] =$offset;
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
		$data['informasi'] = $this->{$this->_model}->get_informasi();
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['all'] = $this->model_berita->get(5);
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
		$data['search_url'] = site_url('f_home/searchpost').'/';
		$data['searchkaryawan_url'] = site_url('prestasimhs/searchpost').'/';
		$data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['menu'] = menu(); 
        $this->load->view('page/template', $data);
    }


	public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}
	
	public function loadthumb()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/thumb/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

	 public function loadattach()
    {
        ob_clean();
        $this->load->helper('file');
        $file = $this->uri->segment(3);
        $path = FCPATH . '../upload_file/berita/' . $file;
        $files = get_mime_by_extension($path);
        // echo $files;
        // exit();
        if ($files == 'application/pdf') {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $path . '"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($path);

        }   elseif 
            ($files == 'application/msword') {
            header('Content-type: application/msword');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    

        }   elseif 
            ($files == 'application/vnd.ms-excel') {
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path); 
            
        }   elseif 
            ($files == 'application/vnd.ms-powerpoint') {
            header('Content-type: application/vnd.ms-powerpoint');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    

        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
            header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);
            
        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    
    
        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
            header('Content-type: application/vnd.openxmlformats-officedocument.presentationml.presentation');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);
            
        }   else {
            $size = getimagesize($path);
            header('Content-Type:' . $size['mime']);
            switch ($size['mime']) {
                case 'image/png':
                    $img = imagecreatefrompng($path);

                    imagepng($img);
                    break;

                default:
                    $img = imagecreatefromjpeg($path);
                    imagejpeg($img);
                    break;
            }
            imagedestroy($img);
        }
    }

	public function searchpost()
	{

		$keyword = $this->input->get('search', true);
		// echo "<pre>";
		// print_r ($keyword);
		// echo "</pre>";exit();
		$this->session->keyword = $keyword;
		if ($keyword==NULL) {
			redirect('prestasimhs');
		}
		$data['offset'] =0;
		$data['keyword'] = $keyword;
		$data['is_active'] = 'Prestasi';	
		$data['datas'] = false;	
		$data['pages'] = 'page/prestasimhs/response';
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['data'] = $this->model_berita->get(5);
		$data['all'] = $this->model_berita->get(5);
		$data['search_url'] = site_url($this->_controller_name.'/searchpost').'/';
		$data['searchkaryawan_url'] = site_url('prestasimhs/searchpost').'/';
        $data['menu'] = menu();
		$data['data'] = $this->model_berita->get(5);
        $data['internasional'] = $this->model_prestasimhs->cari_prestasi($keyword);
        $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['informasi'] = $this->{$this->_model}->get_informasi();
		$this->load->view('page/template',$data);
	}
}