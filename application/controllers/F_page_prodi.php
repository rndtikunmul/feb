
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class f_page_prodi extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/f_page_prodi/';
        $this->_path_js = 'prodi/';
        $this->_judul = 'Menu Prodi';
        $this->_controller_name = 'f_page_prodi';
        $this->_model_name = 'model_f_page_prodi';
        $this->_page_index = 'index';
        $this->_logged_in = $this->session->userdata('logged_in');
        $this->load->model($this->_model_name,'',TRUE);
    }


    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [$this->_path_js . 'f_prodi'];
        $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
        if(($this->_logged_in['susrSgroupNama'] == "ADMIN_FEB") or ($this->_logged_in['susrSgroupNama'] == "ADMIN") ){
         $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');
     }else{
        $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi','',['prodijurusanId'=>$jurusan->sgroupProdiId]); 
    }
    $data['user'] = $this->_logged_in['susrSgroupNama'] ;
    $data['show_url'] = site_url($this->_controller_name . '/response') . '/';
    $this->load->view($this->_template, $data);
}

    public function response()
    {
        $this->form_validation->set_rules('beritaUnit', 'beritaUnit', 'trim|required|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data['scripts'] = [$this->_path_js . 'f_prodi'];
                $data = $this->get_master($this->_path_page.$this->_page_index);
                $beritaUnit = $this->input->post('beritaUnit');
                $this->session->_unit     = $beritaUnit;
                // $data['datas'] = $this->{$this->_model_name}->get_slider($beritaUnit);
                $data['datas'] = $this->{$this->_model_name}->all($beritaUnit);
            
               
                $data['create_url'] = site_url($this->_controller_name.'/create').'/';
                $data['update_url'] = site_url($this->_controller_name.'/update').'/';
                $data['delete_url'] = site_url($this->_controller_name.'/delete').'/';
                $data['Ya'] = site_url($this->_controller_name .'/publisYa') . '/';
                $data['Tidak'] = site_url($this->_controller_name .'/publisTak') . '/';
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }
   
    public function create()
    {	
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts'] =[$this->_path_js . 'f_prodi'];
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
        if(($this->_logged_in['susrSgroupNama'] == "ADMIN_FEB") or ($this->_logged_in['susrSgroupNama'] == "ADMIN") ){
         $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');
     }else{
        $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi','',['prodijurusanId'=>$jurusan->sgroupProdiId]); 
    }
    $data['s_user_modul_group_ref'] = $this->{$this->_model_name}->get_ref_table('s_user_modul_group_ref');

    $this->load->view($this->_template, $data);
}

public function update()
{		
    $data = $this->get_master($this->_path_page.'form');	
    $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
    $data['scripts'] =['b_slider/b_slider'];
    $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
    if(($this->_logged_in['susrSgroupNama'] == "ADMIN_FEB") or ($this->_logged_in['susrSgroupNama'] == "ADMIN") ){
     $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');
 }else{
    $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi','',['prodijurusanId'=>$jurusan->sgroupProdiId]); 
}
$data['save_url'] = site_url($this->_controller_name.'/save').'/';	
$data['status_page'] = 'Update';
$key = ['pageId'=>$keyS];
$data['datas'] = $this->{$this->_model_name}->by_id($key);
$data['s_user_modul_group_ref'] = $this->{$this->_model_name}->get_ref_table('s_user_modul_group_ref');

$this->load->view($this->_template, $data);
}

public function save()
{		
    $pageIdOld = $this->input->post('pageIdOld');
    $this->form_validation->set_rules('pageHead','pageHead','trim|xss_clean');
    $this->form_validation->set_rules('pageJudul','pageJudul','trim|xss_clean|required');
    $this->form_validation->set_rules('pageContent','pageContent','trim|xss_clean|required');

    if($this->form_validation->run()) 
    {	
        if(IS_AJAX)
        {
            $pageHead = $this->input->post('pageHead');
            $pageJudul = $this->input->post('pageJudul');
            $pageContent = $this->input->post('pageContent');
            $pageDatetime = $this->input->post('pageDatetime');
            $pageProdi = $this->input->post('pageProdi');
            $pageInfo = $this->input->post('pageInfo');
            $pageJudulEng = $this->input->post('pageJudulEng');
            $pageContentEng = $this->input->post('pageContentEng');
            $pageInfoEng = $this->input->post('pageInfoEng');



            $param = array(
                'pageHead'=>$pageHead,
                'pageJudul'=>$pageJudul,
                'pageContent'=>$pageContent,
                'pageDatetime'=>date('Y-m-d'),
                'pageProdi'=>$pageProdi,
                'pageInfo'=>$pageInfo,
                'pageJudulEng'=>$pageJudulEng,
                'pageContentEng' => $pageContentEng,
                'pageInfoEng' =>$pageInfoEng,

            );

            if(empty($pageIdOld))
            {
                $proses = $this->{$this->_model_name}->insert('f_page_prodi',$param);
            } else {
                $key = array('pageId'=>$pageIdOld);
                $proses = $this->{$this->_model_name}->update('f_page_prodi',$param,$key);
            }

            if($proses)
                message($this->_judul.' Berhasil Disimpan','success');
            else
            {
                $error = $this->db->error();
                message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
            }
        }
    } else {
        message('Ooops!! Something Wrong!! '.validation_errors(),'error');
    }
}

public function delete()
{
    $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
    $key = ['pageId'=>$keyS];
    $proses = $this->{$this->_model_name}->delete('f_page_prodi',$key);
    if ($proses) 
        message($this->_judul.' Berhasil Dihapus','success');
    else
    {
        $error = $this->db->error();
        message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
    }
}
}
