
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class B_dosen extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_dosen/';
        $this->_path_js = null;
        $this->_judul = 'Detail Dosen';
        $this->_controller_name = 'b_dosen';
        $this->_model_name = 'model_b_dosen';
        $this->_page_index = 'index';
        $this->_logged_in = $this->session->userdata('logged_in');
        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page.$this->_page_index);
        $data['scripts'] = [];
        $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
         if(($this->_logged_in['susrSgroupNama'] == "ADMIN_FEB") or ($this->_logged_in['susrSgroupNama'] == "ADMIN") ){
           $data['datas'] = $this->{$this->_model_name}->all('');
        }else{
              $data['datas'] = $this->{$this->_model_name}->all(['prodijurusanId'=>$jurusan->sgroupProdiId]);
        }
        $data['create_url'] = site_url($this->_controller_name.'/create').'/';
        $data['update_url'] = site_url($this->_controller_name.'/update').'/';
        $data['delete_url'] = site_url($this->_controller_name.'/delete').'/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {	
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts'] = [];	
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
      
         if(($this->_logged_in['susrSgroupNama'] == "ADMIN_FEB") or ($this->_logged_in['susrSgroupNama'] == "ADMIN") ){
            $data['f_karyawan'] = $this->{$this->_model_name}->all_dosen_jurusan('');
        }else{
              $data['f_karyawan'] = $this->{$this->_model_name}->all_dosen_jurusan(['prodijurusanId'=>$jurusan->sgroupProdiId]);
        }
        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] = [];	
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['dosenid'=>$keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
         $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
      
         if(($this->_logged_in['susrSgroupNama'] == "ADMIN_FEB") or ($this->_logged_in['susrSgroupNama'] == "ADMIN") ){
            $data['f_karyawan'] = $this->{$this->_model_name}->all_dosen_jurusan('');
        }else{
              $data['f_karyawan'] = $this->{$this->_model_name}->all_dosen_jurusan(['prodijurusanId'=>$jurusan->sgroupProdiId]);
        }
        $this->load->view($this->_template, $data);
    }

    public function save()
    {		
        $dosenidOld = $this->input->post('dosenidOld');
        $this->form_validation->set_rules('dosenKaryawanId','dosenKaryawanId','trim|requred|xss_clean|is_unique[f_dosen.dosenKaryawanId]');
        $this->form_validation->set_rules('dosenNIP','dosenNIP','trim|xss_clean');
        $this->form_validation->set_rules('dosenContent','dosenContent','trim|xss_clean');

        if($this->form_validation->run()) 
        {	
            if(IS_AJAX)
            {
                $dosenKaryawanId = $this->input->post('dosenKaryawanId');
                $dosenNIP = $this->input->post('dosenNIP');
                $dosenContent = $this->input->post('dosenContent');


                $param = array(
                    'dosenKaryawanId'=>$dosenKaryawanId,
                    'dosenNIP'=>$dosenNIP,
                    'dosenContent'=>$dosenContent,

                );

                if(empty($dosenidOld))
                {
                    $proses = $this->{$this->_model_name}->insert('f_dosen',$param);
                } else {
                    $key = array('dosenid'=>$dosenidOld);
                    $proses = $this->{$this->_model_name}->update('f_dosen',$param,$key);
                }

                if($proses)
                    message($this->_judul.' Berhasil Disimpan','success');
                else
                {
                    $error = $this->db->error();
                    message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['dosenid'=>$keyS];
        $proses = $this->{$this->_model_name}->delete('f_dosen',$key);
        if ($proses) 
            message($this->_judul.' Berhasil Dihapus','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
        }
    }
}
