<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_berita extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_berita/';
        $this->_path_js = 'b_berita/';
        $this->_judul = 'Ragam Informasi';
        $this->_controller_name = 'b_berita';
        $this->_model_name = 'model_b_berita';
        $this->_page_index = 'index';
        $this->_path_upload = '../upload_file/berita/';
        $this->_logged_in = $this->session->userdata('logged_in');
       
        $this->load->model($this->_model_name,'',TRUE);
    }
    public function index()
    {
        $data = $this->get_master($this->_path_page.$this->_page_index);
        $data['scripts'] = [$this->_path_js . 'b_berita_form'];
        $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
         if(($this->_logged_in['susrSgroupNama'] == "ADMIN_FEB") or ($this->_logged_in['susrSgroupNama'] == "ADMIN") ){
             $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');
        }else{
            $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi','',['prodijurusanId'=>$jurusan->sgroupProdiId]); 
        }
        $data['user'] = $this->_logged_in['susrSgroupNama'] ;
        $data['show_url'] = site_url($this->_controller_name . '/response') . '/';
        $this->load->view($this->_template, $data);
    }


     public function response()
    {
        $this->form_validation->set_rules('beritaUnit', 'beritaUnit', 'trim|required|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {

                $data = $this->get_master($this->_path_page.$this->_page_index);
                $beritaUnit = $this->input->post('beritaUnit');
                $tanggalawal = $this->input->post('tanggalawal');
                $tanggalakhir = $this->input->post('tanggalakhir');
                $date_awal = date('Y-m-d',  strtotime($tanggalawal));
                $date_akhir = date('Y-m-d', strtotime($tanggalakhir));
                $this->session->_unit     = $beritaUnit;
                $data['datas'] = $this->{$this->_model_name}->by_user($beritaUnit,$date_awal,$date_akhir);
                // echo $this->db->last_query();
                // exit();
                $data['create_url'] = site_url($this->_controller_name.'/create').'/';
                $data['update_url'] = site_url($this->_controller_name.'/update').'/';
                $data['delete_url'] = site_url($this->_controller_name.'/delete').'/';
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }


    public function create()
    {	
        $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts'] = [$this->_path_js . 'b_berita_form'];	
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['user']= $this->_logged_in['susrSgroupNama'];
        $data['datas'] = false;
        $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi','',['prodiId'=>$this->session->_unit]); 
        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] = [$this->_path_js . 'b_berita'];	
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['beritaId'=>$keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('f_berita',$key);
        $data['user']= $this->_logged_in['susrSgroupNama'];  
        $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi','',['prodiId'=>$this->session->_unit]); 
        $this->load->view($this->_template, $data);
    }

    public function save()
    {	
        $session_data = $this->session->userdata('logged_in');	
        $beritaIdOld = $this->input->post('beritaIdOld');
        $this->form_validation->set_rules('beritaSection','beritaSection','trim|xss_clean');
        $this->form_validation->set_rules('beritaJudul','beritaJudul','trim|xss_clean|required');
        $this->form_validation->set_rules('beritaContent','beritaContent','required');
        $this->form_validation->set_rules('beritaDatetime','beritaDatetime','trim|xss_clean|required');
        $this->form_validation->set_rules('beritaTag','beritaTag','trim|xss_clean|required');

        if($this->form_validation->run()) 
        {	
            if(IS_AJAX)
            {
                $beritaSection  = $this->input->post('beritaSection');
                $beritaJudul    = $this->input->post('beritaJudul');
                $beritaContent  = $this->input->post('beritaContent');
                $beritaDatetime = $this->input->post('beritaDatetime');
                $beritaTag      = $this->input->post('beritaTag');
                $beritaAuthor   = $this->_logged_in['susrProfil'];
                $beritaBanner   = $this->input->post('berkas');
                $beritaNama     = str_replace(' ','.',(preg_replace('/[^a-z0-9]+/i', ' ', strip_tags($this->input->post('beritaJudul')))));
                $beritaIsDisplay =  $this->input->post('beritaIsDisplay');
                $beritaUnit      =  $this->input->post('beritaUnit');
                $beritaIsPin     =  $this->input->post('beritaIsPin');
                $beritaprodiaprov =  $this->input->post('beritaprodiaprov');
                $BeritaJudulEng     =  $this->input->post('BeritaJudulEng');
                $BeritaContentEng =  $this->input->post('BeritaContentEng');
                $user = $this->_logged_in['susrSgroupNama'] ; 

                if (($beritaSection == 'pengumuman' or $beritaSection == 'berita') and  ( $user == in_array($user, array("ADMIN_JURUSAN_AKUNTANSI","ADMIN_JURUSAN_ILMUEKONOMI","ADMIN_JURUSAN_MANAJEMEN")))) {
                  $beritaprodiaprov = 1;  
                }

                $param = array(
                    'beritaNama'=>$beritaNama,
                    'beritaSection'=>$beritaSection,
                    'beritaJudul'=>$beritaJudul,
                    'beritaContent'=>$beritaContent,
                    'beritaDatetime'=>date("Y-m-d",strtotime($beritaDatetime)),
                    'beritaTag'=>$beritaTag,
                    'beritaAuthor'=>$beritaAuthor,
                    'beritaIsDisplay'=> $beritaIsDisplay,
                    'beritaUnit' => $beritaUnit,
                    'beritaIsPin' => $beritaIsPin,
                    'beritaprodiaprov' => $beritaprodiaprov,
                    'BeritaJudulEng'=> $BeritaJudulEng,
                    'BeritaContentEng'=>$BeritaContentEng


                );
               


                 $paramupdate = array(
                    'beritaNama'=>$beritaNama,
                    'beritaSection'=>$beritaSection,
                    'beritaJudul'=>$beritaJudul,
                    'beritaContent'=>$beritaContent,
                    'beritaTag'=>$beritaTag,
                    'beritaIsDisplay'=> $beritaIsDisplay,
                    'beritaUnit' => $beritaUnit,
                    'beritaIsPin' => $beritaIsPin,
                    'beritaprodiaprov' => $beritaprodiaprov,
                    'BeritaJudulEng'=> $BeritaJudulEng,
                    'BeritaContentEng'=>$BeritaContentEng


                );
                 // print_r($paramupdate);
                 // exit();
              

                if (!empty($_FILES['berkas']['name'])) {
                    $config['upload_path'] = realpath(APPPATH . '../upload_file/berita/'); //path folder
                    $config['allowed_types'] = 'jpg|jpeg|png'; //type yang dapat diakses bisa anda sesuaikan
                    $config['max_size'] = 1024 * 15; //maksimum besar file 5M
                    $config['file_name'] = $beritaSection. '_' .mt_rand(); //nama yang terupload nantiny

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('berkas'))
                        echo message(strip_tags($this->upload->display_errors()), 'error');
                    else {
                        $file = $this->upload->data();
                        $image_path = $file['full_path'];
                        $param['beritaBanner'] = $file['file_name'];
                        $paramupdate['beritaBanner'] = $file['file_name'];
                        if (file_exists($image_path)) {
                            $upload = true;
                            $this->load->helper('image_thumb');
                            image_thumb($config['upload_path'], $param['beritaBanner'], 400, 300);
                            image_thumb($config['upload_path'], $paramupdate['beritaBanner'], 400, 300);
                        } else {
                            $upload = false;
                        }
                    }
                }
                
                if(empty($beritaIdOld))
                {
                    if($this->{$this->_model_name}->by_id($beritaNama)==false)
					$proses = $this->{$this->_model_name}->insert('f_berita',$param);
				    else
				    {
					$proses = false;
					message($this->_judul.' Gagal Simpan, Judul Berita sudah ada','error');
				    }
                } else {
                    $key = array('beritaId'=>$beritaIdOld);
                    $proses = $this->{$this->_model_name}->update('f_berita',$paramupdate,$key);
                }

                if($proses)
                    message($this->_judul.' Berhasil Disimpan','success');
                else
                {
                    $error = $this->db->error();
                    message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['beritaId'=>$keyS];
        $data['scripts'] = [$this->_path_js . 'b_berita']; 
        $proses = $this->{$this->_model_name}->delete('f_berita',$key);
        if ($proses) 
            message($this->_judul.' Berhasil Dihapus','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
        }
    }

    public function loadimage()
    {
        $file = $this->uri->segment(3);
        ob_clean();
        $path = FCPATH . '../upload_file/berita/'. $file;
        $size = getimagesize($path);
        header('Content-Type:' . $size['mime']);
        switch ($size['mime']) {
            case 'image/png':
            $img = imagecreatefrompng($path);

            imagepng($img);
            break;

            default:
            $img = imagecreatefromjpeg($path);
            imagejpeg($img);
            break;
        }
        imagedestroy($img);
    }
}
