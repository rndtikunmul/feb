<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Page extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('model_menu','',TRUE);
        $this->_model = 'model_f_master';
		$this->load->model('model_berita','',TRUE);
		$this->load->model('model_b_berita','',TRUE);
		$this->load->model($this->_model, '', TRUE);
	}

	public function index()//single post page
    {
    	redirect('/home', 'refresh');
        $data['currentPage'] = false;
        $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $data['search_url'] = site_url('f_home/searchpost').'/';
    }

	public function post()//single post page
    {
        $data['is_active'] = 'pages';
    	$pageNama = preg_replace('/[^A-Za-z0-9\.]/', '', strip_tags($this->uri->segment(3)));
        $data['menu'] = menu();
        $data['pages'] = 'page/menu/post';
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['all'] = $this->model_berita->get(5);
        $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['search_url'] = site_url('f_home/searchpost').'/';
        $data['informasi'] = $this->{$this->_model}->get_informasi();
        $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
        // $data['kegiatan'] = $this->model_berita->get_berita('KEGIATAN',5);
		// $data['pengumuman'] = $this->model_berita->get_berita('PENGUMUMAN',5);
        $data['datas'] = $this->model_menu->get_nama($pageNama);
        // print_r($datas);
		// exit();	
        $data['is_active'] = $data['datas']!=false?$data['datas']->pageHead:'';
		$this->load->view('page/template', $data);
    }

	public function loadattach()
    {
        ob_clean();
        $this->load->helper('file');
        $file = $this->uri->segment(3);
        $path = FCPATH . '../upload_file/files/' . $file;
        $files = get_mime_by_extension($path);
        // echo $files;
        // exit();
        if ($files == 'application/pdf') {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $path . '"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($path);

        }   elseif 
            ($files == 'application/msword') {
            header('Content-type: application/msword');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    

        }   elseif 
            ($files == 'application/vnd.ms-excel') {
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path); 
            
        }   elseif 
            ($files == 'application/vnd.ms-powerpoint') {
            header('Content-type: application/vnd.ms-powerpoint');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    

        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
            header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);
            
        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    
    
        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
            header('Content-type: application/vnd.openxmlformats-officedocument.presentationml.presentation');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);
            
        }   else {
            $size = getimagesize($path);
            header('Content-Type:' . $size['mime']);
            switch ($size['mime']) {
                case 'image/png':
                    $img = imagecreatefrompng($path);

                    imagepng($img);
                    break;

                default:
                    $img = imagecreatefromjpeg($path);
                    imagejpeg($img);
                    break;
            }
            imagedestroy($img);
        }
    }
	

}