<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_page extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_page/';
        $this->_path_js = null;
        $this->_judul = 'Page Editor';
        $this->_controller_name = 'b_page';
        $this->_model_name = 'model_b_page';
        $this->_page_index = 'index';
        $this->_logged_in = $this->session->userdata('logged_in');

        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $session_data = $this->session->userdata('logged_in');
        $data = $this->get_master($this->_path_page.$this->_page_index);
        $data['scripts'] = [];
        $user = $this->_logged_in['susrNama'];
        $data['datas'] = $this->{$this->_model_name}->all($user);
        $data['create_url'] = site_url($this->_controller_name.'/create').'/';
        $data['update_url'] = site_url($this->_controller_name.'/update').'/';
        $data['delete_url'] = site_url($this->_controller_name.'/delete').'/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {	
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts'] = ['b_berita/b_page'];	
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['s_user_modul_group_ref'] = $this->{$this->_model_name}->get_ref_table('s_user_modul_group_ref');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] = [];	
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['pageId'=>$keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['s_user_modul_group_ref'] = $this->{$this->_model_name}->get_ref_table('s_user_modul_group_ref');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {	
        $session_data = $this->session->userdata('logged_in');
        $pageIdOld = $this->input->post('pageIdOld');
        $this->form_validation->set_rules('pageHead','pageHead','trim|xss_clean');
        $this->form_validation->set_rules('pageJudul','pageJudul','trim|xss_clean|required');
        $this->form_validation->set_rules('pageContent','pageContent','required');
        $this->form_validation->set_rules('pageTag','pageTag','trim|xss_clean|required');

        if($this->form_validation->run()) 
        {	
            if(IS_AJAX)
            {
                $pageHead = $this->input->post('pageHead');
                $pageJudul = $this->input->post('pageJudul');
                $pageContent = $this->input->post('pageContent');
                $pageDatetime = date('Y-m-d H:i:s');
                $pageTag = $this->input->post('pageTag');
                $pageAuthor = $this->_logged_in['susrNama'];
                $pageUrut = $this->input->post('pageUrut');
                $pageNama = str_replace(' ','.',(preg_replace('/[^a-z0-9]+/i', ' ', strip_tags($this->input->post('pageJudul')))));
                $pageSidebar = $this->input->post('pageSidebar');
                $pageContenteng = $this->input->post ('pageContenteng');
                $pageSidebareng = $this->input->post ('pageSidebareng');
                $pageNamaEng =  $this->input->post ('pageNamaEng');
                $pageHeadEng =  $this->input->post ('pageHeadEng');
                $pageLink = $this->input->post ('pageLink');

                $param = array(
                    'pageNama'=>$pageNama,
                    'pageHead'=>$pageHead,
                    'pageJudul'=>$pageJudul,
                    'pageContent'=>$pageContent,
                    'pageDatetime'=>$pageDatetime,
                    'pageTag'=>$pageTag,
                    'pageAuthor'=>$pageAuthor,
                    'pageUrut'=>$pageUrut,
                    'pageSidebar'=> $pageSidebar,
                    'pageContenteng'=> $pageContenteng,
                    'pageSidebareng'=> $pageSidebareng,
                    'pageNamaEng'=> $pageNamaEng,
                    'pageHeadEng'=>$pageHeadEng,
                    'pageLink'=> $pageLink


                );

                if(empty($pageIdOld))
                {
                    $proses = $this->{$this->_model_name}->insert('f_page',$param);
                } else {
                    $key = array('pageId'=>$pageIdOld);
                    $proses = $this->{$this->_model_name}->update('f_page',$param,$key);
                }

                if($proses)
                    message($this->_judul.' Berhasil Disimpan','success');
                else
                {
                    $error = $this->db->error();
                    message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['pageId'=>$keyS];
        $proses = $this->{$this->_model_name}->delete('f_page',$key);
        if ($proses) 
            message($this->_judul.' Berhasil Dihapus','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
        }
    }
}
