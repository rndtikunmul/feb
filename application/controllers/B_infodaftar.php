<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_infodaftar extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_infodaftar/';
        $this->_path_js = null;
        $this->_judul = 'Informasi Pendaftaran';
        $this->_controller_name = 'b_infodaftar';
        $this->_model_name = 'model_b_infodaftar';
        $this->_page_index = 'index';
        $this->_logged_in = $this->session->userdata('logged_in');
        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['f_jenjang'] = $this->{$this->_model_name}->get_ref_table('f_jenjang');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['infodaftarId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['f_jenjang'] = $this->{$this->_model_name}->get_ref_table('f_jenjang');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $session_data = $this->session->userdata('logged_in');
        $infodaftarIdOld = $this->input->post('infodaftarIdOld');
        $this->form_validation->set_rules('infodaftarJenjangId', 'infodaftarJenjangId', 'trim|xss_clean');
        $this->form_validation->set_rules('infodaftarContent', 'infodaftarContent', ''); 
        $this->form_validation->set_rules('infodaftarContent', 'infodaftarContentEng', '');          
        $this->form_validation->set_rules('infodaftarNama', 'infodaftarNama', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $infodaftarJenjangId = $this->input->post('infodaftarJenjangId');
                $infodaftarContent = $this->input->post('infodaftarContent');
                $infodaftarContentEng = $this->input->post('infodaftarContentEng');
                $infodaftarNama = $this->input->post('infodaftarNama');

                $param = array(
                    'infodaftarJenjangId' => $infodaftarJenjangId,
                    'infodaftarContent' => $infodaftarContent,
                    'infodaftarContentEng' => $infodaftarContentEng,
                    'infodaftarNama' => $infodaftarNama,

                );

                if (empty($infodaftarIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('f_infodaftar', $param);
                } else {
                    $key = array('infodaftarId' => $infodaftarIdOld);
                    $proses = $this->{$this->_model_name}->update('f_infodaftar', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['infodaftarId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('f_infodaftar', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
