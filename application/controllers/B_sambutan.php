
                <?php
                defined('BASEPATH') OR exit('No direct script access allowed');
                define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

                class b_sambutan extends MY_Controller {
                    function __construct()
                    {
                        parent::__construct();

                        $this->_template = 'layouts/template';
                        $this->_path_page = 'pages/b_sambutan/';
                        $this->_path_js = null;
                        $this->_judul = 'B_sambutan';
                        $this->_controller_name = 'b_sambutan';
                        $this->_model_name = 'model_b_sambutan';
                        $this->_page_index = 'index';

                        $this->load->model($this->_model_name,'',TRUE);
                    }

                    public function index()
                    {
                        $data = $this->get_master($this->_path_page.$this->_page_index);
                        $data['scripts'] = [];
                        $data['datas'] = $this->{$this->_model_name}->get_ref_table('f_sambutan');
                        $data['create_url'] = site_url($this->_controller_name.'/create').'/';
                        $data['update_url'] = site_url($this->_controller_name.'/update').'/';
                        $data['delete_url'] = site_url($this->_controller_name.'/delete').'/';
                        $this->load->view($this->_template, $data);
                    }

                    public function create()
                    {	
                        $data = $this->get_master($this->_path_page.'form');	
                        $data['scripts'] = [];	
                        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
                        $data['status_page'] = 'Create';
                        $data['datas'] = false;
                        	
                        $this->load->view($this->_template, $data);
                    }

                    public function update()
                    {		
                        $data = $this->get_master($this->_path_page.'form');	
                        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
                        $data['scripts'] = [];	
                        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
                        $data['status_page'] = 'Update';
                        $key = ['sbtId'=>$keyS];
                        $data['datas'] = $this->{$this->_model_name}->get_by_id('f_sambutan',$key);
                        	
                        $this->load->view($this->_template, $data);
                    }

                    public function save()
                    {		
                        $sbtIdOld = $this->input->post('sbtIdOld');$this->form_validation->set_rules('sbtFoto','sbtFoto','trim|xss_clean');
$this->form_validation->set_rules('sbtIsi','sbtIsi','trim|xss_clean');
$this->form_validation->set_rules('sbtNama','sbtNama','trim|xss_clean');
$this->form_validation->set_rules('sbtJabatan','sbtJabatan','trim|xss_clean');

                        if($this->form_validation->run()) 
                        {	
                            if(IS_AJAX)
                            {
                                $sbtFoto = $this->input->post('sbtFoto');
$sbtIsi = $this->input->post('sbtIsi');
$sbtNama = $this->input->post('sbtNama');
$sbtJabatan = $this->input->post('sbtJabatan');
	

                                $param = array(
                                    'sbtFoto'=>$sbtFoto,
'sbtIsi'=>$sbtIsi,
'sbtNama'=>$sbtNama,
'sbtJabatan'=>$sbtJabatan,

                                );

                                if(empty($sbtIdOld))
                                {
                                    $proses = $this->{$this->_model_name}->insert('f_sambutan',$param);
                                } else {
                                    $key = array('sbtId'=>$sbtIdOld);
                                    $proses = $this->{$this->_model_name}->update('f_sambutan',$param,$key);
                                }

                                if($proses)
                                    message($this->_judul.' Berhasil Disimpan','success');
                                else
                                {
                                    $error = $this->db->error();
                                    message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                                }
                            }
                        } else {
                            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
                        }
                    }

                    public function delete()
                    {
                        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
                        $key = ['sbtId'=>$keyS];
                        $proses = $this->{$this->_model_name}->delete('f_sambutan',$key);
                        if ($proses) 
                            message($this->_judul.' Berhasil Dihapus','success');
                        else
                        {
                            $error = $this->db->error();
                            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
                        }
                    }
                }
                