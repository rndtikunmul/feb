<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_files extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_files/';
        $this->_path_js = 'b_files/';
        $this->_judul = 'File Upload';
        $this->_controller_name = 'b_files';
        $this->_model_name = 'model_b_files';
        $this->_path_upload = '../upload_file/files/';
        $this->_page_index = 'index';
        $this->_logged_in = $this->session->userdata('logged_in');
        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $session_data = $this->session->userdata('logged_in');
        $user = $this->_logged_in['susrNama'];
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [$this->_path_js . 'b_files'];
        $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
         if(($this->_logged_in['susrSgroupNama'] == "ADMIN_FEB") or ($this->_logged_in['susrSgroupNama'] == "ADMIN") or ($this->_logged_in['susrSgroupNama'] == "MAHASISWA")  ){
             $data['datas'] = $this->{$this->_model_name}->all($user);
        }else{
             $data['datas'] = $this->{$this->_model_name}->all_admin($jurusan->sgroupProdiId);   
        }
        // $data['datas'] = $this->{$this->_model_name}->get_ref_table('f_files');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [$this->_path_js . 'b_files'];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $session_data = $this->session->userdata('logged_in');
        $fileIdOld = $this->input->post('fileIdOld');
        $this->form_validation->set_rules('fileNama', 'fileNama', 'required|trim|xss_clean');
        if (empty($_FILES['fileFiles']['name']) and empty($fileIdOld))
            $this->form_validation->set_rules('fileFiles', 'fileFiles', 'required|trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                if (empty($filesIdOld))
                    $string_replace = '/[^a-zA-Z0-9 ]/';
                $fileNama = str_replace(' ', '-', preg_replace($string_replace, '', (strip_tags($this->input->post('fileNama')))));
                $fileFiles = $this->input->post('fileFiles');
                $fileDatetime = date('Y-m-d H:i:s');
                $fileUserId = $this->_logged_in['susrNama'];

                $konfig = array(
                    'url'      => '../upload_file/files/',
                    'type'     => 'pdf|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx',
                    'size'     => 1024 * 8,
                    'namafile' => $fileNama . '_' . mt_rand(),
                    'name' => 'fileFiles'
                );


                if (!empty($_FILES['fileFiles']['name'])) {
                    $prosesUpload = uploadfile($konfig, $this->_model_name);
                }

                $param = array(
                    'fileNama' => $fileNama,
                    'fileFiles' => $prosesUpload['file_name'],
                    'fileDatetime' => $fileDatetime,
                    'fileUserId' => $fileUserId,

                );

                if (empty($fileIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('f_files', $param);
                } else {
                    $key = array('' => $fileIdOld);
                    $proses = $this->{$this->_model_name}->update('f_files', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['fileId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('f_files', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/files/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

    public function loadfile()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/files/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

    public function loadattach()
    {
        ob_clean();
        $this->load->helper('file');
        $file = $this->uri->segment(3);
        $path = FCPATH . '../upload_file/files/' . $file;
        $files = get_mime_by_extension($path);
        // echo $files;
        // exit();
        if ($files == 'application/pdf') {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $path . '"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($path);

        }   elseif 
            ($files == 'application/msword') {
            header('Content-type: application/msword');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    

        }   elseif 
            ($files == 'application/vnd.ms-excel') {
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path); 
            
        }   elseif 
            ($files == 'application/vnd.ms-powerpoint') {
            header('Content-type: application/vnd.ms-powerpoint');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    

        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
            header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);
            
        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    
    
        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
            header('Content-type: application/vnd.openxmlformats-officedocument.presentationml.presentation');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);
            
        }   else {
            $size = getimagesize($path);
            header('Content-Type:' . $size['mime']);
            switch ($size['mime']) {
                case 'image/png':
                    $img = imagecreatefrompng($path);

                    imagepng($img);
                    break;

                default:
                    $img = imagecreatefromjpeg($path);
                    imagejpeg($img);
                    break;
            }
            imagedestroy($img);
        }
    }
}
