<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_jurnal extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_jurnal/';
        $this->_path_js = 'b_jurnal/';
        $this->_judul = 'Jurnal';
        $this->_controller_name = 'b_jurnal';
        $this->_model_name = 'model_b_jurnal';
        $this->_path_upload = '../upload_file/abstrak/';
        $this->_page_index = 'index';
        $this->_logged_in = $this->session->userdata('logged_in');

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [$this->_path_js . 'b_jurnal'];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [$this->_path_js . 'b_jurnal'];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['f_jurnal_nama'] = $this->{$this->_model_name}->get_ref_table('f_jurnal_nama');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [$this->_path_js . 'b_jurnal'];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['jurnalId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['f_jurnal_nama'] = $this->{$this->_model_name}->get_ref_table('f_jurnal_nama');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $session_data = $this->session->userdata('logged_in');
        $jurnalIdOld = $this->input->post('jurnalIdOld');
        $this->form_validation->set_rules('jurnalnamaId', 'jurnalnamaId', 'required|trim|xss_clean');
        $this->form_validation->set_rules('jurnalJudul', 'jurnalJudul', 'required|trim|xss_clean');
        $this->form_validation->set_rules('jurnalPenulis', 'jurnalPenulis', 'required|trim|xss_clean');
        $this->form_validation->set_rules('jurnalAbst', 'jurnalAbst', 'trim|xss_clean');
        if (empty($_FILES['jurnalFile']['name']) and empty($jurnalIdOld))
            $this->form_validation->set_rules('jurnalFile', 'jurnalFile', 'required|trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {

                $jurnalJudul = $this->input->post('jurnalJudul');
                $jurnalnamaId = $this->input->post('jurnalnamaId');
                $jurnalAbst = $this->input->post('jurnalAbst');
                $jurnaluserId = $this->_logged_in['susrNama'];
                $jurnalTgl = $this->input->post('jurnalTgl');
                $jurnalPenulis = $this->input->post('jurnalPenulis');

                $konfig = array(
                    'url'      => '../upload_file/abstrak/',
                    'type'     => 'pdf|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx',
                    'size'     => 1024 * 8,
                    'namafile' => substr(strip_tags($jurnalJudul), 0, 30) . '_' . mt_rand(),
                    'name' => 'jurnalFile'
                );

                if (!empty($_FILES['jurnalFile']['name'])) {
                    $prosesUpload = uploadfile($konfig, $this->_model_name);
                }

                $param = array(
                    'jurnalnamaId' => $jurnalnamaId,
                    'jurnalJudul' => $jurnalJudul,
                    'jurnalAbst' => $jurnalAbst,
                    'jurnalFile' => $prosesUpload['file_name'],
                    'jurnaluserId' => $jurnaluserId,
                    'jurnalTgl' => date("Y-m-d",strtotime($jurnalTgl)),
                    'jurnalPenulis' => $jurnalPenulis,                   

                );

                if (empty($jurnalIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('f_jurnal', $param);
                } else {
                    $key = array('jurnalId' => $jurnalIdOld);
                    $proses = $this->{$this->_model_name}->update('f_jurnal', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['jurnalId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('f_jurnal', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
