
<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_lab extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_lab/';
        $this->_path_js = null;
        $this->_judul = 'Laboratorium';
        $this->_controller_name = 'b_lab';
        $this->_model_name = 'model_b_lab';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['labId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $labIdOld = $this->input->post('labIdOld');
        $this->form_validation->set_rules('labprodiId', 'labprodiId', 'trim|xss_clean');
        $this->form_validation->set_rules('labNama', 'labNama', 'trim|xss_clean');
        $this->form_validation->set_rules('labDesk', 'labDesk', 'trim|xss_clean');
        $this->form_validation->set_rules('labVisi', 'labVisi', 'trim|xss_clean');
        $this->form_validation->set_rules('labMisi', 'labMisi', 'trim|xss_clean');
        $this->form_validation->set_rules('labKalab', 'labKalab', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $labprodiId = $this->input->post('labprodiId');
                $labNama = $this->input->post('labNama');
                $labDesk = $this->input->post('labDesk');
                $labVisi = $this->input->post('labVisi');
                $labMisi = $this->input->post('labMisi');
                $labKalab = $this->input->post('labKalab');


                $param = array(
                    'labprodiId' => $labprodiId,
                    'labNama' => $labNama,
                    'labDesk' => $labDesk,
                    'labVisi' => $labVisi,
                    'labMisi' => $labMisi,
                    'labKalab' => $labKalab,

                );

                if (empty($labIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('f_lab', $param);
                } else {
                    $key = array('labId' => $labIdOld);
                    $proses = $this->{$this->_model_name}->update('f_lab', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['labId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('f_lab', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
