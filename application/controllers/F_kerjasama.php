
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class f_kerjasama extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/f_kerjasama/';
        $this->_path_js = 'b_prestasimhs/';
        $this->_judul = 'Kerja Sama';
        $this->_controller_name = 'f_kerjasama';
        $this->_model_name = 'model_f_kerjasama';
        $this->_page_index = 'index';
        $this->_logged_in = $this->session->userdata('logged_in');
        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] =  ['b_prestasimhs/f_kerjasama'];
        $data['user'] = $this->_logged_in['susrSgroupNama'] ;
        $data['show_url'] = site_url($this->_controller_name . '/response') . '/';
        $this->load->view($this->_template, $data);
    }

    public function response()
    {
        $this->form_validation->set_rules('jenis', 'jenis', 'trim|required|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data['scripts'] = ['b_prestasimhs/f_kerjasama'];
                $data = $this->get_master($this->_path_page.$this->_page_index);
                $jenis = $this->input->post('jenis');
                // print_r($jenis);
                // exit();
                $data['datas'] = $this->{$this->_model_name}->get_ref_table('f_kerjasama','', ['kerjasamaJenis'=>$jenis]);
                $data['create_url'] = site_url($this->_controller_name.'/create').'/';
                $data['update_url'] = site_url($this->_controller_name.'/update').'/';
                $data['delete_url'] = site_url($this->_controller_name.'/delete').'/';
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }



    public function create()
    {	
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts'] = [$this->_path_js . 'f_kerjasama'];	
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] = [$this->_path_js . 'f_kerjasama'];	
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['kerjasamaId'=>$keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('f_kerjasama',$key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {		
        $kerjasamaIdOld = $this->input->post('kerjasamaIdOld');
        $this->form_validation->set_rules('kerjasamaNama','kerjasamaNama','trim|xss_clean');
        $this->form_validation->set_rules('kerjasamaLembaga','kerjasamaLembaga','trim|xss_clean');
        $this->form_validation->set_rules('kerjasamaJenis','kerjasamaJenis','trim|xss_clean');
        $this->form_validation->set_rules('kerjasamaTahun','kerjasamaTahun','trim|xss_clean');

        if($this->form_validation->run()) 
        {	
            if(IS_AJAX)
            {
                $kerjasamaNama = $this->input->post('kerjasamaNama');
                $kerjasamaLembaga = $this->input->post('kerjasamaLembaga');
                $kerjasamaJenis = $this->input->post('kerjasamaJenis');
                $kerjasamaTahun = $this->input->post('kerjasamaTahun');
                $kerjasamaKet = $this->input->post('kerjasamaKet');
                $kerjasamaFile = $this->input->post('kerjasamaFile');


                $param = array(
                    'kerjasamaNama'=>$kerjasamaNama,
                    'kerjasamaLembaga'=>$kerjasamaLembaga,
                    'kerjasamaJenis'=>$kerjasamaJenis,
                    'kerjasamaTahun'=>$kerjasamaTahun,
                    'kerjasamaKet'=>$kerjasamaKet,


                );
                if (!empty($_FILES['berkas']['name'])) {
                    $config['upload_path'] = realpath(APPPATH . '../upload_file/files/'); //path folder
                    $config['allowed_types'] = 'pdf'; //type yang dapat diakses bisa anda sesuaikan
                    $config['max_size'] = 1024 * 5; //maksimum besar file 5M
                    $config['file_name'] = $kerjasamaNama. '_' .mt_rand(); //nama yang terupload nantinya
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('berkas'))
                        echo message(strip_tags($this->upload->display_errors()), 'error');
                    else {
                        $file = $this->upload->data();
                        $image_path = $file['full_path'];
                        $param['kerjasamaFile'] = $file['file_name'];
                      
                    }
                }
             

                if(empty($kerjasamaIdOld))
                {
                    $proses = $this->{$this->_model_name}->insert('f_kerjasama',$param);
                } else {
                    $key = array('kerjasamaId'=>$kerjasamaIdOld);
                    $proses = $this->{$this->_model_name}->update('f_kerjasama',$param,$key);
                }

                if($proses)
                    message($this->_judul.' Berhasil Disimpan','success');
                else
                {
                    $error = $this->db->error();
                    message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['kerjasamaId'=>$keyS];
        $proses = $this->{$this->_model_name}->delete('f_kerjasama',$key);
        if ($proses) 
            message($this->_judul.' Berhasil Dihapus','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
        }
    }
}
