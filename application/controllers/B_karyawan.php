<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class B_karyawan extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_karyawan/';
        $this->_path_js = 'b_karyawan/';
        $this->_judul = 'Daftar Tendik Dan Dosen';
        $this->_controller_name = 'b_karyawan';
        $this->_model_name = 'model_b_karyawan';
        $this->_page_index = 'index';
        $this->_path_upload = '../upload_file/berita/';
        $this->load->library('Aksesapi');
        $this->_logged_in = $this->session->userdata('logged_in');
        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page.$this->_page_index);
        $data['scripts'] =[$this->_path_js . 'b_karyawan'];
         $jurusan = $this->{$this->_model_name}->get_by_id('s_user_group',['sgroupNama'=>$this->_logged_in['susrSgroupNama']]);
         if(($this->_logged_in['susrSgroupNama'] == "ADMIN_FEB") or ($this->_logged_in['susrSgroupNama'] == "ADMIN") ){
             $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');
        }else{
            $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi','',['prodijurusanId'=>$jurusan->sgroupProdiId]); 
        }

        $data['show_url'] = site_url($this->_controller_name . '/response') . '/';
        $this->load->view($this->_template, $data);
    }


         public function response()
    {
        $this->form_validation->set_rules('beritaUnit', 'beritaUnit', 'trim|required|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {

                $data = $this->get_master($this->_path_page.$this->_page_index);
                $beritaUnit = $this->input->post('beritaUnit');
                $karyawanJenis = $this->input->post('karyawanJenis');
                $data['datas'] = $this->{$this->_model_name}->all($beritaUnit,$karyawanJenis);
                // echo $this->db->last_query();
                // print_r(  $data['datas']);
                // exit();
                $data['create_url'] = site_url($this->_controller_name.'/create').'/';
                $data['update_url'] = site_url($this->_controller_name.'/update').'/';
                $data['delete_url'] = site_url($this->_controller_name.'/delete').'/';
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }

    public function create()
    {	
        $api = new Aksesapi();
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts'] = [$this->_path_js . 'b_karyawan'];	
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['datakaryawan'] = $api->Getkaryawanunit();
        // print_r($data['datakaryawan']->datas);
        // exit();
        $data['datas'] = false;
        $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $api = new Aksesapi();
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] = [$this->_path_js . 'b_karyawan'];
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['karyawanId'=>$keyS];
        $data['datakaryawan'] = $api->Getkaryawanunit();
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        //   print_r($data['datas']);
        // exit();
        $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {		
        $karyawanIdOld = $this->input->post('karyawanIdOld');
        $this->form_validation->set_rules('karyawanProdiId','karyawanProdiId','trim|xss_clean');
        $this->form_validation->set_rules('karyawanNama','karyawanNama','trim|xss_clean');
        $this->form_validation->set_rules('karyawanJenis','karyawanJenis','trim|xss_clean');
        $this->form_validation->set_rules('karyawanJabatan','karyawanJabatan','trim|xss_clean');
       
        if($this->form_validation->run()) 
        {	
            if(IS_AJAX)
            {
                $karyawanProdiId = $this->input->post('karyawanProdiId');
                $karyawanNama = explode('-', $this->input->post('karyawanNama'));
                $karyawanJenis = $this->input->post('karyawanJenis');
                $karyawanJabatan = $this->input->post('karyawanJabatan');
                $karyawanNIP = $this->input->post('karyawanNIP');
                $karyawanDetail = $this->input->post('karyawanDetail');
                
                $keycek = ['karyawanNIP'=>$karyawanNama[0]];
                $cek = $this->{$this->_model_name}->get_karyawan($keycek);
                // secho $this->db->last_query();
                // print_r($cek);
                // exit();
                $param = array(
                    'karyawanProdiId'=>$karyawanProdiId,
                    'karyawanNama'=>$karyawanNama[1],
                    'karyawanJenis'=>$karyawanJenis,
                    'karyawanJabatan'=>$karyawanJabatan,
                    'karyawanNIP'=> $karyawanNama[0],
                    'karyawanDetail' =>$karyawanDetail
                   
                );
                   
                 if (!empty($_FILES['berkas']['name'])) {
                    $config['upload_path'] = realpath(APPPATH . '../upload_file/berita/'); //path folder
                    $config['allowed_types'] = 'jpg|jpeg'; //type yang dapat diakses bisa anda sesuaikan
                    $config['max_size'] = 1024 * 5; //maksimum besar file 5M
                    $config['file_name'] = $karyawanJenis. '_' .mt_rand(); //nama yang terupload nantinya

                 

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('berkas'))
                        echo message(strip_tags($this->upload->display_errors()), 'error');
                    else {
                        $file = $this->upload->data();
                        $image_path = $file['full_path'];
                        $param['karyawanFoto'] = $file['file_name'];
                        if (file_exists($image_path)) {
                            $upload = true;
                            $this->load->helper('image_thumb');
                            image_thumb($config['upload_path'], $param['karyawanFoto'], 400, 300);
                        } else {
                            $upload = false;
                        }
                    }
                }

                if(empty($karyawanIdOld))
                {
                    if ($cek == false){
                         $proses = $this->{$this->_model_name}->insert('f_karyawan',$param);
                     }else{
                        $error = $this->db->error();
                      message($this->_judul.' Gagal Disimpan Data Sudah Tersedia','error');

                     }
                   
                } else {
                    $key = array('karyawanId'=>$karyawanIdOld);
                    $proses = $this->{$this->_model_name}->update('f_karyawan',$param,$key);
                }

                if($proses)
                    message($this->_judul.' Berhasil Disimpan','success');
                else
                {
                    $error = $this->db->error();
                    message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['karyawanId'=>$keyS];
        $proses = $this->{$this->_model_name}->delete('f_karyawan',$key);
        if ($proses) 
            message($this->_judul.' Berhasil Dihapus','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
        }
    }
}
