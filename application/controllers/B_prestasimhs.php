<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class B_prestasimhs extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_prestasimhs/';
        $this->_path_js = 'b_prestasimhs/';
        $this->_judul = 'Prestasi Mahasiswa';
        $this->_controller_name = 'b_prestasimhs';
        $this->_model_name = 'model_b_prestasimhs';
        $this->_page_index = 'index';
        $this->_path_upload = '../upload_file/berita/';

        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page.$this->_page_index);
        $data['scripts'] =[$this->_path_js . 'b_prestasimhs'];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name.'/create').'/';
        $data['update_url'] = site_url($this->_controller_name.'/update').'/';
        $data['delete_url'] = site_url($this->_controller_name.'/delete').'/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {	
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts'] = [$this->_path_js . 'b_prestasimhs'];	
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] = [$this->_path_js . 'b_prestasimhs'];
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['prestasiId'=>$keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {		
        $prestasiIdOld = $this->input->post('prestasiIdOld');
        $this->form_validation->set_rules('prestasiTingkat','prestasiTingkat','trim|xss_clean');
        $this->form_validation->set_rules('prestasiNamaMhs','prestasiNamaMhs','trim|xss_clean');
        $this->form_validation->set_rules('prestasiNIM','prestasiNIM','trim|xss_clean');
        $this->form_validation->set_rules('prestasiProdiId','prestasiProdiId','trim|xss_clean');        
        $this->form_validation->set_rules('prestasiNamaKegiatan','prestasiNamaKegiatan','trim|xss_clean');
        $this->form_validation->set_rules('prestasiTglKegiatan','prestasiTglKegiatan','trim|xss_clean');
        $this->form_validation->set_rules('prestasiJuara','prestasiJuara','trim|xss_clean');
       
        if($this->form_validation->run()) 
        {	
            if(IS_AJAX)
            {
                $prestasiTingkat = $this->input->post('prestasiTingkat');
                $prestasiNamaMhs = $this->input->post('prestasiNamaMhs');
                $prestasiNIM = $this->input->post('prestasiNIM');
                $prestasiProdiId = $this->input->post('prestasiProdiId');
                $prestasiNamaKegiatan = $this->input->post('prestasiNamaKegiatan');
                $prestasiTglKegiatan = $this->input->post('prestasiTglKegiatan');
                $prestasiJuara = $this->input->post('prestasiJuara');
              

                $param = array(
                    'prestasiTingkat'=>$prestasiTingkat,
                    'prestasiNamaMhs'=>$prestasiNamaMhs,
                    'prestasiNIM'=>$prestasiNIM,
                    'prestasiProdiId'=>$prestasiProdiId,
                    'prestasiNamaKegiatan'=>$prestasiNamaKegiatan,
                    'prestasiTglKegiatan'=>date("Y-m-d",strtotime($prestasiTglKegiatan)),
                    'prestasiJuara'=>$prestasiJuara,
                   
                );

                 if (!empty($_FILES['berkas']['name'])) {
                    $config['upload_path'] = realpath(APPPATH . '../upload_file/berita/'); //path folder
                    $config['allowed_types'] = 'jpg|jpeg|pdf'; //type yang dapat diakses bisa anda sesuaikan
                    $config['max_size'] = 1024 * 5; //maksimum besar file 5M
                    $config['file_name'] = $prestasiNIM. '_' .mt_rand(); //nama yang terupload nantinya

                    // print_r($config);
                    // exit();

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('berkas'))
                        echo message(strip_tags($this->upload->display_errors()), 'error');
                    else {
                        $file = $this->upload->data();
                        $image_path = $file['full_path'];
                        $param['prestasiBerkas'] = $file['file_name'];
                        if (file_exists($image_path)) {
                            $upload = true;
                            $this->load->helper('image_thumb');
                            image_thumb($config['upload_path'], $param['prestasiBerkas'], 400, 300);
                        } else {
                            $upload = false;
                        }
                    }
                }

                if(empty($prestasiIdOld))
                {
                    $proses = $this->{$this->_model_name}->insert('f_prestasimhs',$param);
                } else {
                    $key = array('prestasiId'=>$prestasiIdOld);
                    $proses = $this->{$this->_model_name}->update('f_prestasimhs',$param,$key);
                }

                if($proses)
                    message($this->_judul.' Berhasil Disimpan','success');
                else
                {
                    $error = $this->db->error();
                    message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['prestasiId'=>$keyS];
        $proses = $this->{$this->_model_name}->delete('f_prestasimhs',$key);
        if ($proses) 
            message($this->_judul.' Berhasil Dihapus','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
        }
    }

    public function loadattach()
    {
        ob_clean();
        $this->load->helper('file');
        $file = $this->uri->segment(3);
        $path = FCPATH . '../upload_file/berita/' . $file;
        $files = get_mime_by_extension($path);
        // echo $files;
        // exit();
        if ($files == 'application/pdf') {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $path . '"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($path);

        }   elseif 
            ($files == 'application/msword') {
            header('Content-type: application/msword');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    

        }   elseif 
            ($files == 'application/vnd.ms-excel') {
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path); 
            
        }   elseif 
            ($files == 'application/vnd.ms-powerpoint') {
            header('Content-type: application/vnd.ms-powerpoint');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    

        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
            header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);
            
        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    
    
        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
            header('Content-type: application/vnd.openxmlformats-officedocument.presentationml.presentation');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);
            
        }   else {
            $size = getimagesize($path);
            header('Content-Type:' . $size['mime']);
            switch ($size['mime']) {
                case 'image/png':
                    $img = imagecreatefrompng($path);

                    imagepng($img);
                    break;

                default:
                    $img = imagecreatefromjpeg($path);
                    imagejpeg($img);
                    break;
            }
            imagedestroy($img);
        }
    }
}
