<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class sdm extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_controller_name = 'sdm';
		$this->_model = 'model_f_master';
		$this->load->model('model_berita','',TRUE);
        $this->load->model('model_b_berita','',TRUE);
        $this->load->model('model_prodi','',TRUE);
		$this->load->model($this->_model, '', TRUE);
		$this->load->model('Model_b_karyawan', '', TRUE);
		$this->load->library('Aksesapi');
	}

	public function index()
    {
    	// $data = $this->get_master('page/sdm/view');
    	$data['is_active'] = 'Sumber Daya Manusia';	
		$data['pages'] = 'page/sdm/view';
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$data['data'] = $this->model_berita->get(5);
		$data['all'] = $this->model_berita->get(5);
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['berita'] = $this->model_berita->get_berita('BERITA, KEGIATAN',5);
		$data['searchkaryawan_url'] = site_url('sdm/searchpost').'/';
		$data['search_url'] = site_url('f_home/searchpost').'/';
		 $data['informasi'] = $this->{$this->_model}->get_informasi();
		// $data['datas'] = $this->model_berita->get_berita();
		 $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
		 $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $data['menu'] = menu();
        $data['datas'] = false;
		$this->load->view('page/template', $data);
    }

	public function response()
    {
    	$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['data'] = $this->model_berita->get(5);
        $data['menu'] = menu();
		$pages = $this->_path_page . 'response';
        $this->load->view($pages, $data);
    }

	public function post($labId)//single post page
    {
    	$data['datas'] = false;
    	$api = new Aksesapi();
    	$data['is_active'] = 'List Sumber Daya Manusia';
        $data['pages'] = 'page/sdm/response';

        if ($labId == 1){
        	$jenis = 'dosen';
        	$is_active = 'Dosen PNS';
        }elseif ($labId == 12){
        	$jenis = 'dosenonpns';
        	$is_active = 'Dosen Non PNS';
        }elseif ($labId == 2){
        	$jenis = 'tendikpns';
        	$is_active = 'Pegawai Tendik PNS';
        }else{
        	$jenis = 'tendiknonpns';
        	$is_active = 'Pegawai Tendik NON PNS';
        }
        $data['detail_url'] = site_url('sdm/detail').'/';
	     $perpage = 10;
	     $offset = $this->uri->segment(4);
	     $getnip = $this->Model_b_karyawan->getDataPagination($perpage, $offset,['karyawanJenis'=>$jenis])->result();
			     $datanew = false;
			     if($getnip!==false) 
			     {
			     	foreach ($getnip as $row) 
			     	{
			     		$foto= false;
			     		$api = new Aksesapi();
			     		$pegawai = $api->Getkaryawan( $row->karyawanNIP);
			     		$foto =  $this->{$this->_model}->get_by_id('f_karyawan',['karyawanNIP'=>$row->karyawanNIP]);  
						if($foto == true and $pegawai->status == true) 
						{
							$pegawai->datas->karyawanFoto = $foto->karyawanFoto;
						}else{ 
							$pegawai->datas = false; 
						}   			
						$datanew[] = $pegawai;
			
			     	}
			     }

		$data['datassdm'] = $datanew;
		$data['jenis'] = $jenis;
		$config['base_url'] = site_url('sdm/post/'.$labId.'/');
	    $config['total_rows'] = $this->Model_b_karyawan->getAll(['karyawanJenis'=>$jenis])->num_rows();
	    // $config['total_rows'] = $jumlh ; 
	   
		$config['per_page'] = $perpage;
		$config['full_tag_open']    = '<ul class="pagination">';
		$config['full_tag_close']   = '</ul>';
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['first_tag_open']   = '<li class="page-item page-link">';
		$config['first_tag_close']  = '</li>';
		$config['prev_link']        = '&laquo';
		$config['prev_tag_open']    = '<li class="page-item page-link">';
		$config['prev_tag_close']   = '</li>';
		$config['next_link']        = '&raquo';
		$config['next_tag_open']    = '<li class="page-item page-link">';
		$config['next_tag_close']   = '</li>';
		$config['last_tag_open']    = '<li class="page-item page-link">';
		$config['last_tag_close']   = '</li>';
		$config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
		$config['cur_tag_close']    = '</a></li>';
		$config['num_tag_open']     = '<li class="page-item page-link">';
		$config['num_tag_close']    = '</li>';
     	$this->pagination->initialize($config);
     	$data['informasi'] = $this->{$this->_model}->get_informasi();
        $data['is_active'] = $is_active;	
        $data['offset'] =$offset;
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$data['all'] = $this->model_berita->get(5);
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(4);
		$data['langganan'] = $this->{$this->_model}->get_langganan(1);
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['search_url'] = site_url('f_home/searchpost').'/';
		$data['searchkaryawan_url'] = site_url('sdm/searchpost').'/';
        $data['menu'] = menu(); 
        $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
        $data['agenda'] = $this->model_berita->get_berita('Agenda',5);
      
        $this->load->view('page/template', $data);
    }

    public function detail($labId)//single post page
    {

    	$api = new Aksesapi();
    	$keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
    	$data['is_active'] = 'List Sumber Daya Manusia';
        $data['pages'] = 'page/sdm/detail';
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
        $datanip = $this->Model_b_karyawan->sdm_dosen_detail($keyS);
         // echo $this->db->last_query();
        $data['datassdm'] = $api->Getkaryawan($keyS);
        $data['penelitian'] = $api->penelitian_by_id($keyS);
        $data['pengabdian'] = $api->pengabdian_by_id($keyS);
        $data['karyawan'] = $datanip;
        $data['datas'] = false;
     //      		print_r(  $data['penelitian']);
				 // exit();
        $data['informasi'] = $this->{$this->_model}->get_informasi();
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$data['all'] = $this->model_berita->get(5);
		$data['pin'] = $this->model_berita->get_berita_pin(5);
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(4);
		$data['langganan'] = $this->{$this->_model}->get_langganan(1);
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
		$data['search_url'] = site_url('f_home/searchpost').'/';
		$data['searchkaryawan_url'] = site_url('sdm/searchpost').'/';
        $data['menu'] = menu(); 
         $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
         	$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $this->load->view('page/template', $data);
    }


   public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}


	public function searchpost()
	{

		$keyword = $this->input->get('searchkaryawan', true);
		// echo "<pre>";
		// print_r ($keyword);
		// echo "</pre>";exit();
		$this->session->keyword = $keyword;
		if ($keyword==NULL) {
			redirect('sdm');
		}
		$data['offset'] =0;
		$data['keyword'] = $keyword;
		$data['is_active'] = 'Sumber Daya Manusia';	
		$data['pages'] = 'page/sdm/response';
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['informasi'] = $this->{$this->_model}->get_informasi();
		$data['lab'] = $lab;
		$data['keanggotaan'] = $this->{$this->_model}->get_keanggotaan(5);
		$data['langganan'] = $this->{$this->_model}->get_langganan(5);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['stat'] = $stat;
		$link = $this->{$this->_model}->get_link();
		$data['link'] = $link;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['data'] = $this->model_berita->get(5);
		$data['all'] = $this->model_berita->get(5);
		$data['search_url'] = site_url($this->_controller_name.'/searchpost').'/';
		$data['searchkaryawan_url'] = site_url('sdm/searchpost').'/';
        $data['menu'] = menu();
        $data['detail_url'] = site_url('sdm/detail').'/';
		$data['data'] = $this->model_berita->get(5);
		$data['agenda'] = $this->model_berita->get_berita('Agenda',5);
        $getnip = $this->Model_b_karyawan->search($keyword);
        // echo $this->db->last_query();
        //         print_r($getnip);
        // exit();
        $api = new Aksesapi();

           $datanew = false;
			     if($getnip!==false) 
			     {
			     	foreach ($getnip as $row) 
			     	{
			     		$api = new Aksesapi();
			     		$pegawai = $api->Getkaryawan( $row->karyawanNIP);
			     		$foto =  $this->{$this->_model}->get_by_id('f_karyawan',['karyawanNIP'=>$row->karyawanNIP]); 
						 if($foto == true and $pegawai->status == true) 
						 {
							 $pegawai->datas->karyawanFoto = $foto->karyawanFoto;
						 }else{ 
							 $pegawai->datas = false; 
						 }  

			     			$datanew[] = $pegawai;
			     		
			     	}
			     }
		$data['datas'] = false;
		$data['datassdm'] = $datanew;
        $data['footer'] = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
		$this->load->view('page/template',$data);
	}


}
