<header id="header" class="header-no-border-bottom has-nav-bar" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 140, 'stickySetTop': '-90px', 'stickyChangeLogo': true}">
	<div class="header-top header-top-secondary header-top-style-3">
		<div class="header-body">
			<div class="container">
				<div class="header-row py-2">
				<div class="header-column justify-content-start">
						<div class="header-row">
						<h4 style="font-size:18px;color: #076a1e; margin-top: 8px">AGENDA |</h4>
									<strong>
									<span class="word-rotator" data-plugin-options="{'delay': 3500, 'animDelay': 400}">
										<span class="word-rotator-items">
											<?php if (!empty($agenda)) {
												foreach ($agenda as $row) : ?>
													<span><?= $row->BeritaJudulEng ?></span>
												<?php endforeach;
											} ?>
										</span>
									</span>
								</strong>
							</div>
						</div>

					<div class="header-column justify-content-end">
						<div class="header-row">
							<div class="header-row">
								<div class="header-column justify-content-end">
									<div class="header-row">
										<ul class="social-icons">
											<a href="<?= base_url(); ?>"><img alt="Porto" width="30" height="20" data-sticky-width="300" data-sticky-height="35" data-sticky-top="30" src="<?=base_url('f_home/loadslider/bendera.jpg')?>"></i></a>
											<a href="<?= base_url(); ?>Eng/"><img alt="Porto" width="30" height="20" data-sticky-width="300" data-sticky-height="35" data-sticky-top="30" src="<?=base_url('f_home/loadslider/benderamerika.jpg')?>"></i></a>
											<li style="margin-left: 10px" class="social-icons-facebook"><a href="https://www.facebook.com/FEBUNMUL1966" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-youtube"><a href="https://www.youtube.com/channel/UCdTPhXVkxjeqI3FVNvrwrEQ" target="_blank" title="YouTube"><i class="fa fa-youtube"></i></a></li>
											<li class="social-icons-instagram"><a href="https://www.instagram.com/febunmul/" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
										</ul>
									</div>
								</div>
								<form id="cari" action="<?=$search_url?>" method="get">
									<div class="input-group input-group-4">
										<input class="form-control" placeholder="Search..." name="search" id="keyword" type="text">
										<span class="input-group-btn">
											<button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


		<div class="header-container container">
			<div class="header-row">
				<div class="header-column justify-content-start">
					<div class="header-row">
						<div class="header-logo" style=" margin: 10px;">
							<a href="<?php echo base_url(); ?>">
								<img alt="Porto" width="70" height="70" data-sticky-width="300" data-sticky-height="35" data-sticky-top="30" src="<?php echo base_url(); ?>front/img/logo/unmul.png">
							</a>

						</div>
						<h5 style="font-size:18px;color: #076a1e"><br>Faculty Of Economics and Business<br> Mulawarman University</h5>
					</div>
				</div>
				<div class="header-column justify-content-end">
					<div class="header-row">
						<ul class="header-extra-info d-none d-md-block">
							<li>
								<div class="feature-box feature-box-style-3 align-items-center">
									<div class="feature-box-icon">
										<i class="fa fa-phone"></i>
									</div>
									<div class="feature-box-info">
										<h4 class="mb-0"><?= $footer->footTlp ?></h4>
										<p><small>Get in touch with us</small></p>
									</div>
								</div>
							</li>
							<li>
								<div class="feature-box feature-box-style-3 align-items-center">
									<div class="feature-box-icon">
										<i class="fa fa-envelope"></i>
									</div>
									<div class="feature-box-info">
										<h4 class="mb-0"><?= $footer->footEmail ?></h4>
										<p><small>Send us an e-mail</small></p>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
	</div><!-- 
	<div class="header-body">
		<div class="header-container container">
			<div class="header-row">
				<div class="header-column">
					<div class="header-row">
						<div class="header-logo" style=" margin: 10px;">
							<a href="<?php echo base_url(); ?>">
								<img alt="Porto" width="70" height="70" data-sticky-width="300" data-sticky-height="35" data-sticky-top="30" src="<?php echo base_url(); ?>front/img/logo/unmul.png">
							</a>
						</div>
						<h5 style="font-size:18px;color: #076a1e"><br><?= $header->hdLogo ?><br>Universitas Mulawarman</h5>
					</div>
				</div>
				<div class="header-column justify-content-end">
					<div class="header-row pt-4 pb-2">
						<form id="cari" action="<?=$search_url?>" method="get">
							<div class="input-group input-group-4">
								<input class="form-control" placeholder="Search..." name="search" id="keyword" type="text">
								<span class="input-group-btn">
									<button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-search"></i></button>
								</span>
							</div>
						</form>
					</div>
					<div class="header-row">
						<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
							<i class="fa fa-bars"></i>
						</button>
					</div>
				</div>
			</div>
		</div> -->
		<div class="header-nav-bar">
			<div class="header-container container">
				<div class="header-row">
					<div class="header-column">
						<div class="header-row">
							<div class="header-nav justify-content-start">
								<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
									<nav class="collapse">
										<ul class="nav nav-pills" id="mainNav">
											<li class="dropdown">
												<a class="nav-item <?php echo $is_active == 'home' ? 'start active open' : ''; ?>" href="<?php echo base_url(); ?>Eng">
													Home
												</a>
											</li>
											<?php
											//print_r($menu);
											foreach ($menu as $row) {
												//echo $is_active;
												
												
												?>
												<li class="dropdown">
													<?php if($row['headNama'] != 'Information' and  $row['headNama'] != 'organisasi'){?>
														<a class="dropdown-item dropdown-toggle <?= $row['headId'] == $is_active ? 'active open' : '' ?>" href="#">
															
															
																<?= $row['headNama'] ?>
																
														</a>
													<?php }?>
													<ul class="dropdown-menu">
														<?php
														foreach ($row['child'] as $cRow) {
															$head = $cRow->pageHead ; 
															
															?>
															<?php if($cRow->pageJudul != 'Sumber Daya Manusia'){?>
																<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/post/<?= $cRow->pageNama ?>"><?= $cRow->pageNamaEng ?></a></li>
															<?php }?>

															<?php if($cRow->pageJudul == 'Sumber Daya Manusia'){?>
																<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/sdm">Human Resources</a></li>
															<?php }?>
														<?php }	?>
													</ul>
												</li>

												<?php
											}
											?>

											<li class="dropdown">
											<a class="dropdown-item dropdown-toggle" href="#">
											Publication
											</a>
											<ul class="dropdown-menu">
												<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/publikasi/penelitian/">Research</a></li>
												<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/publikasi/PM/">Community Dedication</a></li>
												<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/publikasi/jurnal/">Journal</a></li>
												<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/publikasi/conference/">Conference</a></li>
												<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/publikasi/buku/">Book</a></li>
												<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/publikasi/haki/">Haki</a></li>

											</ul>
										</li>

											<li class="dropdown">
												<a class="dropdown-item dropdown-toggle <?php echo $is_active == 'prodi' ? ' active open' : ''; ?>" href="#">
													department
												</a>
												<ul class="dropdown-menu">
													<?php
													if ($prodi != false) :
														$i = 1; $head = null;
														foreach ($prodi as $row) :
															if($head!=$row->jurusanNama):
																$head = $row->jurusanNama;	
																$key = $this->encryptions->encode($row->prodiId,$this->config->item('encryption_key'));												
																if($i>1):
																	?>
																</ul>
															</li>


															<?php
														endif;
														?>
														<li class="dropdown-submenu">
															<a class="dropdown-item" href=""><?= $row->jurusanNama ?></a>
															<ul class="dropdown-menu">
																<?php
															endif;

															?>
															<?php $ke = $this->encryptions->encode($row->prodiId,$this->config->item('encryption_key'));	?>
															<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/prodi/<?= $ke ?>"><?=$row->prodiNama?></a>
															</li>
															<?php
															if(count($prodi)==$i):
																?>
															</ul>
														</li>
														<?php
													endif;
													$i++;
												endforeach;
											endif;
											?>
										</ul>
									</li>

									<li class="dropdown">
										<a class="dropdown-item dropdown-toggle" href="#">
											Student and Alumni
										</a>
										<ul class="dropdown-menu">
											<li class="dropdown-submenu">
												<a class="dropdown-item" href="#">Admission of new Students</a>
												<ul class="dropdown-menu">
													<li class="dropdown-submenu">
														<?php if ($informasi != false) :
															$i = 1; $head = null;
															foreach ($informasi as $row) :
																if($head!=$row->jenjangNama):
																	$head = $row->jenjangNama;	
																	$key = $this->encryptions->encode($row->jenjangId,$this->config->item('encryption_key'));						
																	if($i>1): ?>		
																	</ul>
																</li>
															<?php endif; ?>
															<li class="dropdown-submenu">
																<a class="dropdown-item" href=""><?= $row->jenjangNama ?></a>
																<ul class="dropdown-menu">
																<?php endif; ?>
																<?php $ke = $this->encryptions->encode($row->infodaftarId,$this->config->item('encryption_key'));	?>
																<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/postinfoeng/<?= $ke ?>"><?=$row->infodaftarNama?></a>
																</li>
																<?php if(count($informasi)==$i): ?>
																</ul>
															</li>
														<?php endif; $i++; endforeach; endif;?>
													</li>
												</ul>
												<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/post/Organisasi.Mahasiswa">Student organizations</a></li>
												<li>
													<a class="dropdown-item" <?php echo $is_active == 'prestasi' ? 'start active open' : ''; ?>" href="<?php echo base_url(); ?>prestasimhs/">The most Oustanding Student</a>
												</li>
												<li>
													<a class="dropdown-item" <?php echo $is_active == 'prestasi' ? 'start active open' : ''; ?>" href="https://perkasa.unmul.ac.id/perkasa">Trace Study</a>
												</li>
												<!-- <li>
													<a class="dropdown-item" <?php echo $is_active == 'prestasi' ? 'start active open' : ''; ?>" href="<?php echo base_url(); ?>prestasimhs/">Forum Alumni</a>
												</li> -->


											</li>
										</ul>

										<li class="dropdown">
											<a class="dropdown-item dropdown-toggle" href="#">
												Cooperation
											</a>
											<ul class="dropdown-menu">

												<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/Kerjasama/post/1/">Domestic</a></li>
												<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/Kerjasama/post/2/">International</a></li>
											</ul>
										</li>
										<li class="dropdown">
											<a class="dropdown-item dropdown-toggle" href="#">
												Facility
											</a>
											<ul class="dropdown-menu">
												<li><a class="dropdown-item" href="http://perpustakaan.unmul.ac.id/feb/">Library</a></li>


												<li class="dropdown-submenu">
													<a class="dropdown-item" href="#">Layanan Sertifikasi</a>
													<ul class="dropdown-menu">
														<li><a class="dropdown-item" href="https://iapi.or.id/">CPA</a></li>
													</ul>
												</li>
												<li><a class="dropdown-item" href="https://www.idx.co.id/">Bursa Efek</a></li>

											</ul>
										</li>
										
										<li class="dropdown">
											<a class="dropdown-item dropdown-toggle <?php echo $is_active == 'ragam' ? ' active open' : ''; ?>" href="#">
												News
											</a>
											<ul class="dropdown-menu">
												<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/News/Bulletin/">Bulletin</a></li>
												<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/News/News/">News</a></li>
												<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/News/Event/">Event</a></li>
												<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/News/Agenda/">Agenda</a></li>
												<li><a class="dropdown-item" href="<?= base_url(); ?>Eng/News/Student/">Student</a></li>
											</ul>
										</li>
									</ul>

								</nav>

							</div>

						</div>

					</div>
				</div>					
			</div>
		</div>
	</div>
</div>
</header>