<div role="main" class="main">
<div class="slider-container light rev_slider_wrapper">
					<div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 500, 'gridheight': 500, 'responsiveLevels': [4096,1200,992,500]}">
						<ul>
							<li data-transition="fade">
								<img src="img/slides/light-cover.jpg"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat" 
									class="rev-slidebg">

								<div class="rs-background-video-layer" 
									data-forcerewind="on" 
									data-volume="mute" 
									data-videowidth="100%" 
									data-videoheight="100%" 
									data-videomp4="video/light.mp4" 
									data-videopreload="preload" 
									data-videoloop="loop" 
									data-forceCover="1" 
									data-aspectratio="16:9" 
									data-autoplay="true" 
									data-autoplayonlyfirsttime="false" 
									data-nextslideatend="true" 
								></div>

								<div class="tp-caption"
									data-x="center" data-hoffset="-150"
									data-y="188"
									data-start="1000"
									style="z-index: 5"
									data-transform_in="x:[-300%];opacity:0;s:500;"><img src="img/slides/slide-title-border-light.png" alt=""></div>

								<div class="tp-caption top-label"
									data-x="center" data-hoffset="0"
									data-y="188"
									data-start="500"
									style="z-index: 5"
									data-transform_in="y:[-300%];opacity:0;s:500;">DO YOU NEED A NEW</div>

								<div class="tp-caption"
									data-x="center" data-hoffset="150"
									data-y="188"
									data-start="1000"
									style="z-index: 5"
									data-transform_in="x:[300%];opacity:0;s:500;"><img src="img/slides/slide-title-border-light.png" alt=""></div>

								<div class="tp-caption main-label"
									data-x="center" data-hoffset="0"
									data-y="218"
									data-start="1500"
									data-whitespace="nowrap"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">WEB DESIGN?</div>

								<div class="tp-caption bottom-label"
									data-x="center" data-hoffset="0"
									data-y="288"
									data-start="2000"
									data-fontsize="['20','20','20','30']"
									style="z-index: 5"
									data-transform_in="y:[100%];opacity:0;s:500;">Check out our options and features.</div>
								
							</li>
						</ul>
					</div>
				</div>
    <div class="slider-container rev_slider_wrapper">
        <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 600, 'disableProgressBar': 'on', 'responsiveLevels': [4096,1200,992,500]}">
            <ul>
                <!-- <li data-transition="fade">
                    <img src="<?php echo base_url(); ?>front/img/bg/papan-nama.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">

                    <div class="tp-caption" data-x="177" data-y="188" data-start="1000" data-transform_in="x:[-300%];opacity:0;s:500;"><img src="<?php echo base_url(); ?>front/img/slides/slide-title-border.png" alt=""></div>

                    <div class="tp-caption top-label" data-x="227" data-y="180" data-start="500" data-transform_in="y:[-300%];opacity:0;s:500;">DO YOU NEED A NEW</div>

                    <div class="tp-caption" data-x="480" data-y="188" data-start="1000" data-transform_in="x:[300%];opacity:0;s:500;"><img src="<?php echo base_url(); ?>front/img/slides/slide-title-border.png" alt=""></div>

                    <div class="tp-caption main-label" data-x="135" data-y="210" data-start="1500" data-whitespace="nowrap" data-transform_in="y:[100%];s:500;" data-transform_out="opacity:0;s:500;" data-mask_in="x:0px;y:0px;">WEB DESIGN?</div>

                    <div class="tp-caption bottom-label" data-x="['185','185','185','95']" data-y="280" data-start="2000" data-fontsize="['20','20','20','30']" data-transform_in="y:[100%];opacity:0;s:500;">Check out our options and features.</div>

                    <div class="tp-caption" data-x="['910','910','910','930']" data-y="248" data-start="2500" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1300;"><img src="<?php echo base_url(); ?>front/img/slides/slide-concept-2-1.png" alt=""></div>

                    <div class="tp-caption" data-x="['960','960','960','980']" data-y="200" data-start="3500" data-transform_in="y:[300%];opacity:0;s:300;"><img src="<?php echo base_url(); ?>front/img/slides/slide-concept-2-2.png" alt=""></div>

                    <div class="tp-caption" data-x="['930','930','930','950']" data-y="170" data-start="3650" data-transform_in="y:[300%];opacity:0;s:300;"><img src="<?php echo base_url(); ?>front/img/slides/slide-concept-2-3.png" alt=""></div>

                    <div class="tp-caption" data-x="['880','880','880','900']" data-y="130" data-start="3750" data-transform_in="y:[300%];opacity:0;s:300;"><img src="<?php echo base_url(); ?>front/img/slides/slide-concept-2-4.png" alt=""></div>

                    <div class="tp-caption" data-x="['610','610','610','630']" data-y="80" data-start="3950" data-transform_in="y:[300%];opacity:0;s:300;"><img src="<?php echo base_url(); ?>front/img/slides/slide-concept-2-5.png" alt=""></div>

                    <div class="tp-caption blackboard-text" data-x="['640','640','640','660']" data-y="300" data-start="3950" data-fontsize="['37','37','37','47']" data-transform_in="y:[300%];opacity:0;s:300;">Think</div>

                    <div class="tp-caption blackboard-text" data-x="['665','665','665','685']" data-y="350" data-start="4150" data-fontsize="['47','47','47','57']" data-transform_in="y:[300%];opacity:0;s:300;">Outside</div>

                    <div class="tp-caption blackboard-text" data-x="['690','690','690','710']" data-y="400" data-start="4350" data-fontsize="['32','32','32','42']" data-transform_in="y:[300%];opacity:0;s:300;">The box :)</div>
                </li>
                <li data-transition="fade">
                    <img src="<?php echo base_url(); ?>front/img/bg/perpus.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>

                    <div class="tp-caption top-label" data-x="['155','155','155','110']" data-y="['100','100','100','150']" data-start="500" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;"><img src="<?php echo base_url(); ?>front/img/slides/slide-concept.png" alt=""></div>

                    <div class="tp-caption blackboard-text" data-x="['285','285','285','225']" data-y="['180','180','180','230']" data-start="1000" data-fontsize="['30','30','30','40']" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;">easy to</div>

                    <div class="tp-caption blackboard-text" data-x="['285','285','285','225']" data-y="['220','220','220','270']" data-start="1200" data-fontsize="['40','40','40','50']" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;">customize!</div>

                    <div class="tp-caption main-label" data-x="['685','685','685','630']" data-y="['190','190','190','220']" data-start="1800" data-whitespace="nowrap" data-fontsize="['62','62','62','82']" data-transform_in="y:[100%];s:500;" data-transform_out="opacity:0;s:500;" data-mask_in="x:0px;y:0px;">DESIGN IT!</div>

                    <div class="tp-caption bottom-label" data-x="['685','685','685','595']" data-y="['250','250','250','300']" data-start="2000" data-fontsize="['20','20','20','30']" data-lineheight="['20','20','20','30']" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;s:600;e:Power4.easeInOut;" data-transform_out="opacity:0;s:500;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05">Create slides with brushes and fonts</div>

                </li> -->
                <li data-transition="fade">
                <!-- <video autoplay>
                    <source src="<?php echo base_url(); ?>front/video/vid-fk.mp4" type="video/mp4">
                </video> -->
                    <img src="<?php echo base_url(); ?>front/video/vid-fk.mp4"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat" 
									class="rev-slidebg">
                    <div class="rs-background-video-layer" 
                        data-forcerewind="on" 
                        data-volume="mute" 
                        data-videowidth="100%" 
                        data-videoheight="100%" 
                        data-videomp4="<?php echo base_url(); ?>front/video/vid-fk.mp4" 
                        data-videopreload="preload" 
                        data-videoloop="loop" 
                        data-forceCover="1" 
                        data-aspectratio="16:9" 
                        data-autoplay="true" 
                        data-autoplayonlyfirsttime="true" 
                        data-nextslideatend="true" 
                    ></div>
                </li>
            </ul>
        </div>
    </div>

    <!-- <section class="parallax section section-text-light section-parallax section-center mt-0" data-plugin-parallax data-plugin-options="{'speed': 0.5}" data-image-src="<?php echo base_url(); ?>front/img/bg/perpus.jpg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options="{'items': 1, 'loop': true, 'delay': 9000}">
                        <div>
                            <div class="col">
                                <div class="testimonial testimonial-style-2 testimonial-with-quotes mb-0">
                                    <div class="testimonial-author">
                                        <img src="<?php echo base_url(); ?>front/img/bg/perpus.jpg" class="img-fluid rounded-circle" alt="">
                                    </div>
                                    <blockquote>
                                    <p>Menjadi intitusi pendidikan tinggi unggul dalam menyelenggarakan pendidikan, pengembangan sains dan teknologi, bertumpu pada hutan tropis lembab dan lingkungannya tahun 2034.</p>
                                    </blockquote>
                                    <div class="testimonial-author">
                                        <p><strong>Dr. Eng. Idris Mandang, M.Si.</strong><span>Dekan Fakultas MIPA Unmul</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="col">
                                <div class="testimonial testimonial-style-2 testimonial-with-quotes mb-0">
                                    <div class="testimonial-author">
                                        <img src="<?php echo base_url(); ?>front/img/bg/perpus.jpg" class="img-fluid rounded-circle" alt="">
                                    </div>
                                    <blockquote>
                                    <p>Menjadi intitusi pendidikan tinggi unggul dalam menyelenggarakan pendidikan, pengembangan sains dan teknologi, bertumpu pada hutan tropis lembab dan lingkungannya tahun 2034.</p>
                                    </blockquote>
                                    <div class="testimonial-author">
                                        <p><strong>Dr. Eng. Idris, M.Si.</strong><span>Wakil Dekan I Fakultas MIPA Unmul</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <section class="section mt-0 section-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><strong>Berita</strong> dan <strong>Artikel</strong> Terbaru</h2>
                </div>
            </div>
            <div class="row mt-4">
                <?php if (!empty($data)) {
                    foreach ($data as $row) : ?>
                        <div class="col-lg-3">
                            <img class="img-fluid img-thumbnail" src="<?php echo base_url('f_home/loadthumb/') . $row->beritaBanner ?>" alt="Blog">
                            <div class="recent-posts mt-3 mb-4">
                                <article class="post">
                                    <h4><a class="text-dark" href="<?php echo base_url() ?>berita/post/<?= $row->beritaNama ?>"><b><?= $row->beritaJudul ?></b></a></h4>
                                    <p><?= substr(strip_tags($row->beritaContent), 0, 180) . '...'; ?></p>
                                    <div class="post-meta">
                                        <span><i class="fa fa-calendar"></i> <?= $row->beritaDatetime ?></span>
                                        <span><i class="fa fa-user"></i> By <a href="#"><?= $row->beritaAuthor ?></a> </span>
                                        <!-- <span><i class="fa fa-tag"></i> <a href="#">Duis</a>, <a href="#">News</a> </span> -->
                                    </div>
                                </article>
                            </div>
                        </div>
                <?php endforeach;
                } ?>
            </div>
        </div>
    </section>

    <!-- <section class="section section-tertiary">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><strong>Pengumuman </strong></h2>
                </div>
            </div>
            <div class="row">
                <?php if (!empty($pengumuman)) {
                    foreach ($pengumuman as $row) : ?>
                        <div class="col-lg-3">
                            <div class="recent-posts mt-4">
                                <article class="post">
                                    <div class="date">
                                        <span class="day"><?= date('d', strtotime($row->beritaDatetime)) ?></span>
                                        <span class="month"><?= date('M', strtotime($row->beritaDatetime)) ?></span>
                                    </div>
                                    <h4><a href="blog-post.html"><?= $row->beritaJudul ?></a></h4>

                                </article>
                            </div>
                        </div>
                <?php endforeach;
                } ?>
            </div>
        </div>
    </section> -->

    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-6 p-0">
                <section class="section section-secondary h-100 m-0">
                    <div class="row justify-content-end m-0">
                        <div class="col-half-section col-half-section-right">
                            <h2 class="heading-dark"><strong>Pengumuman</strong></h2>
                            <div class="row">
                                <?php if (!empty($pengumuman)) {
                                    foreach ($pengumuman as $row) : ?>
                                        <div class="col-sm-6">
                                            <div class="recent-posts">
                                                <article class="post">
                                                    <div class="date">
                                                        <span class="day"><?= date('d', strtotime($row->beritaDatetime)) ?></span>
                                                        <span class="month"><?= date('M', strtotime($row->beritaDatetime)) ?></span>
                                                    </div>
                                                    <h4 class="heading-primary"><a href="blog-post.html"><?= $row->beritaJudul ?></a></h4>
                                                    </article>
                                            </div>
                                        </div>
                                <?php endforeach;
                                } ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-6 p-0">
                <section class="section section-tertiary h-100 m-0">
                    <div class="row m-0">
                        <div class="col-half-section">
                            <h2 class="heading-dark"><strong>Agenda</strong></h2>
                            <div class="row">
                                <?php if (!empty($agenda)) {
                                    foreach ($agenda as $row) : ?>
                                        <div class="col-sm-6">
                                            <div class="recent-posts">
                                                <article class="post">
                                                    <div class="date">
                                                        <span class="day"><?= date('d', strtotime($row->beritaDatetime)) ?></span>
                                                        <span class="month"><?= date('M', strtotime($row->beritaDatetime)) ?></span>
                                                    </div>
                                                    <h4 class="heading-primary"><a href="blog-post.html"><?= $row->beritaJudul ?></a></h4>
                                                    </article>
                                            </div>
                                        </div>
                                <?php endforeach;
                                } ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </div>
    </div>

    <section class="section">

        <hr class="short">

        <div class="row counters">
            <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
                <div class="counter counter-dark">
                    <i class="fa fa-handshake-o"></i>
                    <strong data-to="6">0</strong>
                    <label>Kerjasama</label>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
                <div class="counter counter-dark">
                    <i class="fa fa-book"></i>
                    <strong data-to="4">0</strong>
                    <label>Program Studi</label>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3 mb-4 mb-sm-0">
                <div class="counter counter-dark">
                    <i class="fa fa-graduation-cap"></i>
                    <strong data-to="800">0</strong>
                    <label>Mahasiswa</label>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="counter counter-dark">
                    <i class="fa fa-users"></i>
                    <strong data-to="80">0</strong>
                    <label>Tenaga Pendidik</label>
                </div>
            </div>
        </div>
        <hr class="short">
    </section>

    <!-- <section class="section section-tertiary">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><strong>Agenda</strong></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="recent-posts mt-4">
                        <article class="post">
                            <div class="date">
                                <span class="day">01</span>
                                <span class="month">Nov</span>
                            </div>
                            <h4><a href="blog-post.html">Agenda Pemilihan Dekan Fakultas MIPA Universitas Mulawarman Periode 2018-2022</a></h4>

                        </article>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="recent-posts mt-lg-4">
                        <article class="post">
                            <div class="date">
                                <span class="day">28</span>
                                <span class="month">Mei</span>
                            </div>
                            <h4><a href="blog-post.html">The 2nd International Conference on Mathematics, Science, and Computer Science 2018</a></h4>

                        </article>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="recent-posts mt-lg-4">
                        <article class="post">
                            <div class="date">
                                <span class="day">11</span>
                                <span class="month">Mei</span>
                            </div>
                            <h4><a href="blog-post.html">Temu Alumni Prodi Statistika</a></h4>

                        </article>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="recent-posts mt-lg-4">
                        <article class="post">
                            <div class="date">
                                <span class="day">28</span>
                                <span class="month">Ags</span>
                            </div>
                            <h4><a href="blog-post.html">Open Science FMIPA 2017</a></h4>

                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section> -->


</div>