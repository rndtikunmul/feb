<div role="main" class="main">
    <div class="slider-container light rev_slider_wrapper">
        <div style= "margin-top: 100px; margin-bottom: 100px; margin-left: 100px; margin-right: 100px; " id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 500, 'gridheight': 500, 'responsiveLevels': [4096,1200,992,500]}">
            <ul>  
               <?php if (!empty($slider)) {
                foreach ($slider as $row) { ?>

                    <li data-transition="fade">

                        <img src="<?=base_url('f_home/loadslider/').$row->sliderFiles?>" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">


                        <div class="rs-background-video-layer" data-forcerewind="on" data-volume="mute" data-videowidth="100%" data-videoheight="100%" data-videomp4="<?=base_url('f_home/loadslider/').$row->sliderFiles?>" data-videopreload="preload" data-videoloop="loop" data-forceCover="1" data-aspectratio="16:9" data-autoplay="true" data-autoplayonlyfirsttime="false" ></div>

                    </li>
                <?php } } ?>

            </ul>
        </div>
    </div>

    <hr>
    <section class="section section-text-dark section-background section-center section-overlay-opacity section-overlay-opacity-light section-overlay-opacity-light-scale-1" style="background-image: url(<?=base_url('f_home/loadimage/1920x1200-pastel-yellow-solid-color-background.jpg')?>);">
        <div class="container" style="align-content: center">
            <div class="row" style="align-content: space-around; align-items: center">
                <div class="col-lg-12" style="">
                 <div class="row" style="">
                    <div class="col-lg-12 text-center">
                        <h2 style="margin-bottom: 0px"><strong>Major</strong></h2>
                    </div>
                </div>
                <div class="featured-boxes featured-boxes-style-3 featured-boxes-flat text-center" style="align-content: space-around; align-items: center">
                  <div class="row">
                    <?php if (!empty($jurusan)) {
                       foreach ($jurusan as $row) { ?>

                         <div class="col-lg-4 col-sm-6 text-center">
                            <a href="<?= $row->logoLink ?>" target="_blank" style=" margin-right: 10px; text-align: center">
                                <div class="featured-box featured-box-primary featured-box-effect-3">
                                   <div class="box-content">										
                                      <img src="<?php echo base_url('f_home/loadimage/') .$row->logoFiles?>" class="img-fluid" style="width: 200px; height: 100px;">
                                      <h5 style=font-color: black><?=$row->logoNamaEng?></h5>
                                  </div>
                              </div>
                          </a>
                      </div>
                  <?php } } ?>
              </div>
          </div>
      </div>
  </div>
</div>
</section>

<section class="section section-text-dark section-background section-center section-overlay-opacity section-overlay-opacity-light section-overlay-opacity-light-scale-9" style="background-image: url(<?=base_url('f_home/loadimage/istockphoto-1206223829-1024x1024.jpg')?>); min-height: 260px;">
<div class="container-fluid">
    <div class="row">
    <div class="col-4">
            <section class="section section-text-dark section-background section-center section-overlay-opacity section-overlay-opacity-light section-overlay-opacity-light-scale-9" style="background-image: url(<?=base_url('f_home/loadimage/3072x2304.jpg')?>); min-height: 260px;">
                <div class="row justify-content-end m-0">
                    <div class="col-half-section col-half-section-right">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h2>Link <strong>Application</strong></h2>
                            </div>
                        </div>

                        <div class="row text-center">
                            <div class="content-grid mt-5 mb-4">

                                <div class="row content-grid-row">

                                    <?php if (!empty($link)) {
                                        foreach ($link as $row) : ?>

                                           <div class="col-lg-4 text-center">
                                            <a href="<?= $row->linkLink ?>" target="_blank">
                                                <i class="icon-featured fa fa-<?= $row->linkIcon ?>"></i>
                                                <br>
                                                <h6><?= $row->linkNamaEng ?></h6>
                                            </a>
                                        </div>
                                    <?php endforeach;
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-8">
        <section class="section section-text-dark section-background section-center section-overlay-opacity section-overlay-opacity-light section-overlay-opacity-light-scale-9" style="background-image: url(<?=base_url('f_home/loadimage/3072x2304.jpg')?>); min-height: 260px;">
            <div class="container">
                <div class="">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2><strong>Pinned</strong> <strong> Posts</strong></h2>
                        </div>
                    </div>
                    <div class="row text-center">
                        <?php if (!empty($pinhome)) {
                            foreach ($pinhome as $row) : ?>
                                <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                    <span class="thumb-info thumb-info-hide-wrapper-bg">
                                       <span class="thumb-info-wrapper">
                                          <a href="about-me.html">
                                             <img src="<?php echo base_url('f_home/loadthumb/') . $row->beritaBanner ?>" class="img-fluid" alt="">

                                         </a>
                                     </span>

                                     <span class="thumb-info-caption">
                                        <div class="recent-posts mt-3 mb-4">
                                            <article class="post">
                                                <h5><a class="text-dark" href="<?php echo base_url() ?>berita/post/<?= $row->BeritaNamaEng ?>"><b><?= $row->BeritaJudulEng ?></b></a></h5>
                                                <p><?= substr(strip_tags($row->BeritaContentEng), 0, 125) . '...'; ?></p>
                                                <div class="post-meta">
                                                    <span><i class="fa fa-calendar"></i> <?=datetoindo($row->beritaDatetime)?> </span>
                                                    <span><i class="fa fa-user"></i> By <a href="#"><?= $row->beritaAuthor ?></a> </span>
                                                </div>
                                            </article>
                                        </div>
                                    </span>
                                </span>
                                </div>
                            <?php endforeach;
                        } ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
</div>
</section>

<!-- <section class="section mt-0 section-footer">
    <div class="container">
        <div class="row justify-content-end m-0">
            <div class="col-lg-12 text-center">
                <h2><strong>Berita</strong> <strong>Pilihan</strong></h2>
            </div>
        </div>
        <div class="row mt-4">
            <?php if (!empty($pinhome)) {
                foreach ($pinhome as $row) : ?>
                    <div class="col-lg-3">
                        <img class="img-fluid img-thumbnail" src="<?php echo base_url('f_home/loadthumb/') . $row->beritaBanner ?>" alt="Blog">
                        <div class="recent-posts mt-3 mb-4">
                            <article class="post">
                                <h4><a class="text-dark" href="<?php echo base_url() ?>berita/post/<?= $row->BeritaNamaEng ?>"><b><?= $row->BeritaJudulEng ?></b></a></h4>
                                <p><?= substr(strip_tags($row->BeritaContentEng), 0, 180) . '...'; ?></p>
                                <div class="post-meta">
                                    <span><i class="fa fa-calendar"></i><?=datetoindo($row->beritaDatetime)?></span>
                                    <span><i class="fa fa-user"></i> By <a href="#"><?= $row->beritaAuthor ?></a> </span>
                                </div>
                            </article>
                        </div>
                    </div>
                <?php endforeach;
            } ?>
        </div>
    </div>
</section> -->


<div class="container-fluid">
    <section class="section section-text-dark section-background  section-overlay-opacity section-overlay-opacity-light section-overlay-opacity-light-scale-9" style="background-image: url(<?=base_url('f_home/loadimage/949e5c9c81cec5a487ed1cfa87a779f7.jpg')?>); min-height: 260px;">
        <div class="row">
        <div class="col-4">
            <section class="section section-text-dark section-background section-center section-overlay-opacity section-overlay-opacity-light section-overlay-opacity-light-scale-9" style="background-image: url(<?=base_url('f_home/loadimage/3072x2304.jpg')?>); min-height: 260px;">
                <div class="row justify-content-end m-0">
                        <div class="col-half-section col-half-section-right">
                            <div class="tabs">
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="#pengumuman" data-toggle="tab" class="text-center"></i>Announcement</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#agenda" data-toggle="tab" class="text-center">Agenda</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div id="pengumuman" class="tab-pane active">
                                        <ul class="simple-post-list">
                                            <?php
                                            if($pengumuman!==false) {
                                                $i = 1;
                                                foreach($pengumuman as $row) { ?>
                                                    <li>
                                                        <div class="post-image">
                                                            <div class="img-thumbnail">
                                                                <a href="<?php echo base_url() ?>berita/post/<?= $row->BeritaNamaEng ?>">
                                                                    <img style="width:30px;height:30px;" src="<?php echo base_url('berita/loadthumb/') . $row->beritaBanner ?>" alt="">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="post-info">
                                                            <a href="<?php echo base_url() ?>berita/post/<?= $row->BeritaNamaEng ?>" style="font-size: 18px"><?=$row->BeritaJudulEng?></a>
                                                            <div class="post-meta">
                                                                <?=datetoindo($row->beritaDatetime)?> 
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php }
                                            }?>                                        
                                        </ul>
                                        <h4 class="heading-dark"><a class="text-dark" href="<?= base_url(); ?>pengumuman/"><strong>Others Announcement</strong></a></h4>
                                    </div>
                                    <div id="agenda" class="tab-pane">
                                        <ul class="simple-post-list">
                                            <?php
                                            if($agenda!==false) {
                                                $i = 1;
                                                foreach($agenda as $row) { ?>
                                                    <li>
                                                        <div class="post-image">
                                                            <div class="img-thumbnail">
                                                                <a href="<?php echo base_url() ?>berita/post/<?= $row->BeritaNamaEng?>">
                                                                    <img style="width:30px;height:30px;" src="<?php echo base_url('berita/loadthumb/') . $row->beritaBanner ?>" alt="">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="post-info">
                                                            <a href="<?php echo base_url() ?>berita/post/<?= $row->BeritaNamaEng ?>" style="font-size: 18px"><?=$row->BeritaJudulEng?></a>
                                                            <div class="post-meta">
                                                                <?=datetoindo($row->beritaDatetime)?> 
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php }
                                            }?>                                        
                                        </ul>
                                        <h4 class="heading-dark"><a class="text-dark" href="<?= base_url(); ?>agenda/"><strong>Others Agenda</strong></a></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-8">
            <section class="section section-text-dark section-background section-center section-overlay-opacity section-overlay-opacity-light section-overlay-opacity-light-scale-9" style="background-image: url(<?=base_url('f_home/loadimage/3072x2304.jpg')?>); min-height: 260px;">
                <div class="container">
                        <div class="">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <h2><strong>Latest</strong> News <strong>and</strong> Article</h2>
                                </div>
                            </div>

                            <div class="row text-center">
                                <?php if (!empty($data)) {
                                    foreach ($data as $row) : ?>
                                        <div class="col-lg-3">
                                            <img class="img-fluid img-thumbnail" src="<?php echo base_url('f_home/loadthumb/') . $row->beritaBanner ?>" alt="Blog">
                                            <div class="recent-posts mt-3 mb-4">
                                                <article class="post">
                                                    <h5><a class="text-dark" href="<?php echo base_url() ?>berita/post/<?= $row->BeritaNamaEng ?>"><b><?= $row->BeritaJudulEng ?></b></a></h5>
                                                    <p><?= substr(strip_tags($row->BeritaContentEng), 0, 100) . '...'; ?></p>
                                                    <div class="post-meta">
                                                        <span><i class="fa fa-calendar"></i> <?=datetoindo($row->beritaDatetime)?> </span>
                                                        <span><i class="fa fa-user"></i> By <a href="#"><?= $row->beritaAuthor ?></a> </span>
                                                        <!-- <span><i class="fa fa-tag"></i> <a href="#">Duis</a>, <a href="#">News</a> </span> -->
                                                    </div>
                                                </article>
                                            </div>
                                        </div>
                                    <?php endforeach;
                                } ?>
                            </div>
                            <div class="col-lg-12 text-center">
                                <h4><a class="text-dark" href="<?= base_url(); ?>berita/"><strong>Other News</strong></a></h4>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </div>
    </section>
</div>

<!-- <section class="section mt-0 section-footer">
    <div class="container">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><strong>Berita</strong> dan <strong>Artikel</strong> Terbaru</h2>
                </div>
            </div>

            <div class="row">
                <?php if (!empty($data)) {
                    foreach ($data as $row) : ?>
                        <div class="col-lg-3">
                            <img class="img-fluid img-thumbnail" src="<?php echo base_url('f_home/loadthumb/') . $row->beritaBanner ?>" alt="Blog">
                            <div class="recent-posts mt-3 mb-4">
                                <article class="post">
                                    <h4><a class="text-dark" href="<?php echo base_url() ?>berita/post/<?= $row->beritaNama ?>"><b><?= $row->beritaJudul ?></b></a></h4>
                                    <p><?= substr(strip_tags($row->beritaContent), 0, 180) . '...'; ?></p>
                                    <div class="post-meta">
                                        <span><i class="fa fa-calendar"></i> <?=datetoindo($row->beritaDatetime)?> </span>
                                        <span><i class="fa fa-user"></i> By <a href="#"><?= $row->beritaAuthor ?></a> </span>
                                    </div>
                                </article>
                            </div>
                        </div>
                    <?php endforeach;
                } ?>
            </div>
            <div class="col-lg-12 text-center">
                <h4><a class="text-dark" href="<?= base_url(); ?>berita/"><strong>Berita Lainnya</strong></a></h4>
            </div>
        </div>
    </div>
</section> -->

    <!-- <section class="section section-tertiary">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><strong>Pengumuman </strong></h2>
                </div>
            </div>
            <div class="row">
                <?php if (!empty($kegiatan)) {
                    foreach ($kegiatan as $row) : ?>
                        <div class="col-lg-3">
                            <div class="recent-posts mt-4">
                                <article class="post">
                                    <div class="date">
                                        <span class="day"><?= date('d', strtotime($row->beritaDatetime)) ?></span>
                                        <span class="month"><?= date('M', strtotime($row->beritaDatetime)) ?></span>
                                    </div>
                                    <h4><a href="blog-post.html"><?= $row->beritaJudul ?></a></h4>

                                </article>
                            </div>
                        </div>
                <?php endforeach;
                } ?>
            </div>
        </div>
    </section> -->
<!-- 
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-6 p-0">
                <section class="section section-secondary h-100 m-0">
                    <div class="row justify-content-end m-0">
                        <div class="col-half-section col-half-section-right">
                            <h2 class="heading-dark"><strong>Pengumuman</strong></h2>
                            <div class="row">
                                <?php if (!empty($pengumuman)) {
                                    foreach ($pengumuman as $row) : ?>
                                        <div class="col-sm-6">
                                            <div class="recent-posts">
                                                <article class="post">
                                                    <div class="date">
                                                        <span class="day"><?= date('d', strtotime($row->beritaDatetime)) ?></span>
                                                        <span class="month"><?= date('M', strtotime($row->beritaDatetime)) ?></span>
                                                    </div>
                                                    <h4 class="heading-primary"><a href="<?php echo base_url() ?>berita/post/<?= $row->beritaNama ?>"><?= $row->beritaJudul ?></a></h4>
                                                </article>
                                            </div>
                                        </div>
                                    <?php endforeach;
                                } ?>
                            </div>
                            <h4 class="heading-dark"><a class="text-dark" href="<?= base_url(); ?>pengumuman/"><strong>Pengumuman Lainnya</strong></a></h4> 
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-6 p-0">
                <section class="section section-tertiary h-100 m-0">
                    <div class="row m-0">
                        <div class="col-half-section">
                            <h2 class="heading-dark"><strong>Agenda</strong></h2>
                            <div class="row">
                                <?php if (!empty($agenda)) {
                                    foreach ($agenda as $row) : ?>
                                        <div class="col-sm-6">
                                            <div class="recent-posts">
                                                <article class="post">
                                                    <div class="date">
                                                        <span class="day"><?= date('d', strtotime($row->beritaDatetime)) ?></span>
                                                        <span class="month"><?= date('M', strtotime($row->beritaDatetime)) ?></span>
                                                    </div>
                                                    <h4 class="heading-primary"><a href="<?php echo base_url() ?>berita/post/<?= $row->beritaNama ?>"><?= $row->beritaJudul ?></a></h4>
                                                </article>
                                            </div>
                                        </div>
                                    <?php endforeach;
                                } ?>
                            </div>
                            <h4 class="heading-dark"><a class="text-dark" href="<?= base_url(); ?>agenda/"><strong>Agenda Lainnya</strong></a></h4>
                        </div>
                    </div>
                </section>
            </div>

        </div>
    </div> -->

    <section class="section">

        <hr class="short">

        <div class="row counters">
            <div class="col-sm-6 col-lg-3 ">
                <div class="counter counter-dark">
                    <i class="fa fa-handshake-o"></i>
                    <strong data-to="<?= $stat->statKerjasama ?>">0</strong>
                    <label>Cooperation</label>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3 ">
                <div class="counter counter-dark">
                    <i class="fa fa-book"></i>
                    <strong data-to="<?= $stat->statProdi ?>">0</strong>
                    <label>Departement</label>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3 ">
                <div class="counter counter-dark">
                    <i class="fa fa-graduation-cap"></i>
                    <strong data-to="<?= $stat->statMhs ?>">0</strong>
                    <label>Students</label>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="counter counter-dark">
                    <i class="fa fa-users"></i>
                    <strong data-to="<?= $stat->statDosen ?>">0</strong>
                    <label>Lecturer</label>
                </div>
            </div>
        </div>
        <hr class="short">
    </section>

    
    <section class="section section-text-dark section-background section-overlay-opacity section-overlay-opacity-light section-overlay-opacity-light-scale-3" style="background-image: url(<?=base_url('f_home/loadimage/design-flower-line-wallpaper-preview.jpg')?>); min-height: 260px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                 <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 style="margin-bottom: 0px"><strong>Video</strong> & <strong>Photo Gallery</strong></h2>
                    </div>
                </div>
                <h3><b>Video</b></h3>
                <div class="col-lg-12 mb-4 mb-lg-0" style="text-align: center">

                    <?php if (!empty($youtube)) {
                        foreach ($youtube as $row) { ?>

                            <iframe width="560" height="315" src="<?= $row->videoEmbed ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                        <?php } } ?>

                        <br>
                        <h6 style="text-align: right;" class="heading-dark"><a target="_blank" class="text-dark" href="https://www.youtube.com/channel/UCdTPhXVkxjeqI3FVNvrwrEQ"><strong>Find More</strong> <i class="fa fa-chevron-circle-right"></i></a></h6>
                    </div>

                    <div class="col-lg-12">
                        <h3><b>Photos</b></h3>
                        <div class="row mt-4" style="text-align: center">
                          <br>    
                          <?php if (!empty($fotofb)) {
                           foreach ($fotofb as $row) { ?>

                            <a href="<?= $row->logoLink ?>" target="_blank" style=" margin-right: 10px; text-align: center">
                                <span class="thumb-info thumb-info-centered-info">
                                    <span class="thumb-info-wrapper">
                                        <img src="<?php echo base_url('f_home/loadimage/') .$row->logoFiles?>" class="img-fluid" style="width: 350px; height: 200px;">
                                        <span class="thumb-info-title">
                                            <span class="thumb-info-inner"><?=$row->logoNamaEng?></span>
                                            <span class="thumb-info-type">Photo</span>
                                        </span>
                                        <span class="thumb-info-action">
                                            <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                                        </span>
                                    </span>
                                </span>
                            </a>
                        <?php } } ?>

                    </div>
                    <br>
                    <h6 style="text-align: right;" class="heading-dark"><a target="_blank" class="text-dark" href="https://www.facebook.com/FEBUNMUL1966/photos_albums"><strong>Find More</strong> <i class="fa fa-chevron-circle-right"></i></a></h6>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

<!-- <hr>
<section class="section section-text-dark section-background section-overlay-opacity section-overlay-opacity-light section-overlay-opacity-light-scale-3" style="background-image: url(<?=base_url('f_home/loadimage/AVXrPO.jpg')?>); min-height: 260px;">
    <div class="container" style="align-content: center">
        <div class="row" style="align-content: space-around; align-items: center">
            <div class="col-lg-12" style="">
             <div class="row" style="">
                <div class="col-lg-12 text-center">
                    <h2 style="margin-bottom: 0px"><strong>Keanggotaan Fakultas</strong></h2>
                </div>
            </div>
            <div class="featured-boxes featured-boxes-style-3 featured-boxes-flat text-center" style="align-content: space-around; align-items: center">
              <div class="row" style="align-content: space-around; align-items: center">
                <?php if (!empty($keanggotaan)) {
                   foreach ($keanggotaan as $row) { ?>


                    <div class="col-lg-3 col-sm-6 text-center">
                        <a href="<?= $row->logoLink ?>" target="_blank" style=" margin-right: 10px; text-align: center">
                            <div class="featured-box featured-box-primary featured-box-effect-3">
                               <div class="box-content">										
                                  <img src="<?php echo base_url('f_home/loadimage/') .$row->logoFiles?>" class="img-fluid" style="width: 200px; height: 100px;">
                                  <h5><?=$row->logoNama?></h5>
                              </div>
                          </div>
                      </a>
                  </div>

              <?php } } ?>
          </div>
      </div>
  </div>
</div>
</div>
</section> -->

<!-- <hr>
<section class="section section-text-dark section-background section-overlay-opacity section-overlay-opacity-light section-overlay-opacity-light-scale-3" style="background-image: url(<?=base_url('f_home/loadimage/Honeycomb-Pattern-PNG-Clipart.png')?>); min-height: 260px;">
    <div class="container" style="align-content: center">
        <div class="row" style="">
            <div class="col-lg-12" style="">
             <div class="row" style="">
                <div class="col-lg-12 text-center">
                    <h2 style="margin-bottom: 0px"><strong>Jurnal Berlangganan</strong></h2>
                </div>
            </div>
            <div class="featured-boxes featured-boxes-style-3 featured-boxes-flat text-center" style="">
              <div class="row" style="">
                <?php if (!empty($langganan)) {
                   foreach ($langganan as $row) { ?>

                     <div class="col-lg-4 col-sm-6 text-center">
                        <a href="<?= $row->logoLink ?>" target="_blank" style=" margin-right: 10px; text-align: center">
                            <div class="featured-box featured-box-primary featured-box-effect-3">
                               <div class="box-content">										
                                  <img src="<?php echo base_url('f_home/loadimage/') .$row->logoFiles?>" class="img-fluid" style="width: 200px; height: 100px;">
                                  <h5><?=$row->logoNama?></h5>
                              </div>
                          </div>
                      </a>
                  </div>
              <?php } } ?>
          </div>
      </div>
  </div>
</div>
</div>
</section> -->

</div>
</div>
</div>