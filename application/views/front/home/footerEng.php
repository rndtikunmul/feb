<hr>
<section class="section section-text-dark section-background section-overlay-opacity section-overlay-opacity-light section-overlay-opacity-light-scale-3" style="background-image: url(<?=base_url('f_home/loadimage/AVXrPO.jpg')?>); min-height: 260px;">
  <div class="container" style="align-content: center">
    <div class="row" style="align-content: space-around; align-items: center">
      <div class="col-lg-12" style="">
       <div class="row" style="">
        <div class="col-lg-12 text-center">
          <h2 style="margin-bottom: 0px"><strong>Faculty Membership</strong></h2>
        </div>
      </div>
      <div style="text-align: center">
        <div class="featured-boxes featured-boxes-style-3 featured-boxes-flat text-center" style="text-align: center">
          <div class="owl-carousel owl-theme" data-plugin-options="{'items': 5, 'autoplay': true, 'autoplayTimeout': 3000}" style="text-align: center">
            <?php if (!empty($keanggotaan)) {
             foreach ($keanggotaan as $row) { ?>
              <div>
                <a href="<?= $row->logoLink ?>" target="_blank">
                  <div class="featured-box featured-box-primary featured-box-effect-3" style=" margin-right: 10px; text-align: center">
                   <div class="box-content">										
                    <img src="<?php echo base_url('f_home/loadimage/') .$row->logoFiles?>" class="img-fluid" style="width: 200px; height: 100px;">
                    <h5><?=$row->logoNamaEng?></h5>
                  </div>
                </div>
              </a>
            </div>
          <?php } } ?>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>

<hr>
<section class="section section-text-dark section-background section-overlay-opacity section-overlay-opacity-light section-overlay-opacity-light-scale-3" style="background-image: url(<?=base_url('f_home/loadimage/Honeycomb-Pattern-PNG-Clipart.png')?>); min-height: 260px;">
  <div class="container" style="align-content: center">
    <div class="row" style="">
      <div class="col-lg-12" style="">
       <div class="row" style="">
        <div class="col-lg-12 text-center">
          <h2 style="margin-bottom: 0px"><strong>Subscription Journal</strong></h2>
          <br>
          <br>
        </div>
      </div>
      <div class="featured-boxes featured-boxes-style-3 featured-boxes-flat text-center" style="">
        <div style="text-align: center">
          <?php if (!empty($langganan)) {
           foreach ($langganan as $row) { ?>
             <div class="">
              <a href="<?= $row->logoLink ?>" target="_blank" style=" margin-right: 10px; text-align: center">
              <div class="">
               <div class="box-content">										
                <img src="<?php echo base_url('f_home/loadimage/') .$row->logoFiles?>" class="img-fluid" style="width: 200px; height: 100px;">
                <h5><?=$row->logoNamaEng?></h5>
              </div>
           </div>
            </a>
          </div>
        <?php } } ?>
      </div>
    </div>
  </div>
</div>
</div>
</section>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div>
                    <h4>LINK</h4>
                    <ul>
                    <?php if (!empty($link)) {
                    foreach ($link as $row) : ?>
                    
                     <li>
                            <a href="<?= $row->linkLink ?>" target="_blank" title="<?= $row->linkNamaEng ?>"><?= $row->linkNamaEng ?></a>
                    </li>
                    
                          
                    <?php endforeach;
                } ?>
                    </ul>

                </div>
            </div>
            <div class="col-lg-3">
                <h4>Maps</h4>
                <div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15958.74157570011!2d117.1543998!3d-0.4677006!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xaee3647b25ea9c3a!2sFakultas%20Ekonomi%20%26%20Bisnis%20(FEB)%20-%20Unmul!5e0!3m2!1sid!2sid!4v1639375517371!5m2!1sid!2sid" width="250" height="150" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="contact-details">
                    <h4>Contact</h4>
                    <ul class="contact">
                        <li>
                            <p><i class="fa fa-map-marker"></i> <strong>Address:</strong> <?= $footer->footAlamat ?></p>
                        </li>
                        <li>
                            <p><i class="fa fa-phone"></i> <strong>Phone:</strong> <?= $footer->footTlp ?></p>
                        </li>
                        <li>
                            <p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:<?= $footer->footEmail ?>"><?= $footer->footEmail ?></a></p>
                        </li>
                    </ul>
                </div>
                <h4>Social Media</h4>
                <ul class="social-icons">
                    <li class="social-icons-facebook"><a href="https://www.facebook.com/FEBUNMUL1966" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li class="social-icons-youtube"><a href="https://www.youtube.com/channel/UCdTPhXVkxjeqI3FVNvrwrEQ" target="_blank" title="YouTube"><i class="fa fa-youtube"></i></a></li>
                    <li class="social-icons-instagram"><a href="https://www.instagram.com/febunmul/" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                     <li class="social-icons-linkedin"><a href="<?php echo base_url(); ?>login" target="_blank" title="Login Admin"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-3">
      <div class="text-center">
        <h4>Visitor Statistic</h4>
        <div class="row counters">
            <div class="col-sm-6 col-lg-6 ">
                <div class="counter counter-light">
                    <i class="fa fa-users"></i>
                    <strong data-to="<?= $stat->statPengunjungHari ?>">0</strong>
                    <label>Today</label>
                </div>
            </div>
            <div class="col-sm-6 col-lg-6 ">
                <div class="counter counter-light">
                    <i class="fa fa-calendar"></i>
                    <strong data-to="<?= $stat->statPengunjungBulan ?>">0</strong>
                    <label>This Month</label>
                </div>
            </div>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <p>© Copyright 2021 UPT.TIK Universitas Mulawarman.</p>
                </div>
            </div>
        </div>
    </div>
</footer>