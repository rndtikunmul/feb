<hr>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div>
                    <h4>LINK</h4>
                    <ul>
                    <?php if (!empty($link)) {
                    foreach ($link as $row) : ?>
                    
                     <li>
                            <a href="<?= $row->linkLink ?>" target="_blank" title="<?= $row->linkNama ?>"><?= $row->linkNama ?></a>
                    </li>
                    
                          
                    <?php endforeach;
                } ?>
                    </ul>

                </div>
            </div>
            <div class="col-lg-4">
                <h4>Maps</h4>
                <div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15958.74157570011!2d117.1543998!3d-0.4677006!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xaee3647b25ea9c3a!2sFakultas%20Ekonomi%20%26%20Bisnis%20(FEB)%20-%20Unmul!5e0!3m2!1sid!2sid!4v1639375517371!5m2!1sid!2sid" width="250" height="150" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="contact-details">
                    <h4>Contact</h4>
                    <ul class="contact">
                        <li>
                            <p><i class="fa fa-map-marker"></i> <strong>Address:</strong> <?= $footer->footAlamat ?></p>
                        </li>
                        <li>
                            <p><i class="fa fa-phone"></i> <strong>Phone:</strong> <?= $footer->footTlp ?></p>
                        </li>
                        <li>
                            <p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:<?= $footer->footEmail ?>"><?= $footer->footEmail ?></a></p>
                        </li>
                    </ul>
                </div>
                <h4>Social Media</h4>
                <ul class="social-icons">
                    <li class="social-icons-facebook"><a href="https://www.facebook.com/FEBUNMUL1966" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li class="social-icons-youtube"><a href="https://www.youtube.com/channel/UCdTPhXVkxjeqI3FVNvrwrEQ" target="_blank" title="YouTube"><i class="fa fa-youtube"></i></a></li>
                    <li class="social-icons-instagram"><a href="https://www.instagram.com/febunmul/" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                     <li class="social-icons-linkedin"><a href="<?php echo base_url(); ?>login" target="_blank" title="Login Admin"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
         </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <p>© Copyright 2021 UPT.TIK Universitas Mulawarman.</p>
                </div>
            </div>
        </div>
    </div>
</footer>