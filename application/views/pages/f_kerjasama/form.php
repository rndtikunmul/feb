
<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_upload" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="kerjasamaIdOld" value="<?=$datas!=false?$datas->kerjasamaId:''?>">

                        <div class="form-group">
                            <label>Nama Kerjaama</label>
                            <input type="text" class="form-control" name="kerjasamaNama" placeholder="kerjasamaNama" aria-describedby="kerjasamaNama" value="<?=$datas!=false?$datas->kerjasamaNama:''?>">
                        </div>

                        <div class="form-group">
                            <label>kerjasama Dengan Lembaga</label>
                            <input type="text" class="form-control" name="kerjasamaLembaga" placeholder="kerjasamaLembaga" aria-describedby="kerjasamaLembaga" value="<?=$datas!=false?$datas->kerjasamaLembaga:''?>">
                        </div>

                          <div class="form-group">
                            <label>Jenis</label>
                            <select class="form-control m-select2" name="kerjasamaJenis">
                                <option value=""></option>
                                <option value="Dalam Negeri"<?=$datas==FALSE?'':($datas->kerjasamaJenis=='Dalam Negeri'?'selected':'')?>>Dalam Negeri</option>
                                <option value="Luar Negeri"<?=$datas==FALSE?'':($datas->kerjasamaJenis=='Luar Negeri'?'selected':'')?>>Luar Negeri</option>
                               
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Tahun </label>
                            <input type="text" class="form-control" name="kerjasamaTahun" placeholder="kerjasamaTahun" aria-describedby="kerjasamaTahun" value="<?=$datas!=false?$datas->kerjasamaTahun:''?>">
                        </div>

                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" name="kerjasamaKet" placeholder="kerjasamaKet" aria-describedby="kerjasamaKet" value="<?=$datas!=false?$datas->kerjasamaKet:''?>">
                        </div>

                          <div class="form-group">
                            <label>File Kerjasama</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="berkas" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih File
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *.pdf | Maks. Size: 3MB 
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->
