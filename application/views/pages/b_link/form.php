<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="linkIdOld" value="<?= $datas != false ? $datas->linkId : '' ?>">

                        <div class="form-group">
                            <label>Nama Link</label>
                            <input type="text" class="form-control" name="linkNama" placeholder="linkNama" aria-describedby="linkNama" value="<?= $datas != false ? $datas->linkNama : '' ?>">
                        </div>
                        <div class="form-group">
                            <label>Nama English Link</label>
                            <input type="text" class="form-control" name="linkNamaEng" placeholder="linkNamaEng" aria-describedby="linkNamaEng" value="<?= $datas != false ? $datas->linkNamaEng : '' ?>">
                        </div>
                        <div class="form-group">
                            <label>Link</label>
                            <input type="text" class="form-control" name="linkLink" placeholder="linkLink" aria-describedby="linkLink" value="<?= $datas != false ? $datas->linkLink : '' ?>">
                        </div>
                        <div class="form-group">
                            <label>Icon</label>
                            <input type="text" class="form-control" name="linkIcon" placeholder="linkIcon" aria-describedby="linkIcon" value="<?= $datas != false ? $datas->linkIcon: '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->