
<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_upload" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="karyawanIdOld" value="<?=$datas!=false?$datas->karyawanId:''?>">

                        <div class="form-group">
                            <label>Unit</label>
                            <select class="form-control m-select2" name="karyawanProdiId">
                                <option value=""></option>
                                 <?php 
                                foreach($f_prodi as $row):
                                    echo '<option value="'.$row->prodiId.'" ' . ($datas != false ? $row->prodiId == $datas->karyawanProdiId ? 'selected' : '' : '') . '>'.$row->prodiNama.'</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Nama</label>
                           <select class="form-control m-select2" name="karyawanNama">
                                <option value=""></option>
                                 <?php 
                                foreach($datakaryawan->datas as $row):
                                    echo '<option value="'.$row->pegmNIP.'-'.$row->pegmNama.'" '.($datas != false ? $row->pegmNIP == $datas->karyawanNIP ? 'selected' : '' : '') . '>'.$row->pegmNama.'-'.$row->pegmNIP.'</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Jenis Pegawai</label>
                            <select class="form-control m-select2" name="karyawanJenis">
                                <option value=""></option>
                                <option value="Dosen"<?=$datas==FALSE?'':($datas->karyawanJenis=='Dosen'?'selected':'')?>>Dosen</option>
                                <option value="dosenonpns"<?=$datas==FALSE?'':($datas->karyawanJenis=='dosenonpns'?'selected':'')?>>Dosen Non PNS</option>
                                <option value="tendikpns"<?=$datas==FALSE?'':($datas->karyawanJenis=='tendikpns'?'selected':'')?>>Tendik PNS</option>
                                <option value="tendiknonpns"<?=$datas==FALSE?'':($datas->karyawanJenis=='tendiknonpns'?'selected':'')?>>Tendik NON PNS</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Jabatan</label>
                            <input type="text" class="form-control" name="karyawanJabatan" placeholder="Jabatan" aria-describedby="karyawanJabatan" value="<?=$datas!=false?$datas->karyawanJabatan:''?>">

                        </div>

                           <div class="form-group">
                            <label>Detail</label>
                            <textarea cols="60" class="form-control" rows="10" id="ckeditor" name="karyawanDetail" placeholder="">
                             <?=$datas!==false?$datas->karyawanDetail:''?></textarea>   
                        </div>

                        <div class="form-group">
                            <label>Foto</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="berkas" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih Gambar
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *.jpg | Maks. Size: 3MB 
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->
