<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_upload" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="jurnalIdOld" value="<?= $datas != false ? $datas->jurnalId : '' ?>">

                        <div class="form-group">
                            <label>Jenis Jurnal</label>
                            <select class="form-control m-select2" name="jurnalnamaId">
                                <option value=""></option>
                                <?php
                                foreach ($f_jurnal_nama as $row) :
                                    echo '<option value="' . $row->jurnamId . '" ' . ($datas != false ? $row->jurnamId == $datas->jurnalnamaId ? 'selected' : '' : '') . '>' . $row->jurnamNama . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Judul</label>
                            <input type="text" class="form-control" name="jurnalJudul" placeholder="isi judul jurnal" aria-describedby="jurnalJudul" value="<?= $datas != false ? $datas->jurnalJudul : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Penulis</label>
                            <input type="text" class="form-control" name="jurnalPenulis" placeholder="isi penulis" aria-describedby="jurnalPenulis" value="<?= $datas != false ? $datas->jurnalPenulis : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Abstrak</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="jurnalAbst" placeholder="jurnalAbst" aria-describedby="jurnalAbst"> <?= $datas != false ? $datas->jurnalAbst : '' ?>
                            </textarea>
                        </div>

                        <div class="form-group">
                            <label>Tanggal terbit</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" placeholder="Pilih tanggal" id="kt_datepicker_1" name="jurnalTgl" value="<?= $datas != false ? $datas->jurnalTgl : '' ?>" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Upload File Abstrak</label>
                            <div class="custom-file">
                                <input type="file" id="custom-file" class="custom-file-input" name="jurnalFile" placeholder="File" aria-describedby="File">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->