<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="statIdOld" value="<?= $datas != false ? $datas->statId : '' ?>">

                        <div class="form-group">
                            <label>Kerjasama</label>
                            <input type="text" class="form-control" name="statKerjasama" placeholder="Jumlah kerjasama" aria-describedby="statKerjasama" value="<?= $datas != false ? $datas->statKerjasama : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Prodi</label>
                            <input type="text" class="form-control" name="statProdi" placeholder="Jumlah program studi" aria-describedby="statProdi" value="<?= $datas != false ? $datas->statProdi : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Mhs</label>
                            <input type="text" class="form-control" name="statMhs" placeholder="Jumlah mahasiswa" aria-describedby="statMhs" value="<?= $datas != false ? $datas->statMhs : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Dosen</label>
                            <input type="text" class="form-control" name="statDosen" placeholder="Jumlah dosen" aria-describedby="statDosen" value="<?= $datas != false ? $datas->statDosen : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Pengunjung Per Hari</label>
                            <input type="text" class="form-control" name="statPengunjungHari" placeholder="Jumlah Pengunjung Per Hari" aria-describedby="statPengunjungHari" value="<?= $datas != false ? $datas->statPengunjungHari : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Pengunjung Per Bulan</label>
                            <input type="text" class="form-control" name="statPengunjungBulan" placeholder="Jumlah Pengunjung Per Bulan" aria-describedby="statPengunjungBulan" value="<?= $datas != false ? $datas->statPengunjungBulan : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->