<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="infodaftarIdOld" value="<?= $datas != false ? $datas->infodaftarId : '' ?>">

                        <div class="form-group">
                            <label>Jenjang</label>
                            <select class="form-control m-select2" name="infodaftarJenjangId">
                                <option value=""></option>
                                <?php
                                foreach ($f_jenjang as $row) :
                                    echo '<option value="' . $row->jenjangId . '" ' . ($datas != false ? $row->jenjangId == $datas->infodaftarJenjangId ? 'selected' : '' : '') . '>' . $row->jenjangNama . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Judul Informasi</label>
                            <input type="text" class="form-control" name="infodaftarNama" placeholder="Judul Informasi" aria-describedby="infodaftarNama" value="<?= $datas != false ? $datas->infodaftarNama : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Isi Informasi</label>
                            <textarea cols="60"  rows="10" id="ckeditor" name="infodaftarContent" placeholder="" aria-describedby="infodaftarContent"> <?= $datas != false ? $datas->infodaftarContent : '' ?>
                        </textarea>
                        </div>
                        <div class="form-group">
                            <label>Isi Informasi English</label>
                            <textarea cols="60"  rows="10" id="ckeditor2" name="infodaftarContentEng" placeholder="" aria-describedby="infodaftarContent"> <?= $datas != false ? $datas->infodaftarContentEng : '' ?>
                        </textarea>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->