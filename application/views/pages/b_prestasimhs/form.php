<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_upload" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="prestasiIdOld" value="<?=$datas!=false?$datas->prestasiId:''?>">

                        <div class="form-group">
                            <label>Tingkat Kejuaraan</label>
                            <select class="form-control m-select2" name="prestasiTingkat">
                                <option value=""></option>
                                <option value="Internasional"<?=$datas==FALSE?'':($datas->prestasiTingkat=='Internasional'?'selected':'')?>>Internasional</option>
                                <option value="Nasional"<?=$datas==FALSE?'':($datas->prestasiTingkat=='Nasional'?'selected':'')?>>Nasional</option>
                                <option value="Provinsi"<?=$datas==FALSE?'':($datas->prestasiTingkat=='Provinsi'?'selected':'')?>>Propinsi</option>
                                <option value="Kota"<?=$datas==FALSE?'':($datas->prestasiTingkat=='Kota'?'selected':'')?>>Kota</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Nama Mahasiswa</label>
                            <input type="text" class="form-control" name="prestasiNamaMhs" placeholder="Nama Mahasiswa" aria-describedby="prestasiNamaMhs" value="<?=$datas!=false?$datas->prestasiNamaMhs:''?>">
                        </div>

                        <div class="form-group">
                            <label>NIM </label>
                            <input type="text" class="form-control" name="prestasiNIM" placeholder="NOMOR INDUK MAHASISWA" aria-describedby="prestasiNIM" value="<?=$datas!=false?$datas->prestasiNIM:''?>">
                        </div>                    

                        <div class="form-group">
                            <label>Program Studi</label>
                            <select class="form-control m-select2" name="prestasiProdiId">
                                <option value=""></option>
                                <?php 
                                foreach($f_prodi as $row):
                                    echo '<option value="'.$row->prodiId.'" ' . ($datas != false ? $row->prodiId == $datas->prestasiProdiId ? 'selected' : '' : '') . '>'.$row->prodiNama.'</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Nama Kegiatan</label>
                            <input type="text" class="form-control" name="prestasiNamaKegiatan" placeholder="Nama Kegiatan" aria-describedby="prestasiNamaKegiatan" value="<?=$datas!=false?$datas->prestasiNamaKegiatan:''?>">
                        </div>  

                        <div class="form-group">
                            <label>Juara</label>
                            <input type="text" class="form-control" name="prestasiJuara" placeholder="Juara" aria-describedby="prestasiJuara" value="<?=$datas!=false?$datas->prestasiJuara:''?>">
                        </div>

                        <div class="form-group">
                        <label>Tanggal Kegiatan</label>
                        <div class="input-group date">
                            <input type="text" class="form-control" placeholder="Pilih tanggal" id="kt_datepicker_1" name="prestasiTglKegiatan" value="<?= $datas != false ? $datas->prestasiTglKegiatan : '' ?>" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar-check-o"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                        <div class="form-group">
                            <label>Lampiran</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="berkas" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih Gambar
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *jpg,jpeg,pdf | Maks. Size: 3MB 
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->
