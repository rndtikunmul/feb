<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="prodiIdOld" value="<?= $datas != false ? $datas->prodiId : '' ?>">

                        <div class="form-group">
                            <label>Jurusan</label>
                            <select class="form-control m-select2" name="prodijurusanId">
                                <option value=""></option>
                                <?php
                                foreach ($f_jurusan as $row) :
                                    echo '<option value="' . $row->jurusanId . '" ' . ($datas != false ? $row->jurusanId == $datas->prodijurusanId ? 'selected' : '' : '') . '>' . $row->jurusanNama . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Program Studi</label>
                            <input type="text" class="form-control" name="prodiNama" placeholder="nama program studi" aria-describedby="prodiNama" value="<?= $datas != false ? $datas->prodiNama : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea cols="60"  rows="10" id="ckeditor" name="prodiDesk" placeholder="prodiDesk" aria-describedby="prodiDesk"> <?= $datas != false ? $datas->prodiDesk : '' ?>
                        </textarea>
                        </div>

                      

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->