<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_upload" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="logoIdOld" value="<?=$datas!=false?$datas->logoId:''?>">

                        <div class="form-group">
                            <label>Section</label>
                            <select class="form-control m-select2" name="logoSection">
                                <option value=""></option>
                                <option value="Jurusan"<?=$datas==FALSE?'':($datas->logoSection=='Jurusan'?'selected':'')?>>Jurusan</option>
                                <option value="Keanggotaan"<?=$datas==FALSE?'':($datas->logoSection=='Keanggotaan'?'selected':'')?>>Keanggotaan</option>
                                <option value="Jurnal Langganan"<?=$datas==FALSE?'':($datas->logoSection=='Jurnal Langganan'?'selected':'')?>>Jurnal Langganan</option>
                                <option value="Foto FB"<?=$datas==FALSE?'':($datas->logoSection=='Foto FB'?'selected':'')?>>Foto FB</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Judul</label>
                            <input type="text" class="form-control" name="logoNama" placeholder="Judul" aria-describedby="logonama" value="<?=$datas!=false?$datas->logoNama:''?>">
                        </div>

                        <div class="form-group">
                            <label>Judul English</label>
                            <input type="text" class="form-control" name="logoNamaEng" placeholder="Judul English" aria-describedby="logonamaeng" value="<?=$datas!=false?$datas->logoNamaEng:''?>">
                        </div>

                        <div class="form-group">
                            <label>Link</label>
                            <input type="text" class="form-control" name="logoLink" placeholder="Link" aria-describedby="logoLink" value="<?=$datas!=false?$datas->logoLink:''?>">
                        </div> 

                        <div class="form-group">
                            <label>Gambar</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="berkas" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih Gambar
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *jpg,jpeg | Maks. Size: 3MB 
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->
