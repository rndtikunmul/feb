
<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_upload" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="sliderIdOld" value="<?=$datas!=false?$datas->sliderId:''?>">
                        <div class="form-group">
                            <label>Prodi/Jurusan</label>
                            <select class="form-control m-select2" name="sliderUnit">
                                <?php if(($user == "ADMIN_FEB") or ($user == "ADMIN") )
                                { ?> <option value="1">FAKULTAS</option> <?php } ?>
                                <?php 
                                foreach($f_prodi as $row):
                                    echo '<option value="'.$row->prodiId.'" ' . ($datas != false ? $row->prodiId == $datas->sliderUnit ? 'selected' : '' : '') . '>'.$row->prodiNama.'</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label>Nama Slider</label>
                            <input type="text" class="form-control" name="sliderNama" placeholder="isi nama slider" aria-describedby="sliderNama" value="<?= $datas != false ? $datas->sliderFiles : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Upload Foto</label>
                            <div class="custom-file">
                                <input type="file" id="custom-file" class="custom-file-input" name="sliderFiles" placeholder="Files" aria-describedby="Files" value="<?= $datas != false ? $datas->sliderFiles : '' ?>">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>

                                    <!-- <div class="form-group">
                                        <label>Nomor Urut</label>
                                        <input type="text" class="form-control" name="sliderNoUrut" placeholder="isi nomor urut slider" aria-describedby="sliderNoUrut" value="<?= $datas != false ? $datas->sliderNoUrut : '' ?>">
                                    </div> -->

                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                                        <button type="reset" class="btn btn-secondary">Cancel</button>
                                    </div>
                                </div>
                            </form>

                            <!--end::Form-->
                        </div>

                        <!--end::Portlet-->
                    </div>
                </div>
            </div>
            <!--End::Row-->
            