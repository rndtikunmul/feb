
<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <a href="<?=$create_url?>" class="btn btn-outline-primary">
                                <span>
                                    <i class="flaticon2-plus"></i>
                                    <span>Create</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Ket Publish</th>
                                                        <th>Unit</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if($datas!=false)
                                                {
                                                    $i = 1;
                                                    foreach($datas as $row)
                                                    {
                                                        $key = $this->encryptions->encode($row->sliderId,$this->config->item('encryption_key'));
                                                ?>
                                                    <tr>
                                                        <th scope="row"><?=$i++?></th>
                                                        <td><?= $row->sliderFiles?></td>
                                                        <td>
                                                        <?=$row->sliderIsDisplay == 0 ? 'Belum aproval' : ($row->sliderIsDisplay == 1 ? 'Publish' : 'Tidak Publish') ?>     
                                                            
                                                        </td>
                                                        <td><?= $row->prodiNama?></td>

                                                        <td>
                                                            <a href="<?=base_url('b_slider/loadimage/').$row->sliderFiles?>" target="_blank" title="Lihat Foto" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="far fa-eye"></i>
                                                                </span>
                                                            </a>
                                                            <!-- <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </span>
                                                            </a> -->
                                                            <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fa-trash-alt"></i>
                                                                </span>
                                                            </a>
                                                            <a href="<?=$Ya.$key ?>" title="Publish Ya" id='ts_Ya_row<?= $i;?>' class="ts_Ya_row btn btn-sm btn-outline-warning btn-elevate btn-circle btn-icon"> <span> <i class="fa fa-thumbs-up"></i></span></a>

                                                             <a href="<?=$Tidak.$key ?>" title="Publish TIDAK" id='ts_Tidak_row<?= $i;?>' class="ts_Tidak_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                                <span><i class="fas fa-thumbs-down"></i></span>
                                                            </a>

                                                             <!--  <a href="<?= $aprovalkaprodiseminar1ya . $key ?>" title="Persetujuan Seminar (Kaprodi) YA" id='ts_seminar1ya_row<?= $i ?>' class="ts_seminar1ya_row btn btn-sm btn-outline-warning btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fa-check"></i>
                                                                </span>
                                                            </a> -->
                                                        </td>
                                                    </tr>
                                                <?php
                                                    }
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->
