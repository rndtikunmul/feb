<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="pageIdOld" value="<?= $datas != false ? $datas->pageId : '' ?>">

                        <div class="form-group">
                            <label>Header Grup</label>
                            <select class="form-control m-select2" name="pageHead">
                                <option value=""></option>
                                <?php
                                foreach ($s_user_modul_group_ref as $row) :
                                    echo '<option value="' . $row->susrmdgroupNama . '" ' . ($datas != false ? $row->susrmdgroupNama == $datas->pageHead ? 'selected' : '' : '') . '>' . $row->susrmdgroupDisplay . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                         <div class="form-group">
                            <label>Header Eng</label>
                            <select class="form-control m-select2" name="pageHeadEng">
                                <option value=""></option>
                                <?php
                                foreach ($s_user_modul_group_ref as $row) :
                                    echo '<option value="' . $row->susrmdgroupDisplayEng . '" ' . ($datas != false ? $row->susrmdgroupDisplayEng == $datas->pageHeadEng ? 'selected' : '' : '') . '>' . $row->susrmdgroupDisplayEng . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Judul</label>
                            <input type="text" class="form-control" name="pageJudul" placeholder="isi judul halaman" aria-describedby="pageJudul" value="<?= $datas != false ? $datas->pageJudul : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Judul Eng</label>
                            <input type="text" class="form-control" name="pageNamaEng" placeholder="isi judul halaman English" aria-describedby="pageNamaEng" value="<?= $datas != false ? $datas->pageNamaEng : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Content</label>
                            <textarea cols="60" rows="10" id="ckeditor" name="pageContent" placeholder="pageContent" aria-describedby="pageContent"><?= $datas != false ? $datas->pageContent : '' ?>
                            </textarea>
                        </div>
                         <div class="form-group">
                            <label>Sidebar</label>
                            <textarea cols="60" rows="10" id="ckeditor2" name="pageSidebar" placeholder="pageContent" aria-describedby="pageSidebar"><?= $datas != false ? $datas->pageSidebar : '' ?>
                            </textarea>
                        </div>

                          <div class="form-group">
                            <label>Content English</label>
                            <textarea cols="60" rows="10" id="ckeditor3" name="pageContenteng" placeholder="pageContent" aria-describedby="pageContenteng"><?= $datas != false ? $datas->pageContenteng : '' ?>
                            </textarea>
                        </div>
                         <div class="form-group">
                            <label>Sidebar English</label>
                            <textarea cols="60" rows="10" id="ckeditor4" name="pageSidebareng" placeholder="pageContent" aria-describedby="pageSidebareng"><?= $datas != false ? $datas->pageSidebareng : '' ?>
                            </textarea>
                        </div>

                        <div class="form-group">
                            <label>pageTag</label>
                            <input type="text" class="form-control" name="pageTag" placeholder="isi tag halaman" aria-describedby="pageTag" value="<?= $datas != false ? $datas->pageTag : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Urutan Menu</label>
                            <input type="text" class="form-control" name="pageUrut" placeholder="isi urutan menu halaman" aria-describedby="pageUrut" value="<?= $datas != false ? $datas->pageUrut : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Link</label>
                             <select class="form-control m-select2" name="pageLink">
                                <option value=""></option>
                                <option value="Link"<?=$datas==FALSE?'':($datas->pageLink=='Link'?'selected':'')?>>Link</option>
                                <option value="NoLink"<?=$datas==FALSE?'':($datas->pageLink=='NoLink'?'selected':'')?>>Bukan Link</option>
                               
                            </select>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->