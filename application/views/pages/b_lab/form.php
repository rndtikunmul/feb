<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="labIdOld" value="<?= $datas != false ? $datas->labId : '' ?>">

                        <div class="form-group">
                            <label>Program Studi</label>
                            <select class="form-control m-select2" name="labprodiId">
                                <option value=""></option>
                                <?php
                                foreach ($f_prodi as $row) :
                                    echo '<option value="' . $row->prodiId . '" ' . ($datas != false ? $row->prodiId == $datas->labprodiId ? 'selected' : '' : '') . '>' . $row->prodiNama . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Nama Laboratorium</label>
                            <input type="text" class="form-control" name="labNama" placeholder="isi nama laboratorium" aria-describedby="labNama" value="<?= $datas != false ? $datas->labNama : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="labDesk" placeholder="Deskripsi Laboratorium" aria-describedby="labDesk"> <?= $datas != false ? $datas->labDesk : '' ?>
                            </textarea>
                        </div>

                        <div class="form-group">
                            <label>Visi</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="labVisi" placeholder="labVisi" aria-describedby="labVisi"> <?= $datas != false ? $datas->labVisi : '' ?>
                            </textarea>
                        </div>

                        <div class="form-group">
                            <label>Misi</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="labMisi" placeholder="labMisi" aria-describedby="labMisi"> <?= $datas != false ? $datas->labMisi : '' ?>
                            </textarea>
                        </div>

                        <div class="form-group">
                            <label>Kepala Laboratorium</label>
                            <input type="text" class="form-control" name="labKalab" placeholder="isi Kalab" aria-describedby="labKalab" value="<?= $datas != false ? $datas->labKalab : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->