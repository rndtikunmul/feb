<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="footIdOld" value="<?= $datas != false ? $datas->footId : '' ?>">

                        <div class="form-group">
                            <label>Map</label>
                            <input type="text" class="form-control" name="footMap" placeholder="isi embed map google" aria-describedby="footMap" value="<?= $datas != false ? $datas->footMap : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" class="form-control" name="footAlamat" placeholder="isi alamat" aria-describedby="footAlamat" value="<?= $datas != false ? $datas->footAlamat : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Tlp</label>
                            <input type="text" class="form-control" name="footTlp" placeholder="isi nomor telepon" aria-describedby="footTlp" value="<?= $datas != false ? $datas->footTlp : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" name="footEmail" placeholder="isi alamat email" aria-describedby="footEmail" value="<?= $datas != false ? $datas->footEmail : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->