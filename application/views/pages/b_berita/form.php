<!--  -->
  
<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_upload" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="beritaIdOld" value="<?=$datas!=false?$datas->beritaId:''?>">
                        
                        <div class="form-group">
                            <label>Section</label>
                            <select class="form-control m-select2" name="beritaSection">
                                <option value=""></option>
                                <option value="pengumuman" <?=$datas!=false?$datas->beritaSection=='pengumuman'?'selected="selected"':'':''?>>Pengumuman</option>;
                                <option value="berita" <?=$datas!=false?$datas->beritaSection=='berita'?'selected="selected"':'':''?>>Berita</option>;
                                <option value="kegiatan" <?=$datas!=false?$datas->beritaSection=='kegiatan'?'selected="selected"':'':''?>>Kegiatan</option>;
                                <option value="agenda" <?=$datas!=false?$datas->beritaSection=='agenda'?'selected="selected"':'':''?>>Agenda</option>;
                                <option value="mahasiswa" <?=$datas!=false?$datas->beritaSection=='mahasiswa'?'selected="selected"':'':''?>>Mahasiswa</option>;
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Prodi/Jurusan</label>
                            <select class="form-control m-select2" name="beritaUnit">
                                <?php if(($user == "ADMIN_FEB") or ($user == "ADMIN") )
                                { ?> <option value="1">FAKULTAS</option> <?php } ?>
                                 <?php if(($user == "MAHASISWA") or ($user == "ADMIN_FEB") or ($user == "ADMIN")  )
                                { ?> <option value="15">MAHASISWA</option> <?php } ?>
                                <?php 
                                foreach($f_prodi as $row):
                                    echo '<option value="'.$row->prodiId.'" ' . ($datas != false ? $row->prodiId == $datas->beritaUnit ? 'selected' : '' : '') . '>'.$row->prodiNama.'</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label>Judul</label>
                            <input type="text" class="form-control" name="beritaJudul" placeholder="Judul" aria-describedby="beritaJudul" value="<?=$datas!=false?$datas->beritaJudul:''?>">
                        </div>
                        <div class="form-group">
                            <label>Judul Eng</label>
                            <input type="text" class="form-control" name="BeritaJudulEng" placeholder="Judul" aria-describedby="BeritaJudulEng" value="<?=$datas!=false?$datas->BeritaJudulEng:''?>">
                        </div>
                        
                   <!--  <div class="form-group">
                        <label>Content</label>
                        <textarea cols="60" class="summernote" rows="10" id="kt_summernote_1" name="beritaContent" placeholder="Isi Berita" aria-describedby="beritaContent">
                            <?=$datas!=false?$datas->beritaContent:''?>

                        </textarea>
                    </div>
                -->
                <div class="form-group">
                    <label>Content</label><br>
                    <textarea class="form-control" rows="10" id="ckeditor" name="beritaContent"><?= $datas != false ? $datas->beritaContent:''?></textarea>  
                </div>
                 <div class="form-group">
                    <label>Content English </label><br>
                    <textarea class="form-control" rows="10" id="ckeditor2" name="BeritaContentEng"><?= $datas != false ? $datas->BeritaContentEng:''?></textarea>  
                </div>
                 
                    
                    <div class="form-group">
                        <label>Tanggal</label>
                        <div class="input-group date">
                            <input type="text" class="form-control" placeholder="Pilih tanggal" id="kt_datepicker_1" name="beritaDatetime" value="<?= $datas != false ? $datas->beritaDatetime : '' ?>" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar-check-o"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label>Tag</label>
                        <input type="text" class="form-control" name="beritaTag" placeholder="beritaTag" aria-describedby="beritaTag" value="<?=$datas!=false?$datas->beritaTag:''?>">
                    </div>

                    <div class="form-group">
                        <label>
                            Berita Gambar
                        </label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="berkas" id="custom-file-input">
                            <label class="custom-file-label" for="customFile">
                                Pilih Gambar
                            </label>
                            <span class="m-form__help">
                                Filetipe: *jpg|jpeg|png **Maks. Size: 15MB 
                            </span>
                        </div>
                    </div>
                    <?php if ($user != in_array($user, array("MAHASISWA","SEKRETARIAT"))) { ?>
                      <div class="form-group">
                        <label>Publish</label>
                        
                        <div class="kt-radio-inline">
                            <label class="kt-radio">
                                <input type="radio" name="beritaIsDisplay" value="2" <?= $datas != false ? $datas->beritaIsDisplay == 2 ? 'checked="checked"' : '' : '' ?>> TIDAK
                                <span></span>
                            </label>
                            <label class="kt-radio">
                                <input type="radio" name="beritaIsDisplay" value="1" <?= $datas != false ? $datas->beritaIsDisplay == 1 ? 'checked="checked"' : '' : '' ?>> YA
                                <span></span>
                            </label>
                        </div>
                    </div>
                <?php } ?>

                <?php if ($user == in_array($user, array("ADMIN","ADMIN_FEB"))) { ?>

                    <div class="form-group">
                        <label>Berita Pilihan</label>
                        
                        <div class="kt-radio-inline">
                            <label class="kt-radio">
                                <input type="radio" name="beritaIsPin" value="0" <?= $datas != false ? $datas->beritaIsPin == 0 ? 'checked="checked"' : '' : '' ?>> TIDAK
                                <span></span>
                            </label>
                            <label class="kt-radio">
                                <input type="radio" name="beritaIsPin" value="1" <?= $datas != false ? $datas->beritaIsPin == 1 ? 'checked="checked"' : '' : '' ?>> YA
                                <span></span>
                            </label>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label>Approv Berita Prodi</label>
                        
                        <div class="kt-radio-inline">
                            <label class="kt-radio">
                                <input type="radio" name="beritaprodiaprov" value="0" <?= $datas != false ? $datas->beritaprodiaprov == 0 ? 'checked="checked"' : '' : '' ?>> TIDAK
                                <span></span>
                            </label>
                            <label class="kt-radio">
                                <input type="radio" name="beritaprodiaprov" value="1" <?= $datas != false ? $datas->beritaprodiaprov == 1 ? 'checked="checked"' : '' : '' ?>> YA
                                <span></span>
                            </label>
                        </div>
                    </div>
                <?php } ?>
                
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </form>

        <!--end::Form-->
    </div>

    <!--end::Portlet-->
</div>
</div>
</div>
<!--End::Row-->
