<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$show_url?>" method="post" id="form_show">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Unit</label>
                            <select class="form-control m-select2" name="beritaUnit">
                                <?php if(($user == "ADMIN_FEB") or ($user == "ADMIN") )
                                { ?> <option value="1">FAKULTAS</option> <?php } ?>
                                <?php if(($user == "MAHASISWA") or ($user == "ADMIN_FEB") or ($user == "ADMIN")  )
                                { ?> <option value="15">MAHASISWA</option> <?php } ?>
                               
                                <?php 
                                foreach($f_prodi as $row):
                                    echo '<option value="'.$row->prodiId.'" ' . ($datas != false ? $row->prodiId == $datas->beritaUnit ? 'selected' : '' : '') . '>'.$row->prodiNama.'</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-daterange input-group" id="kt_datepicker_1">
                                <input type="text" class="form-control" name="tanggalawal"  value="<?=date('Y-m').'-01'?>"/>
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                </div>
                                <input type="text" class="form-control" name="tanggalakhir"  value="<?=date('Y-m-d')?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_show" class="btn btn-primary">Show</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>

<div id="response"></div>