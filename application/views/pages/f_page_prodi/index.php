<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$show_url?>" method="post" id="form_show">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Unit</label>
                            <select class="form-control m-select2" name="beritaUnit">
                                <?php if(($user == "ADMIN_FEB") or ($user == "ADMIN") )
                                { ?> <option value="1">FAKULTAS</option> <?php } ?>
                               
                                <?php 
                                foreach($f_prodi as $row):
                                    echo '<option value="'.$row->prodiId.'" ' . ($datas != false ? $row->prodiId == $datas->beritaUnit ? 'selected' : '' : '') . '>'.$row->prodiNama.'</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_show" class="btn btn-primary">Show</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>

<div id="response"></div>