<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                  <form class="kt-form" action="<?=$save_url?>" method="post" id="form_upload" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="pageIdOld" value="<?= $datas != false ? $datas->pageId : '' ?>">

                        <div class="form-group">
                            <label>Header Grup</label>
                            <input type="text" class="form-control" name="pageJudul" placeholder="isi judul halaman" aria-describedby="pageJudul" value="<?= $datas != false ? $datas->pageJudul : '' ?>">
                           <!--  <select class="form-control m-select2" name="pageHead">
                                <option value=""></option>
                                <?php
                                foreach ($s_user_modul_group_ref as $row) :
                                    echo '<option value="' . $row->susrmdgroupNama . '" ' . ($datas != false ? $row->susrmdgroupNama == $datas->pageHead ? 'selected' : '' : '') . '>' . $row->susrmdgroupDisplay . '</option>';
                                endforeach;
                                ?>
                            </select> -->
                        </div>
                         <div class="form-group">
                            <label>Prodi</label>
                          
                            <select class="form-control m-select2" name="pageProdi">
                                  <option value=""></option>
                                <?php 
                                foreach($f_prodi as $row):
                                    echo '<option value="'.$row->prodiId.'" ' . ($datas != false ? $row->prodiId == $datas->pageProdi ? 'selected' : '' : '') . '>'.$row->prodiNama.'</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Judul</label>
                            <input type="text" class="form-control" name="pageJudul" placeholder="isi judul halaman" aria-describedby="pageJudul" value="<?= $datas != false ? $datas->pageJudul : '' ?>">
                        </div>

                          <div class="form-group">
                            <label>Judul English</label>
                            <input type="text" class="form-control" name="pageJudulEng" placeholder="isi judul halaman" aria-describedby="pageJudulEng" value="<?= $datas != false ? $datas->pageJudulEng : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Content</label>
                            <textarea cols="60" rows="10" id="ckeditor" name="pageContent" placeholder="pageContent" aria-describedby="pageContent"><?= $datas != false ? $datas->pageContent : '' ?>
                            </textarea>
                        </div>
                          <div class="form-group">
                            <label>Content English</label>
                            <textarea cols="60" rows="10" id="ckeditor2" name="pageContentEng" placeholder="pageContentEng" aria-describedby="pageContentEng"><?= $datas != false ? $datas->pageContentEng : '' ?>
                            </textarea>
                        </div>
                         <div class="form-group">
                            <label>Informasi</label>
                            <textarea cols="60" class="summernote" rows="10" id="kt_summernote_1" name="pageInfo" placeholder="pageInfo" aria-describedby="pageInfo"><?= $datas != false ? $datas->pageInfo : '' ?>
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label>Informasi English</label>
                            <textarea cols="60" class="summernote" rows="10" id="kt_summernote_1" name="pageInfoEng" placeholder="pageInfoEng" aria-describedby="pageInfoEng"><?= $datas != false ? $datas->pageInfoEng : '' ?>
                            </textarea>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->