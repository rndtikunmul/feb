
<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="dosenidOld" value="<?=$datas!=false?$datas->dosenid:''?>">
                        
                        <div class="form-group">
                            <label>Nama Dosen</label>
                            <select class="form-control m-select2" name="dosenKaryawanId">
                                <option value=""></option>
                                <?php 
                                foreach($f_karyawan as $row):
                                    echo '<option value="'.$row->karyawanId.'" ' . ($datas != false ? $row->karyawanId == $datas->dosenKaryawanId ? 'selected' : '' : '') . '>'.$row->karyawanNama.'</option>';
                                endforeach;
                                ?>
                            </select>
                            
                        </div>
                        
                        <div class="form-group">
                            <label>NIP</label>
                            <input type="text" class="form-control" name="dosenNIP" placeholder="dosenNIP" aria-describedby="dosenNIP" value="<?=$datas!=false?$datas->dosenNIP:''?>">
                        </div>
                        
                        <div class="form-group">
                            <label>Detail</label>
                            <textarea cols="60" class="form-control" rows="10" id="ckeditor" name="dosenContent" placeholder="">
                             <?=$datas!==false?$datas->dosenContent:''?></textarea>   
                        </div>
                        
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->
