<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url()?>">Home</a></li>
                        <li class="active">Prestasi Mahasiswa</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1>Prestasi Mahasiswa</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-lg-9">
            <div class="featured-boxes featured-boxes-flat">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6">
                                <div class="featured-box featured-box-primary featured-box-effect-2">
                                    <div class="box-content">
                                        <a href="<?= base_url(); ?>prestasimhs/post/internasional"><i class="icon-featured fa fa-trophy"></i></a>
                                        <h4>Internasional</h4>
                                        </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="featured-box featured-box-primary featured-box-effect-2">
                                    <div class="box-content">
                                         <a href="<?= base_url(); ?>prestasimhs/post/nasional"><i class="icon-featured fa fa-trophy"></i></a>
                                        <h4>Nasional</h4>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="featured-box featured-box-primary featured-box-effect-2">
                                    <div class="box-content">
                                        <a href="<?= base_url(); ?>prestasimhs/post/provinsi"><i class="icon-featured fa fa-trophy"></i></a>
                                        <h4>Provinsi</h4>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="featured-box featured-box-primary featured-box-effect-2">
                                    <div class="box-content">
                                        <a href="<?= base_url(); ?>prestasimhs/post/kota"><i class="icon-featured fa fa-trophy"></i></a>
                                        <h4>Kota/Kabupaten</h4>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>

<?php
$this->load->view('page/sidebar');
?>
</div>

</div>

</div>