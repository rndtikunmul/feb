<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="<?= base_url(); ?>prestasimhs">Prestasi Mahasiswa</a></li>
                        <li class="active">Prestasi mahasiswa tingkat <?=$is_active?></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1>Prestasi mahasiswa tingkat <?=$is_active?> </h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <form id="cari" action="<?=$searchkaryawan_url?>" method="get">
                        <div class="input-group input-group-4">
                            <input class="form-control" placeholder="Search..." name="search" id="keyword" type="text">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <table class="table table-hover">
                      <thead>
                       <tr>
                        <th>No</th>
                        <th>Tingkat</th>
                        <th>Mahasiswa</th>
                        <th>Kejuaraan</th>
                        <th>Tgl Kegiatan</th>
                        <th>Lampiran</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($internasional!==false) {
                      $i = 1;
                      $nomor = $offset+1;
                      foreach($internasional as $row) { ?>
                       <tr>
                        <th scope="row"><?=$nomor++?></th>
                        <td><?=$row->prestasiTingkat?></td>
                        <td><?=$row->prestasiNamaMhs?> <br> <?=$row->prestasiNIM?> <br> <?=$row->prodiNama?> </td>
                        <td><?=$row->prestasiNamaKegiatan?> <br> <?=$row->prestasiJuara?></td>
                        <td><?=datetoindo($row->prestasiTglKegiatan)?></td>
                        <td><a href="<?=base_url('Prestasimhs/loadattach/').$row->prestasiBerkas?>" target="_blank" title="Lihat" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                            <span>
                                <i class="fa fa-eye"> LIHAT</i>
                            </span>
                        </a></td>
                    </tr>
                <?php }
            }?>
        </tbody>
    </table>
    <nav aria-label="Page navigation">
        <?php echo $this->pagination->create_links(); ?>
    </nav>

</div>
</div>


<?php
$this->load->view('page/sidebar');
?>
</div>

</div>

</div>