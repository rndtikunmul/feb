<div role="main" class="main">

	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col">
					<ul class="breadcrumb">
						<li><a href=""><?= $datas->pageHeadEng?></a></li>
						<li class="active"><?= $datas->pageNamaEng?></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<h1><?= $datas->pageNamaEng?></h1>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">
			<div class="col-lg-9">
				<div class="blog-posts single-post">
					<article class="post post-large blog-single-post">
						<div class="post-content">	
							<p><?= $datas->pageContenteng?></p>
							<!-- /<i class="fa fa-tag"> <?= $datas->pageTag?></i>  -->
						</div>
					</article>

				</div>
			</div>
			<?php if ($datas->pageLink == 'NoLink'){ ?>
			<?php  $this->load->view('page/sidebareng');?>
		    <?php }?>
		</div>

	</div>


</div>