<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url()?>">Informasi Pendaftaran</a></li>
                        <li class="active"><?=$is_active ?></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1><?=$is_active ?></h1>
                </div>
            </div>
            
            <div id="response"></div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-lg-9">
                <div class="blog-posts">
                  
             </div>
        </div>

<?php
$this->load->view('page/sidebar');
?>
</div>

</div>

</div>