<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li class="active">Informasi Pendaftaran</li>
                        <li class="active"><?= $is_active ?></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1><?= $is_active ?></h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-lg-9">
                <div class="blog-posts single-post">
                    <article class="post post-large blog-single-post">
                        <div class="post-image">
                            <div class="owl-carousel owl-theme" data-plugin-options="{'items':1}">
                           
                        </div>
                       
                        <div class="post-content">
                            <p><?= $datasinfo->infodaftarContent ?></p>
                            <i class="fa fa-tag"> <?= $datasinfo->infodaftarNama ?></i>
                        </div>
                    </article>

                </div>
            </div>

            <?php
            $this->load->view('page/sidebar');
            ?>
        </div>

    </div>


</div>