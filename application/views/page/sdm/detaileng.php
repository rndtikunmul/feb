<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url()?>">About FEB</a></li>
                        <li class="active"><?=$is_active ?></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1><?=$datassdm->datas->pegmNama ?></h1>
                </div>
            </div>
            <div id="response"></div>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="owl-carousel owl-theme" data-plugin-options="{'items': 1, 'margin': 10}">
                                <div>
                                    <span class="img-thumbnail d-block">
                                          <?php if ($karyawan->karyawanFoto == true) { ?>
                                        <img alt="" height="300" class="img-fluid" src="<?php echo base_url('sdm/loadimage/'). $karyawan->karyawanFoto ?>">
                                         <?php }else{ ?>
                                             <img alt="" height="300" class="img-fluid" src="<?php echo base_url('sdm/loadimage/noimage.jpg') ?>">
                                        <?php } ?>

                                    </span>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-8">

                            <table class="table table-striped">
                                <thead>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Name </td>
                                        <td><?=$datassdm->datas->namaGelar ?></td>

                                    </tr>
                                    tr>
                                        <td>NIP </td>
                                        <td><?=$datassdm->datas->pegmNIP ?></td>

                                    </tr>
                                    <tr>
                                        <td>Unit</td>
                                        <td><?=$datassdm->datas->unitNama ?></td>

                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><?=$datassdm->datas->pegmEmail ?></td>

                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td><?=$datassdm->datas->pegjenisNama ?></td>

                                    </tr>
                                    <tr>
                                        <td>Unit</td>
                                        <td><?=$datassdm->datas->unitNama ?></td>

                                    </tr>
                                    <tr>
                                        <td>Grade</td>
                                        <td><?=$datassdm->datas->golNama ?></td>
                                    </tr>
                                    <tr>
                                        <td>Group</td>
                                        <td><?=$datassdm->datas->golId ?></td>

                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                         <div class="col-lg-12">
                            <h4></h4>
                            <div class="toggle toggle-primary toggle-sm" data-plugin-toggle>
                                <?php if($karyawan->karyawanDetail==true) { ?>
                                <section class="toggle active">
                                    <label>Detail</label>
                                    <div class="toggle-content">
                                        <p><?=$karyawan->karyawanDetail?>
                                         </p>
                                    </div>
                                </section>
                                 <?php } ?>
                                <?php if($penelitian->status==true) { ?>
                                <section class="toggle">
                                    <label>Research</label>
                                    <div class="toggle-content">
                                        <p>
                                            <table class="table table-hover">
                                                <thead>

                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if($penelitian!=false)
                                                    {
                                                        $i = 1;
                                                        foreach($penelitian->data as $row)
                                                        {  
                                                         //  if($row->status !=false) // {

                                                         ?>
                                                         <tr>
                                                            <th scope="row"><?=$i++?></th>
                                                            <td>
                                                                <table class="table table-striped">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>Title </td>
                                                                            <td> 
                                                                                <?=$row->pltjudul ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Writer </td>
                                                                                <td> 
                                                                                    <?=$row->pltpenulis ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>URL</td>
                                                                                    <td><?=$row->plturl ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Activity</td>
                                                                                    <td><?=$row->rkgkegiatan ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Year</td>
                                                                                    <td><?=$row->plttahunterbit ?></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                             // }
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </p>
                                        </div>
                                    </section>
                                    <?php } ?>
                                <?php if($pengabdian->status!=false) { ?>
                                <section class="toggle">
                                    <label>Dedication</label>
                                    <div class="toggle-content">
                                        <p>
                                            <table class="table table-hover">
                                                <thead>

                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if($pengabdian!=false)
                                                    {
                                                        $i = 1;
                                                        foreach($pengabdian->data as $row)
                                                        {  

                                                         //  if($row->status !=false) // {

                                                         ?>
                                                         <tr>
                                                            <th scope="row"><?=$i++?></th>
                                                            <td>
                                                                <table class="table table-striped">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>Title </td>
                                                                            <td> 
                                                                                <?=$row->pgdketerangan ?></td>
                                                                            </tr>
                                                                                <tr>
                                                                                    <td>Information</td>
                                                                                    <td><?=$row->pgdnomor ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Activity</td>
                                                                                    <td><?=$row->rkgkegiatan ?> </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Year</td>
                                                                                    <td><?=$row->rkgtahun ?></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                     // }
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                        </p>
                                    </div>
                                </section>
                                 <?php } ?>
                                 <!-- <section class="toggle">
                                    <label>Jurnal</label>
                                    <div class="toggle-content">
                                        <p>Lorem ipsum dolor sit amet.</p>
                                    </div>
                                </section>
                                 <section class="toggle">
                                    <label>Aktivitas Akademik</label>
                                    <div class="toggle-content">
                                        <p>Lorem ipsum dolor sit amet.</p>
                                    </div>
                                </section>
                                 <section class="toggle">
                                    <label>Professional Activity</label>
                                    <div class="toggle-content">
                                        <p>Lorem ipsum dolor sit amet.</p>
                                    </div>
                                </section> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $this->load->view('page/sidebareng');
        ?>
    </div>
</div>
</div>