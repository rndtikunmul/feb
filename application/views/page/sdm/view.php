<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url()?>">Tentang Kami</a></li>
                        <li class="active"><?=$is_active ?></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1><?=$is_active ?></h1>
                </div>
            </div>
            
            <div id="response"></div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-lg-9">
                <div class="blog-posts">
                    <div class="featured-boxes featured-boxes-style-3 featured-boxes-flat">
                        <div class="row">
                           
                            <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-quaternary featured-box-effect-3">
                                    <div class="box-content">
                                       <a href="<?= base_url(); ?>sdm/post/1"><i class="icon-featured fa fa-user"></i></a>
                                        <h4>Dosen PNS</h4>
                                        <p>Dosen PNS</p>
                                    </div>
                                </div>
                            </div>
                             <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-quaternary featured-box-effect-3">
                                    <div class="box-content">
                                       <a href="<?= base_url(); ?>sdm/post/12"><i class="icon-featured fa fa-user"></i></a>
                                        <h4>Dosen Non PNS</h4>
                                        <p>Dosen NON PNS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-quaternary featured-box-effect-3">
                                    <div class="box-content">
                                       <a href="<?= base_url(); ?>sdm/post/2"><i class="icon-featured fa fa-user"></i></a>
                                        <h4>Tendik PNS</h4>
                                        <p>Tenaga Kependidikan PNS. </p>
                                    </div>
                                </div>
                            </div>
                             <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-primary featured-box-effect-3">
                                    <div class="box-content">
                                         <a href="<?= base_url(); ?>sdm/post/3"><i class="icon-featured fa fa-user"></i></a>
                                        <h4>Tendik Non PNS</h4>
                                        <p>Tenaga Kependidikan Non PNS.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

   

             </div>
        </div>

<?php
$this->load->view('page/sidebar');
?>
</div>

</div>

</div>