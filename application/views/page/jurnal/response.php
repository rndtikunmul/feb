<table class="table">
    <thead>
        <tr>
            <th>
                No.
            </th>
            <th>
                Judul Jurnal
            </th>
            <th>
                Penulis
            </th>
            <th>
                Link
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <?php
            if($jurnal!==false) {
                $i = 1;
                foreach($jurnal as $row) { ?>
            <td>
                <?=$i++?>
            </td>
            <td>
                <?=$row->jurnalJudul?>
            </td>
            <td>
                <?=$row->jurnalPenulis?>
            </td>
            <td>
                <?=$row->jurnalLink?>
            </td>
            <?php }
			}?>
        </tr>        
    </tbody>
</table>