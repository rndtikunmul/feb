<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url()?>">Home</a></li>
                        <li class="active">Jurnal</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1>Jurnal</h1>
                </div>
            </div>
            
            <div id="response"></div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-lg-9">
                <div class="blog-posts">
                    <?php
                    if($jurnal!==false) {
                      $i = 1;
                      foreach($jurnal as $row) { ?>
                       <div class="accordion" id="accordion2">                       
                        <div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="feature-box">
                                        <div class="feature-box-icon">
                                            <i class="fa fa-group"></i>
                                        </div>
                                        <div class="feature-box-info">
                                            <h4 class="heading-primary mb-0"><?=$row->jurnalJudul?></h4>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="post-meta">
                                                        <span><i class="fa fa-calendar"></i> <?=$row->jurnalTgl?> </span>
                                                        <span><i class="fa fa-user"></i>By <a ><?=$row->jurnalPenulis?></a> </span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="mb-4"><a href="<?=$row->jurnalLink?>">Link Jurnal</a></p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                <?php }
            }?>

        </ul>

    </div>
</div>

<?php
$this->load->view('page/sidebar');
?>
</div>

</div>

</div>