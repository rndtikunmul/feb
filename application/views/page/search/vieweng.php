<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url()?>">Home</a></li>
                        <!-- <li class="active">Ragam Informasi</li> -->
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1>News, Event, Announcement, Journal & Agenda</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-lg-9">
                <div class="blog-posts">
                <?php if ($keyword !== FALSE): ?>
    <div>
        <h4>Search results with keyword "<?=$this->session->keyword?>"</h4>
    </div>
    <br>
    <br>
    <?php if (empty($datassearch)): ?>
      <div>
        <p>Datas Not Found with Keyword "<?=$this->session->keyword?>"</p>
      </div>
    <?php endif ?>
  <?php endif; ?> 
                <?php
	                if($datassearch!==false) {
	                	$i = 1;
						foreach($datassearch as $row) { ?>

                            <!-- <?=$row->judul?>  -->
                      <?php if ($row->beritaSection == in_array($row->beritaSection , array("agenda","berita","pengumuman","kegiatan"))) { ?>

                    <article class="post post-medium">
                        <div class="row">

                            <div class="col-lg-5">
                                <div class="post-image">
                                        <div>
                                            <div class="img-thumbnail">
                                                <img class="img-fluid" src="<?php echo base_url('berita/loadthumb/') . $row->beritaBanner ?>" alt="">
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="col-lg-7">

                                <div class="post-content">
                                    <h2><a href="<?php echo base_url() ?>berita/post/<?= $row->BeritaNamaEng ?>"><?=$row->BeritaJudulEng?></a></h2>
                                    <p><?= substr(strip_tags($row->BeritaContentEng), 0, 200) . '...'; ?></p>
                                    
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="post-meta">
                                    <span><i class="fa fa-calendar"></i> <?=$row->beritaDatetime?> </span>
                                    <span><i class="fa fa-user"></i> By <a ><?=$row->beritaAuthor?></a> </span>
                                    <span><i class="fa fa-tag"></i> <a ><?=$row->beritaTag?></a> </span>
                                    <span class="d-block d-md-inline-block float-md-right mt-3 mt-md-0"><a href="<?php echo base_url() ?>berita/post/<?= $row->BeritaNamaEng ?>" class="btn btn-xs btn-primary">Read more...</a></span>
                                </div>
                            </div>
                        </div>
                    </article>
                <?php }else { ?>
                     <div class="accordion" id="accordion2">                       
                        <div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="feature-box">
                                        <div class="feature-box-icon">
                                            <i class="fa fa-group"></i>
                                        </div>
                                        <div class="feature-box-info">
                                            <h4 class="heading-primary mb-0"><?=$row->BeritaJudulEng?></h4>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="post-meta">
                                                        <span><i class="fa fa-calendar"></i> <?=$row->beritaDatetime?> </span>
                                                        <span><i class="fa fa-user"></i>By <a ><?=$row->BeritaNamaEng?></a> </span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="mb-4"><a href="<?=$row->beritaTag?>">Journal Link</a></p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                <?php } ?>
            <?php   $i++;}} ?>
                    
                    </ul>

                </div>
            </div>

            <?php
            $this->load->view('page/sidebareng');
            ?>
        </div>

    </div>

</div>