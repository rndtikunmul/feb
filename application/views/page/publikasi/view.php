<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url()?>">Publikasi</a></li>
                        <li class="active"><?=$is_active?> <?=$jenis_publikasi?></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1><?=$jenis_publikasi?></h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-lg-9">

                <form id="contactForm"action="<?=$searchpublikasi_url?>" method="POST">
                    <div class="form-row">
                       <div class="form-group col">
                            <label>Tahun</label>
                            <select class="form-control m-select2" id="hakakses" name="tahun">
                             <option value=""></option>
                             <?php for ($x = date('Y'); $x >= 2019; $x--) { ?>
                                <option value="<?= $x ?>"><?= $x ?></option>
                            <?php } ?>
                        </select>
                    </div>
                   
                    <?php if (($jenis_publikasi !='Penelitian') and ($jenis_publikasi !='Pengabdian Masyarakat')) {?>
                    <div class="form-group col">
                        <label>Jenis <?=$jenis_publikasi?></label>
                        <select class="form-control m-select2" name="jenisp">
                           <option value=""></option>
                           <option value="%">Semua</option>
                            <?php
                                    if ($jenis_pub->data != false) {
                                        $rsuket = '';
                                        $i = 1;
                                        foreach ($jenis_pub->data as $row) {
                                            if ($rsuket != $row->rsuket) {
                                                if ($i > 1) {
                                                    echo '</optgroup>';
                                                }
                                                $rsuket = $row->rsuket;
                                                echo '<optgroup label="' . $rsuket . '">';
                                            }
                                            echo '<option value="' . $row->rkgid . '">'.$row->rkgkegiatan . '</option>';
                                            $i++;
                                        }
                                    }
                                    ?>
                        endforeach;
                        ?>

                    </select>
                </div>
                  <?php }?>
            </div>
            <div class="form-row">
                <div class="form-group col">
                    <input type="submit" value="Cari" class="btn btn-primary btn-lg" data-loading-text="Loading...">
                </div>
            </div>
        </form>

        <?php if ($dataspublikasi->status !== false) { ?> 

            <div class="col-lg-12 text-center">
                <h2 class="mt-2 mb-0"><strong> <?=$jenis_publikasi?></strong></h2>
                <p class="lead"> <?=$jenisp?> <span class="alternative-font text-4"> Tahun
                     <?php if ($jenis_publikasi !='Pengabdian Masyarakat') { ?>
                         <?=$tahun?>
                      <?php }else{ ?>
                     <?=$tahunpm?>
                      <?php } ?> </span></p>
            </div>

        

            <table class="table table-hover">
              <thead>
                 <tr>
                    <?php if ($jenis_publikasi !='Pengabdian Masyarakat') {?>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Penulis</th>
                    <th>Anggota Penulis</th> 
                    <th>Detail</th> 
                    <?php }else{ ?> 
                    <th>No</th>
                    <th>Uraian</th>
                    <th>Nama Dosen</th>
                    <th>Detail</th> 
                    <?php } ?> 
                </tr>
            </thead>
            <tbody>
                <?php
                    // print_r($jeniskerjasama);
                    // exit();
                // $nomor = 1;
                  $nomor = $offset+1;
                foreach($dataspublikasi->data as $row) { ?>

                     <?php if ($jenis_publikasi !='Pengabdian Masyarakat') {?>
                 <tr>
                    <th scope="row"><?=$nomor++?></th>
                    <td><?=$row->pltjudul?> <br> 
                        <?=$row->pltnama?></td>
                    <td><?=$row->pltpenulis?></td>
                    <td><?=$row->pegmNama?> (<?=$row->sebagai?>)</td>
                    <td>Penerbit : <?=$row->pltpenerbit?> <br> ISBN : <?=$row->pltisbn?></td>
                </tr>
            <?php }else{ ?> 

                  <tr>
                    <th scope="row"><?=$nomor++?></th>
                    <td><?=$row->pgdketerangan?></td>
                    <td><?=$row->pegmNama?></td>
                    <td>Semester : <?=$row->pgdsemester?> <br>
                        Tanggal :<?=$row->pgdtanggal?></td>
                        <!-- Nomor SK : <?=$row->pgdnomor?> -->
                </tr>

             <?php } } 
            ?> 
        </tbody>
    </table>
     <nav aria-label="Page navigation">
        <?php echo $this->pagination->create_links(); ?>
    </nav>
<?php }else{?>
      <div class="col-lg-12 text-center">
                <h2 class="mt-2 mb-0"><strong> <?=$jenis_publikasi?></strong></h2>
                <p class="lead">Data Tidak Di Temukan  <span class="alternative-font text-4"> Tahun  
                    <?php if ($jenis_publikasi !='Pengabdian Masyarakat') { ?>
                       <?=$tahun?>
                   <?php }else{ ?>
                       <?=$tahunpm?>
                   <?php } ?>
                       </span></p>
            </div>
<?php }?>
</div>

 <?php
    $this->load->view('page/sidebar');
    ?>
</div>

</div>

</div>