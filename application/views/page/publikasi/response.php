<div role="main" class="main">
    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="<?= base_url(); ?>prestasimhs">Kerjasama</a></li>
                        <li class="active"><?=$is_active?></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1><?=$is_active?> </h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <form id="cari" action="<?=$searchkaryawan_url?>" method="get">
                        <div class="input-group input-group-4">
                            <input class="form-control" placeholder="Search..." name="search" id="keyword" type="text">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <table class="table table-hover">
                      <thead>
                       <tr>
                        <th>No</th>
                        <th>Nama Kerjasama</th>
                        <th>Kerjasama Dengan</th>
                        <th>Tahun</th>
                        <th>Lampiran</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // print_r($jeniskerjasama);
                    // exit();
                    if($jeniskerjasama!==false) {
                      $i = 1;
                      $nomor = $offset+1;
                      foreach($jeniskerjasama as $row) { ?>
                       <tr>
                        <th scope="row"><?=$nomor++?></th>
                        <td><?=$row->kerjasamaNama?></td>
                        <td><?=$row->kerjasamaLembaga?></td>
                        <td><?=$row->kerjasamaTahun?></td>
                        <td><a href="<?=base_url('kerjasama/loadattach/').$row->kerjasamaFile?>" target="_blank" title="Lihat" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                            <span>
                                <i class="fa fa-eye"> LIHAT</i>
                            </span>
                        </a></td>
                    </tr>
                <?php }
            }?>
        </tbody>
    </table>
    <nav aria-label="Page navigation">
        <?php echo $this->pagination->create_links(); ?>
    </nav>

</div>
</div>


<?php
$this->load->view('page/sidebar');
?>
</div>

</div>

</div>