<div role="main" class="main">
    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li class="active">Jurusan</li>
                        <li class="active">Prodi</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1><?= $datas->prodiNama?></h1>
                </div>
            </div>
        </div>
    </section>

    <div class="slider-container light rev_slider_wrapper">
        <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 500, 'gridheight': 500, 'responsiveLevels': [4096,1200,992,500]}">
            <ul>  
             <?php if (!empty($slider)) {
                foreach ($slider as $row) { ?>

                    <li data-transition="fade">

                        <img src="<?=base_url('f_home/loadslider/').$row->sliderFiles?>" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">

                        <div class="rs-background-video-layer" data-forcerewind="on" data-volume="mute" data-videowidth="100%" data-videoheight="100%" data-videomp4="<?=base_url('f_home/loadslider/').$row->sliderFiles?>" data-videopreload="preload" data-videoloop="loop" data-forceCover="1" data-aspectratio="16:9" data-autoplay="true" data-autoplayonlyfirsttime="false" ></div>

                    </li>
                <?php } } ?>

            </ul>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-3" style="margin-top: 20px">
                <aside class="sidebar">

                    <ul class="nav nav-list flex-column mb-4 sort-source" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
                        <!--   <li class="nav-item" data-option-value="*"><a class="nav-link active" href="#">Semua</a></li> -->
                        <li class="nav-item" data-option-value=".profil"><a class="nav-link" href="#">Profil</a></li>

                        <!-- <li class="nav-item" data-option-value=".pendidikan"><a class="nav-link" href="#">Pendidikan</a></li> -->

                        <!-- <li class="nav-item" data-option-value=".penelitian"><a class="nav-link" href="#">Penelitian dan Pengabdian kepada masyarakat</a></li> -->
                      <?php if ($prodiId_select != in_array($prodiId_select, array("2","4","8"))) { ?>
                             <li class="nav-item" data-option-value=".download"><a class="nav-link" href="#">Download</a></li>

                        <?php } ?>
                       
                        <li class="nav-item" data-option-value=".informasi"><a class="nav-link" href="#">Ragam Informasi</a></li>
                        <li class="nav-item" data-option-value=".galeri"><a class="nav-link" href="#">Galeri</a></li>
                        <li class="nav-item" data-option-value=".staff"><a class="nav-link" href="#">Sumber Daya Manusia</a>
                        </li>
                    </ul>
                    <hr class="invisible mt-5 mb-2">
                </aside>
            </div>

            <div class="col-lg-6" style="margin-top: 20px">
                <div class="sort-destination-loader sort-destination-loader-showing">
                    <div class="row portfolio-list sort-destination" data-sort-id="portfolio">
                        <div class="col-lg-12 isotope-item profil">
                            <div class="portfolio-item">
                                <h3 class="heading-primary"><strong>Profil</strong> <?= $datas->prodiNama?></h3>
                                <div class="accordion" id="accordion">
                                    <div class="container">
                                        <div class="row">
                                            <!-- <div class="col-lg-4">
                                                <div class="owl-carousel owl-theme" data-plugin-options="{'items': 1, 'margin': 10}">
                                                    <div>
                                                        <span class="img-thumbnail d-block">
                                                            <img alt="" height="300" class="img-fluid" src="<?php echo base_url(); ?>front/img/prodi_logo/<?=$datas->prodiNama?>.jpg">
                                                        </span>
                                                    </div>
                                                    <div>
                                                        <span class="img-thumbnail d-block">
                                                            <img alt="" height="300" class="img-fluid" src="<?php echo base_url(); ?>front/img/prodi_logo/<?=$datas->prodiNama?>".jpg">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="col-lg-12">

                                                <h2 class="mb-0"><strong>Profil Singkat</strong></h2>
                                                <h4 class="heading-primary"><?= $datas->prodiNama?></h4>

                                                <hr class="solid">

                                                <p><?= $datas->prodiVisi?></p>
                                                <p><?= $datas->prodiMisi?></p>
                                            </div>
                                        </div>
                                    </div>

                                <?php if($datas->prodiDesk!=false){?>
                                    <h4 class="mb-2" id="sub-second-1">Sejarah Singkat</h4>
                                    <p> <?= $datas->prodiDesk?></p>
                                <?php }?>
                                    <!-- <h4 class="mb-2" id="sub-second-1">Tujuan</h4>
                                        <p> <?= $datas->prodiTujuan?></p> -->
                                    </div>
                                </div>
                            </div>
                    <!--     <div class="col-lg-12 isotope-item pendidikan">
                            <div class="portfolio-item">
                                <h3 class="heading-primary"><strong>Pendidikan</strong> <?= $datas->prodiNama?></h3>
                                <div class="accordion" id="accordion2">
                                    <section class="call-to-action featured featured-primary">
                                        <div class="col-lg-12">
                                            <div class="call-to-action-content">
                                                <?= $datas->prodiKurikulum ?>
                                            </div>
                                        </div>
                                    </section>

                                    <section class="call-to-action featured featured-primary">
                                        <div class="col-lg-12">
                                            <div class="call-to-action-content">
                                                <?= $datas->prodiKegiatan?>
                                            </div>
                                        </div>
                                    </section>
                                </div>                          
                            </div>
                        </div> -->
                        <!-- <div class="col-lg-12 isotope-item penelitian">
                            <div class="portfolio-item">
                                <h3 class="heading-primary"><strong>Penelitian dan Pengabdian kepada masyarakat</strong></h3>
                                <div class="accordion" id="accordion3">
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse3One">
                                                    Daftar Penelitian dan Publikasi
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse1One" class="collapse show">
                                            <div class="card-body">
                                                <?= $datas->prodiPenelitian?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse3Two">
                                                    Daftar Pengabdian Masyarakat
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse3Two" class="collapse">
                                            <div class="card-body">
                                                <?= $datas->prodiPengabdian?>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>                            
                            </div>
                        </div> -->
                         <?php if ($prodiId_select != in_array($prodiId_select, array("2","4","8"))) { ?>
                     
                        <div class="col-lg-12 isotope-item download">
                            <div class="portfolio-item">
                                <h3 class="heading-primary"><strong>Download</strong></h3>
                                <div>
                                <?php if($datas->prodiPanduan!=false){?>
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                    Panduan
                                            </h4>
                                        </div>
                                       
                                        <div id="collapse1One" class="collapse show">
                                            <div class="card-body">
                                                <?= $datas->prodiPanduan?>
                                            </div>
                                        </div>
                                    </div>
                                <?php }?>
                                <?php if($datas->prodiJadwal!=false){?>
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapse4Two">
                                                    Jadwal
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse1Two" class="collapse">
                                            <div class="card-body">
                                                <?= $datas->prodiJadwal?>
                                            </div>
                                        </div>
                                    </div>
                                <?php }?>
                                <?php if($datas->prodiBuku!=false){?>
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapse4Three">
                                                    Buku Modul, Praktikum, dan Keterampilan medik
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse4Three" class="collapse">
                                            <div class="card-body">
                                                <?= $datas->prodiBuku?></p>
                                            </div>
                                        </div>
                                    </div>
                                <?php }?>
                                <?php if($datas->prodiFormulir!=false){?>
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapse4Four">
                                                    Formulir
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse4Four" class="collapse">
                                            <div class="card-body">
                                                <?= $datas->prodiFormulir?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                     <?php if($foto!=false){?>
                        <div class="col-lg-12 isotope-item galeri">
                            <div class="portfolio-item">
                                <h3 class="heading-primary"><strong>Galeri</strong></h3>
                                <div class="row image-gallery lightbox" data-plugin-options="{'delegate': 'a.lightbox-portfolio', 'type': 'image', 'gallery': {'enabled': true}}">
                                    <?php
                                    if($foto!==false) {
                                        $i = 1;
                                        foreach($foto as $row) { ?>
                                            <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                                <div class="image-gallery-item">
                                                    <a href="<?=base_url('prodi/loadimage/').$row->galfFiles?>" class="lightbox-portfolio">
                                                        <span class="thumb-info">
                                                            <span class="thumb-info-wrapper">
                                                                <img src="<?=base_url('prodi/loadimage/').$row->galfFiles?>" class="img-fluid" alt="">
                                                                <span class="thumb-info-title">
                                                                    <span class="thumb-info-inner"><?= $row->galfNama?></span>
                                                                    <span class="thumb-info-type"><?= $row->headgalfNama?></span>
                                                                </span>
                                                                <span class="thumb-info-action">
                                                                    <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                                                                </span>
                                                            </span>
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        <?php }
                                    }?>
                                </div>
                            </div>
                        </div>
                         <?php } ?>
                        <div class="col-lg-12 isotope-item informasi">
                            <div class="portfolio-item">
                                <div class="col-lg-12">
                                    <div class="blog-posts">
                                        <?php
                                        if($pengumuman!==false) {
                                            $i = 1;
                                            foreach($pengumuman as $row) { ?>

                                                <article class="post post-medium">
                                                    <div class="row">

                                                        <div class="col-lg-3">
                                                            <div class="post-image">
                                                                <div>
                                                                    <div class="img-thumbnail">
                                                                        <img class="img-fluid" src="<?php echo base_url('pengumuman/loadthumb/') . $row->beritaBanner ?>" alt="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-9">

                                                            <div class="post-content">

                                                                <h4><a href="<?php echo base_url() ?>pengumuman/post/<?= $row->beritaNama ?>"><?=$row->beritaJudul?></a></h4>
                                                                
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="post-meta">
                                                                <span><i class="fa fa-calendar"></i> <?=datetoindo($row->beritaDatetime)?> </span>
                                                                <span><i class="fa fa-user"></i> By <a ><?=$row->beritaAuthor?></a> </span>
                                                                <span><i class="fa fa-tag"></i> <a ><?=$row->beritaTag?></a> </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </article>
                                            <?php }
                                        }?>

                                    </div>

                                </div>

                            </div>
                        </div> 
                    <div class="col-lg-12 isotope-item staff">
                        <div class="portfolio-item">
                            <h3 class="heading-primary"><strong>Staff</strong> <?= $datas->prodiNama?></h3>
                            <div class="featured-boxes featured-boxes-style-3 featured-boxes-flat">
                        <div class="row">
                            <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-quaternary featured-box-effect-2">
                                    <div class="box-content">
                                       <a href="<?= base_url(); ?>prodi/post_sdm/<?=$prorjur_select ?>/11">
                                        <i class="icon-featured fa fa-user"></i></a>
                                        <h4>Dosen PNS</h4>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-quaternary featured-box-effect-2">
                                    <div class="box-content">
                                       <a href="<?= base_url(); ?>prodi/post_sdm/<?= $prorjur_select?>/12">
                                        <i class="icon-featured fa fa-user"></i></a>
                                        <h4>Dosen Non PNS</h4>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-quaternary featured-box-effect-3">
                                    <div class="box-content">
                                       <a href="<?= base_url(); ?>prodi/post_sdm/<?= $prorjur_select?>/22"><i class="icon-featured fa fa-user"></i></a>
                                        <h4>Tendik PNS</h4>
                                        <!-- <p>Tenaga Kependidikan PNS. </p> -->
                                    </div>
                                </div>
                            </div>
                             <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-primary featured-box-effect-3">
                                    <div class="box-content">
                                         <a href="<?= base_url(); ?>prodi/post_sdm/<?=$prorjur_select ?>/33"><i class="icon-featured fa fa-user"></i></a>
                                        <h4>Tendik Non PNS</h4>
                                        <!-- <p>Tenaga Kependidikan Non PNS.</p> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>  
                </div>
            </div>

        </div>

    </div>
    <?php
    $this->load->view('page/sidebar');
    ?>
</div>


</div>