<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                       <li class="active">Jurusan</li>
                        <li class="active"><?= $prodinama?></li>
                       
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1><?=$is_active ?> <?= $prodinama?></h1>
                </div>
            </div>
            
            <div id="response"></div>
        </div>
    </section>


    <div class="container">

        <div class="row">
            <div class="col-lg-9">
                <div class="blog-posts">
                    <div class="featured-boxes featured-boxes-style-3 featured-boxes-flat">
                        <div class="row">
                            <form id="cari" action="<?=$searchkaryawan_url?>" method="get">
                                <div class="input-group input-group-4">
                                    <input class="form-control" placeholder="Search..." name="searchkaryawan" id="keyword" type="text">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </form>

                             <table class="table table-hover">
                                <thead>
                                   <!--  <tr>
                                        <th> No </th>
                                        <th> Foto </th>
                                        <th> Keterangan </th>
                                     

                                    </tr> -->
                                </thead>
                                <tbody>
                                    <?php
                                   if($staff!=false)
                                    {
                                        $i = 1;
                                        $nomor = $offset+1;
                                        foreach($staff as $row)
                                        {  
                                            
                                             if($row->status !=false)
                                    {
                                            
                                             $key = $this->encryptions->encode($row->datas->pegmNIP, $this->config->item('encryption_key'));
                                           $i = 1;
                                           ?>
                                           <tr>
                                            <th scope="row"><?=$nomor++?></th>
                                            <td> 
                                                <!-- <div class="col-lg-4" style="width: 200px; height: 60px"> -->
                                              
                                              <?php if ($row->datas->karyawanFoto == true) { ?>
                                              <img style="width: 140px; height: 170px"  class="img-thumbnail d-block" src="<?php echo base_url('sdm/loadimage/'). $row->datas->karyawanFoto?>">
                                              <?php }else{ ?>
                                                 <img style="width: 140px; height: 170px"  alt="" height="300" class="img-fluid" src="<?php echo base_url('sdm/loadimage/noimage.jpg') ?>">
                                              <?php } ?>

                                              <!-- </div> -->
                                          </td>
                                           <td>
                                                <table class="table table-striped">
                                                    <tbody>
                                                        <tr>
                                                            <td>Nama </td>
                                                            <td>  <a href="<?= $detail_url . $key ?>">
                                                            <?=$row->datas->namaGelar ?> </a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Unit</td>
                                                            <td><?=$row->datas->unitNama ?></td>
                                                        </tr>
                                                         <tr>
                                                            <td>Email</td>
                                                            <td><?=$row->datas->pegmEmail ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Status</td>
                                                            <td><?=$row->datas->pegjenisNama ?></td>
                                                       </tr>
                                                    </tbody>
                                                 </table>
                                           </td>
                                         </tr>
                                         <?php
                                     } }
                                 }
                                 ?>
                             </tbody>
                         </table>
                         <nav aria-label="Page navigation">
                                <?php echo $this->pagination->create_links(); ?>
                            </nav>

                    </div>
                </div>



            </div>
        </div>

        <?php
        $this->load->view('page/sidebar');
        ?>
    </div>

</div>

</div>