<div role="main" class="main">
    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li class="active">Jurusan</li>
                        <li class="active"><?= $datas->prodiNama?></li>
                        <?php if ($menuprodi!=false) { ?>
                           <?php if ($contenprodi!=false) { ?>
                            <li class="active"><?=$contenprodi->pageJudul?></li>
                        <?php } } ?>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1><?= $datas->prodiNama?></h1>
                </div>
            </div>
        </div>
    </section>

    <div class="slider-container light rev_slider_wrapper">
        <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options="{'delay': 3000, 'gridwidth': 500, 'gridheight': 500, 'responsiveLevels': [4096,1200,992,500]}">
            <ul>  
               <?php if (!empty($slider)) {
                foreach ($slider as $row) { ?>

                    <li data-transition="fade">

                        <img src="<?=base_url('f_home/loadslider/').$row->sliderFiles?>" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">

                        <div class="rs-background-video-layer" data-forcerewind="on" data-volume="mute" data-videowidth="100%" data-videoheight="100%" data-videomp4="<?=base_url('f_home/loadslider/').$row->sliderFiles?>" data-videopreload="preload" data-videoloop="loop" data-forceCover="1" data-aspectratio="16:9" data-autoplay="true" data-autoplayonlyfirsttime="true" ></div>

                    </li>
                <?php } } ?>

            </ul>
        </div>
    </div>
    <section class="page-header">
    </section>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 order-2 order-lg-1">
                <aside class="sidebar">

                    <h4 class="heading-primary">Menu Prodi</h4>
                    <ul class="nav nav-list flex-column mb-5">
                        <?php if ($menuprodi!=false) {
                            foreach ($menuprodi as $row) { ?>
                               <li class="nav-item"><a class="nav-link" href="<?php echo base_url() ?>prodi/post/<?= $prodi_active ?>/<?=$row->pageId?>">
                                <?=$row->pageJudul?></a></li>
                            <?php } } ?>

                            <li class="nav-item"><a class="nav-link" href="<?php echo base_url() ?>prodi/post/<?= $prodi_active ?>/sdm">Sumber Daya Manusia</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo base_url() ?>prodi/ragam_informasi/<?= $prodi_active ?>/berita">Ragam Informasi</a></li>

                        </ul>
                        <hr>
                        <?php if ($pageId =='sdm') { ?>
                        </aside>
                    </div>
                    <div class="col-lg-9 order-1 order-lg-2">
                     <h2>Sumber Daya Manusia</h2>

                     <div class="row">
                        <div class="col">
                         <p class="lead">
                          <div class="portfolio-item">
                            <h3 class="heading-primary"><strong>Staff</strong> <?= $datas->prodiNama?></h3>
                            <div class="featured-boxes featured-boxes-style-3 featured-boxes-flat">
                        <div class="row">
                            <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-quaternary featured-box-effect-2">
                                    <div class="box-content">
                                       <a href="<?= base_url(); ?>prodi/post_sdm/<?=$prorjur_select ?>_11">
                                        <i class="icon-featured fa fa-user"></i></a>
                                        <h4>Dosen PNS</h4>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-quaternary featured-box-effect-2">
                                    <div class="box-content">
                                       <a href="<?= base_url(); ?>prodi/post_sdm/<?= $prorjur_select?>_12">
                                        <i class="icon-featured fa fa-user"></i></a>
                                        <h4>Dosen Non PNS</h4>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-quaternary featured-box-effect-3">
                                    <div class="box-content">
                                       <a href="<?= base_url(); ?>prodi/post_sdm/<?= $prorjur_select?>_22"><i class="icon-featured fa fa-user"></i></a>
                                        <h4>Tendik PNS</h4>
                                        <!-- <p>Tenaga Kependidikan PNS. </p> -->
                                    </div>
                                </div>
                            </div>
                             <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-primary featured-box-effect-3">
                                    <div class="box-content">
                                         <a href="<?= base_url(); ?>prodi/post_sdm/<?=$prorjur_select ?>_33"><i class="icon-featured fa fa-user"></i></a>
                                        <h4>Tendik Non PNS</h4>
                                        <!-- <p>Tenaga Kependidikan Non PNS.</p> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                      </p>
                      
                  </div>
              </div>
          </div>
      <?php  }  ?>

        <!--Ragam Informasi--- -->
        <?php if ($pageId =='berita') { ?>
                </aside>
            </div>
            <div class="col-lg-9 order-1 order-lg-2">
               <h2>Ragam Infromasi</h2>

                 <div class="portfolio-item">
                                <div class="col-lg-12">
                                    <div class="blog-posts">
                                        <?php
                                        if($pengumuman!==false) {
                                            $i = 1;
                                            foreach($pengumuman as $row) { ?>

                                                <article class="post post-medium">
                                                    <div class="row">

                                                        <div class="col-lg-3">
                                                            <div class="post-image">
                                                                <div>
                                                                    <div class="img-thumbnail">
                                                                    <?php if ($row->beritaBanner == true) { ?>
                                                                         <img class="img-fluid" src="<?php echo base_url('pengumuman/loadthumb/') . $row->beritaBanner ?>" alt="">
                                                                         <?php }else{ ?>
                                                                             <img alt="" height="300" class="img-fluid" src="<?php echo base_url('sdm/loadimage/noimage.jpg') ?>">
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-9">

                                                            <div class="post-content">

                                                                <h4><a href="<?php echo base_url() ?>pengumuman/post/<?= $row->beritaNama ?>"><?=$row->beritaJudul?></a></h4>
                                                                
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="post-meta">
                                                                <span><i class="fa fa-calendar"></i> <?=datetoindo($row->beritaDatetime)?> </span>
                                                                <span><i class="fa fa-user"></i> By <a ><?=$row->beritaAuthor?></a> </span>
                                                                <span><i class="fa fa-tag"></i> <a ><?=$row->beritaTag?></a> </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </article>
                                            <?php }
                                        }?>
                                    </div>

                                </div>

                            </div>
 <nav aria-label="Page navigation">
            <?php echo $this->pagination->create_links(); ?>
        </nav>
                      </div>

        <?php  }  ?>


        <!-- ----- -->
      <?php if ($menuprodi!=false) { ?>
       <?php if ($contenprodi!=false) { ?>
        <p> <?=$contenprodi->pageInfo?> </p>

    </aside>
</div>
<div class="col-lg-9 order-1 order-lg-2">
 <h2><?=$contenprodi->pageJudul?></h2>

 <div class="row">
    <div class="col">
     <p class="lead">
         <?=$contenprodi->pageContent?>
     </p>
 <?php  }  } ?>
</div>
</div>
</div>

</div>

</div>
</div>
