<div role="main" class="main">
	<section class="page-header">
		<div class="container">

            <!-- <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li class="active">Jurusan</li>
                        <li class="active">Prodi</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1><?= $datas->prodiNama?></h1>
                </div>
            </div> -->
            <div class="slider-container light rev_slider_wrapper">
            	<div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 500, 'gridheight': 500, 'responsiveLevels': [4096,1200,992,500]}">
            		<ul>  
            			<?php if (!empty($slider)) {
            				foreach ($slider as $row) { ?>

            					<li data-transition="fade" >
            						<div class="tp-caption"
            						data-x="center" 
            						data-y="center"
            						data-start="1000"
            						style="z-index: 5"
            						data-transform_in="x:[300%];opacity:0;s:500;"><img src="<?=base_url('f_home/loadslider/').$row->sliderFiles?>" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg"  >
            					</div>
							<!-- 
									<div class="tp-caption top-label"
									data-x="center" data-hoffset="0"
									data-y="center" data-voffset="-95"
									data-start="500"
									style="z-index: 5"
									data-transform_in="y:[-300%];opacity:0;s:500;">Fakultas Ekonomi & Bisnis</div>
								<div class="tp-caption main-label"
									data-x="center" data-hoffset="0"
									data-y="center" data-voffset="-45"
									data-start="1500"
									data-whitespace="nowrap"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">Prodi Studi <?= $datas->prodiNama?></div>

								<div class="tp-caption bottom-label"
									data-x="center" data-hoffset="0"
									data-y="center" data-voffset="5"
									data-start="2000"
									data-fontsize="['20','20','20','30']"
									style="z-index: 5"
									data-transform_in="y:[100%];opacity:0;s:500;">Universitas Mulawarman
								</div>
							-->
							<div  class="rs-background-video-layer" 
							data-x="center" 
							data-y="center"
							data-start="1000"
							data-forcerewind="on" 
							data-volume="mute"
							data-videowidth="center" 
							data-videoheight="center" 
							data-videomp4="<?=base_url('f_home/loadslider/').$row->sliderFiles?>" data-videopreload="preload" 
							data-videoloop="loop" 
							data-forceCover="1" 
							data-aspectratio="16:9" 
							data-autoplay="true" data-autoplayonlyfirsttime="false">
						</div>
					</li>
				<?php } } ?>
			</ul>
		</div>
	</div>
</div>
</section>
<div class="container">
	<div class="row">
		<div class="col-lg-3">
			<aside class="sidebar">
				<h4 class="heading-Success"><strong>Menu</strong> Prodi</h4>

				<ul class="nav nav-list flex-column mb-4 sort-source" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows'}">
					<!-- <li class="nav-item" data-option-value="*"><a class="nav-link active" href="#">Show All</a></li> -->
					<?php if ($menuprodi!=false) {
						foreach ($menuprodi as $row) { ?>
							<li class="nav-item" data-option-value=".<?=$row->pageId?>"><a class="nav-link" href="#">
								<?=$row->pageJudul?></a></li>
							<?php } } ?>
							<li class="nav-item" data-option-value=".staff"><a class="nav-link" href="#">Sumber Daya Manusia</a></li>

						</ul>

						<hr class="invisible mt-5 mb-2">

						<div class="tabs mb-4">
							<ul class="nav nav-tabs">
								<li class="nav-item active"><a class="nav-link" href="#popularPosts" data-toggle="tab"><i class="fa fa-star"></i> Pengumuman</a></li>
								<li class="nav-item"><a class="nav-link" href="#recentPosts" data-toggle="tab">Berita</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="popularPosts">
									<ul class="simple-post-list">
										<?php
										if($pengumuman!==false) {
											$i = 1;
											foreach($pengumuman as $row) { ?>
												<li>
													<div class="post-image">
														<div class="img-thumbnail d-block">
															<a href="blog-post.html">
																<img class="img-fluid" src="<?php echo base_url('pengumuman/loadthumb/') . $row->beritaBanner ?>" alt="">
															</a>
														</div>
													</div>
													<div class="post-info">
														<a href="<?php echo base_url() ?>pengumuman/post/<?= $row->beritaNama ?>"><?=$row->beritaJudul?></a>
														<div class="post-meta">
															<span><i class="fa fa-calendar"></i> <?=datetoindo($row->beritaDatetime)?> </span>
															<span><i class="fa fa-user"></i> By <a ><?=$row->beritaAuthor?></a> </span>
															<span><i class="fa fa-tag"></i> <a ><?=$row->beritaTag?></a> </span>
														</div>
													</div>
												</li>
											<?php } }?>
									</ul>
								</div>
								<div class="tab-pane" id="recentPosts">
									<ul class="simple-post-list">
											<?php
										if($pin!==false) {
											$i = 1;
											foreach($pin as $row) { ?>
										<li>
											<div class="post-image">
												<div class="img-thumbnail d-block">
													<a href="blog-post.html">
														<img class="img-fluid" src="<?php echo base_url('pengumuman/loadthumb/') . $row->beritaBanner ?>" alt="">
													</a>
												</div>
											</div>
											<div class="post-info">
												<a href="<?php echo base_url() ?>pengumuman/post/<?= $row->beritaNama ?>"><?=$row->beritaJudul?></a>
												<div class="post-meta">
													<span><i class="fa fa-calendar"></i> <?=datetoindo($row->beritaDatetime)?> </span>
															<span><i class="fa fa-user"></i> By <a ><?=$row->beritaAuthor?></a> </span>
															<span><i class="fa fa-tag"></i> <a ><?=$row->beritaTag?></a> </span>
												</div>
											</div>
										</li>
										<?php } }?>
									</ul>
								</div>
							</div>
						</div>

					</aside>
				</div>
				<div class="col-lg-6">
					<div class="sort-destination-loader sort-destination-loader-showing">
						<div class="row portfolio-list sort-destination" data-sort-id="portfolio">
							<?php if ($menuprodi!=false) {
								foreach ($menuprodi as $row) { ?>
									<div class="col-lg-12 isotope-item <?=$row->pageId?>">
										<div class="portfolio-item">
											<p>  <?=$row->pageContent?></p>
										</div>
									</div>
								<?php } } ?>

								<div class="col-lg-12 isotope-item staff">
									  <div class="portfolio-item">
                            <h3 class="heading-primary"><strong>Staff</strong> <?= $datas->prodiNama?></h3>
                            <div class="featured-boxes featured-boxes-style-3 featured-boxes-flat">
                        <div class="row">
                            <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-quaternary featured-box-effect-2">
                                    <div class="box-content">
                                       <a href="<?= base_url(); ?>prodi/post_sdm/<?=$prorjur_select ?>/11">
                                        <i class="icon-featured fa fa-user"></i></a>
                                        <h4>Dosen PNS</h4>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-quaternary featured-box-effect-2">
                                    <div class="box-content">
                                       <a href="<?= base_url(); ?>prodi/post_sdm/<?= $prorjur_select?>/12">
                                        <i class="icon-featured fa fa-user"></i></a>
                                        <h4>Dosen Non PNS</h4>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-quaternary featured-box-effect-3">
                                    <div class="box-content">
                                       <a href="<?= base_url(); ?>prodi/post_sdm/<?= $prorjur_select?>/22"><i class="icon-featured fa fa-user"></i></a>
                                        <h4>Tendik PNS</h4>
                                        <!-- <p>Tenaga Kependidikan PNS. </p> -->
                                    </div>
                                </div>
                            </div>
                             <div class="col-lg-3 col-sm-4">
                                <div class="featured-box featured-box-primary featured-box-effect-3">
                                    <div class="box-content">
                                         <a href="<?= base_url(); ?>prodi/post_sdm/<?=$prorjur_select ?>/33"><i class="icon-featured fa fa-user"></i></a>
                                        <h4>Tendik Non PNS</h4>
                                        <!-- <p>Tenaga Kependidikan Non PNS.</p> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div> 
								</div>
							</div>
						</div>
					</div>

					<div class="col-lg-3">
						<aside class="sidebar">
						<h4 class="heading-primary">Informasi<strong> & Slider</strong></h4>
						
						<div class="sort-destination-loader sort-destination-loader-showing">
						<div class="row portfolio-list sort-destination" data-sort-id="portfolio">
							<?php if ($menuprodi!=false) {
								foreach ($menuprodi as $row) { ?>
									<div class="col-lg-12 isotope-item <?=$row->pageId?>">
										<div class="portfolio-item">
											<p>  <?=$row->pageInfo?></p>
										</div>
									</div>
								<?php } } ?>

							</div>
						</div>
						</aside>
					</div>
				</div>

			</div>

		</div>
