<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                         <li class="active">Jurusan</li>
                        <li class="active"><?= $prodinama?></li>
                       
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1><?=$datassdm->datas->pegmNama ?></h1>
                </div>
            </div>
            <div id="response"></div>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="container">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="owl-carousel owl-theme" data-plugin-options="{'items': 1, 'margin': 10}">
                                    <div>
                                        <span class="img-thumbnail d-block">
                                             <?php if ($karyawan->karyawanFoto == true) { ?>
                                        <img alt="" height="300" class="img-fluid" src="<?php echo base_url('sdm/loadimage/'). $karyawan->karyawanFoto ?>">
                                         <?php }else{ ?>
                                             <img alt="" height="300" class="img-fluid" src="<?php echo base_url('sdm/loadimage/noimage.jpg') ?>">
                                        <?php } ?>
                                        </span>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-lg-8">

                                <table class="table table-striped">
                                        <thead>
                                           </thead>
                                        <tbody>
                                            <tr>
                                                <td>Nama </td>
                                                <td><?=$datassdm->datas->namaGelar ?></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Jurusan</td>
                                                <td><?=$prodinamajurusan ?></td>
                                                
                                            </tr>
                                             <tr>
                                                <td>Email</td>
                                                <td><?=$datassdm->datas->pegmEmail ?></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Jabatan</td>
                                                <td><?=$karyawan->karyawanJabatan ?></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Pangkat</td>
                                                <td><?=$datassdm->datas->golNama ?></td>
                                            </tr>
                                            <tr>
                                                <td>Golongan</td>
                                                <td><?=$datassdm->datas->golId ?></td>
                                                
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                            </div>
                        </div>

                    <div class="row">
                        <div class="col">
                           <div class="col-lg-12">
                            <h4></h4>
                            <div class="toggle toggle-primary toggle-sm" data-plugin-toggle>
                                 <?php if($karyawan->karyawanDetail==true) { ?>
                                <section class="toggle active">
                                    <label>Detail</label>
                                    <div class="toggle-content">
                                        <p><?=$karyawan->karyawanDetail?>
                                         </p>
                                    </div>
                                </section>
                                 <?php } ?>
                                <?php if($penelitian->status==true) { ?>
                                <section class="toggle">
                                    <label>Penelitian</label>
                                    <div class="toggle-content">
                                        <p>
                                            <table class="table table-hover">
                                                <thead>

                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if($penelitian!=false)
                                                    {
                                                        $i = 1;
                                                        foreach($penelitian->data as $row)
                                                        {  
                                                         //  if($row->status !=false) // {

                                                         ?>
                                                         <tr>
                                                            <th scope="row"><?=$i++?></th>
                                                            <td>
                                                                <table class="table table-striped">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>Judul </td>
                                                                            <td> 
                                                                                <?=$row->pltjudul ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Penulis </td>
                                                                                <td> 
                                                                                    <?=$row->pltpenulis ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>URL</td>
                                                                                    <td><?=$row->plturl ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Kegiatan</td>
                                                                                    <td><?=$row->rkgkegiatan ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Tahun</td>
                                                                                    <td><?=$row->plttahunterbit ?></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                             // }
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </p>
                                        </div>
                                    </section>
                                    <?php } ?>
                                <?php if($pengabdian->status!=false) { ?>
                                <section class="toggle">
                                    <label>Pengabdian</label>
                                    <div class="toggle-content">
                                        <p>
                                            <table class="table table-hover">
                                                <thead>

                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if($pengabdian!=false)
                                                    {
                                                        $i = 1;
                                                        foreach($pengabdian->data as $row)
                                                        {  

                                                         //  if($row->status !=false) // {

                                                         ?>
                                                         <tr>
                                                            <th scope="row"><?=$i++?></th>
                                                            <td>
                                                                <table class="table table-striped">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>Judul </td>
                                                                            <td> 
                                                                                <?=$row->pgdketerangan ?></td>
                                                                            </tr>
                                                                                <tr>
                                                                                    <td>Keterangan</td>
                                                                                    <td><?=$row->pgdnomor ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Kegiatan</td>
                                                                                    <td><?=$row->rkgkegiatan ?> </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Tahun</td>
                                                                                    <td><?=$row->rkgtahun ?></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                     // }
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                        </p>
                                    </div>
                                </section>
                                 <?php } ?>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $this->load->view('page/sidebar');
            ?>
        </div>
    </div>
</div>