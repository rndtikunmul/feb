<div role="main" class="main">
    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url()?>">Home</a></li>
                        <li class="active"><?=$is_active ?></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1><?=$is_active ?></h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-lg-9">
                <div class="blog-posts">
                    <?php if ($keyword !== FALSE): ?>
                        <div>
                            <h4>Hasil Pencarian dengan kata kunci "<?=$this->session->keyword?>"</h4>
                        </div>
                        <br>
                        <br>
                        <?php if (empty($datas)): ?>
                          <div>
                            <p>Tidak ditemukan data dengan kata kunci "<?=$this->session->keyword?>"</p>
                        </div>
                    <?php endif ?>
                <?php endif; ?> 
                <?php
                if($berita!==false) {
                  $i = 1;
                  foreach($berita as $row) { ?>

                    <article class="post post-medium">
                        <div class="row">

                            <div class="col-lg-5">
                                <div class="post-image">
                                    <div>
                                    <?php if ($row->beritaBanner == true) { ?>
                                        <div class="img-thumbnail">
                                        <img alt="" height="300" class="img-fluid" src="<?php echo base_url('berita/loadthumb/') . $row->beritaBanner ?>">
                                        </div>
                                         <?php }else{ ?>
                                            <div class="img-thumbnail">
                                             <img alt="" height="300" class="img-fluid" src="<?php echo base_url('berita/loadthumb/noimage.jpg') ?>">
                                             </div>
                                        <?php } ?>
                                        <!-- <div class="img-thumbnail">
                                            <img class="img-fluid" src="<?php echo base_url('berita/loadthumb/') . $row->beritaBanner ?>" alt="">
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7">

                                <div class="post-content">

                                    <h2><a href="<?php echo base_url() ?>berita/post/<?= $row->beritaNama ?>"><?=$row->beritaJudul?></a></h2>
                                    <p><?= substr(strip_tags($row->beritaContent), 0, 200) . '...'; ?></p>

                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="post-meta">
                                    <span><i class="fa fa-calendar"></i> <?=datetoindo($row->beritaDatetime)?></span>
                                    <span><i class="fa fa-user"></i> By <a ><?=$row->beritaAuthor?></a> </span>
                                    <span><i class="fa fa-tag"></i> <a ><?=$row->beritaTag?></a> </span>
                                    <span class="d-block d-md-inline-block float-md-right mt-3 mt-md-0"><a href="<?php echo base_url() ?>berita/post/<?= $row->beritaNama ?>" class="btn btn-xs btn-primary">Read more...</a></span>
                                </div>
                            </div>
                        </div>

                    </article>
                    <?php   $i++;}} ?>
                    
                </ul>

            </div>
            <nav aria-label="Page navigation">
                <?php echo $this->pagination->create_links(); ?>
            </nav>
        </div>

        <?php
        $this->load->view('page/sidebar');
        ?>
    </div>

</div>

</div>