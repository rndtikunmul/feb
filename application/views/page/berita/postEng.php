<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url()?>">Home</a></li>
                        <li class="active"><?=$is_active ?></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1><?= $datasberita->BeritaJudulEng ?></h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-lg-9">
                <div class="blog-posts single-post">
                    <article class="post post-large blog-single-post">
                        <div class="post-image">
                            <div class="owl-carousel owl-theme" data-plugin-options="{'items':1}">
                                <div>
                                <?php if ($datasberita->beritaBanner == true) { ?>
                                        <div class="img-thumbnail d-block">
                                        <img alt=""  class="img-fluid" src="<?php echo base_url('berita/loadimage/') . $datasberita->beritaBanner ?>">
                                        </div>
                                         <?php }else{ ?>
                                            <div class="img-thumbnail d-block">
                                             <img alt=""  class="img-fluid" src="<?php echo base_url('berita/loadimage/noimage.jpg') ?>">
                                             </div>
                                    <?php } ?>
                                    <!-- <div class="img-thumbnail d-block">
                                        <img class="img-fluid" src="" alt="">
                                    </div> -->
                                </div>
                            </div>
                        </div>

                        <div class="post-date">
                            <span class="day"><?= date('d', strtotime($datasberita->beritaDatetime)) ?></span>
                            <span class="month"><?= date('M', strtotime($datasberita->beritaDatetime)) ?></span>
                        </div>
                        
                        <div class="post-content">
                            <p><?= $datasberita->BeritaContentEng ?></p>
                            <i class="fa fa-tag"> <?= $datasberita->beritaTag ?></i> <i class="fa fa-user"> <?= $datasberita->beritaAuthor ?></i>
                        </div>
                        
                    </article>
                    
                </div>
            </div>

            <?php
            $this->load->view('page/sidebareng');
            ?>
        </div>

    </div>


</div>