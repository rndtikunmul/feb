<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url()?>">Home</a></li>
                        <li class="active"><?=$is_active ?></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1><?=$is_active ?></h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-lg-9">
                <div class="blog-posts">
                    <?php
                    if($pengumuman!==false) {
                      $i = 1;
                      foreach($pengumuman as $row) { ?>

                        <article class="post post-medium">
                            <div class="row">

                                <div class="col-lg-5">
                                    <div class="post-image">
                                        <div>
                                        <?php if ($row->beritaBanner == true) { ?>
                                        <div class="img-thumbnail">
                                        <img alt="" height="300" class="img-fluid" src="<?php echo base_url('berita/loadthumb/') . $row->beritaBanner ?>">
                                        </div>
                                         <?php }else{ ?>
                                            <div class="img-thumbnail">
                                             <img alt="" height="300" class="img-fluid" src="<?php echo base_url('berita/loadthumb/noimage.jpg') ?>">
                                             </div>
                                        <?php } ?>
                                            <!-- <div class="img-thumbnail">
                                                <img class="img-fluid" src="<?php echo base_url('pengumuman/loadthumb/') . $row->beritaBanner ?>" alt="">
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7">

                                    <div class="post-content">

                                        <h2><a href="<?php echo base_url() ?>Eng/postEng/<?= $row->beritaNama ?>"><?=$row->BeritaJudulEng?></a></h2>
                                        <p><?= substr(strip_tags($row->BeritaContentEng), 0, 200) . '...'; ?></p>

                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="post-meta">
                                        <span><i class="fa fa-calendar"></i> <?=datetoindo($row->beritaDatetime)?> </span>
                                        <span><i class="fa fa-user"></i> By <a ><?=$row->beritaAuthor?></a> </span>
                                        <span><i class="fa fa-tag"></i> <a ><?=$row->beritaTag?></a> </span>
                                        <span class="d-block d-md-inline-block float-md-right mt-3 mt-md-0"><a href="<?php echo base_url() ?>Eng/postEng/<?= $row->beritaNama ?>" class="btn btn-xs btn-primary">Read more...</a></span>
                                    </div>
                                </div>
                            </div>

                        </article>
                    <?php }
                }?>
                
            </div>
            <nav aria-label="Page navigation">
                <?php echo $this->pagination->create_links(); ?>
            </nav>

        </div>

        <?php
        $this->load->view('page/sidebareng');
        ?>
    </div>

</div>

</div>